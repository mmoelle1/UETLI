########################################################################
# ArrayFire.cmake
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the UETLI authors
#
# This file is part of the UETLI library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

########################################################################
# ArrayFire
########################################################################

if (UETLI_BUILTIN_ARRAYFIRE)
  
  find_package(ArrayFire QUIET REQUIRED)

  # Add include directory
  include_directories(${ArrayFire_INCLUDE_DIRS})
  
  if(UETLI_BUILD_UNITTESTS_DEVICES STREQUAL "ALL")
    # Unified backend
    list(APPEND UETLI_C_TARGET_LINK_LIBRARIES "${ArrayFire_LIBRARIES}")
    list(APPEND UETLI_CXX_TARGET_LINK_LIBRARIES "${ArrayFire_LIBRARIES}")
  elseif(UETLI_BUILD_UNITTESTS_DEVICES STREQUAL "CPU")
    # CPU backend
    list(APPEND UETLI_C_TARGET_LINK_LIBRARIES "${ArrayFire_CPU_LIBRARIES}")
    list(APPEND UETLI_CXX_TARGET_LINK_LIBRARIES "${ArrayFire_CPU_LIBRARIES}")
  else()
    if(UETLI_WITH_CUDA)
      list(APPEND UETLI_C_TARGET_LINK_LIBRARIES "${ArrayFire_CUDA_LIBRARIES}")
      list(APPEND UETLI_CXX_TARGET_LINK_LIBRARIES "${ArrayFire_CUDA_LIBRARIES}")
    elseif(UETLI_WITH_OCL)
      list(APPEND UETLI_C_TARGET_LINK_LIBRARIES "${ArrayFire_OpenCL_LIBRARIES}")
      list(APPEND UETLI_CXX_TARGET_LINK_LIBRARIES "${ArrayFire_OpenCL_LIBRARIES}")
    else()
      message(FATAL_ERROR "Explicit support for GPUs requires UETLI_WITH_CUDA or UETLI_WITH_OCL enabled.")
    endif()
  endif()

else()

  # Add include directory
  if(ARRAYFIRE_INCLUDE_PATH)
    include_directories(${ARRAYFIRE_INCLUDE_PATH})
  else()
    message(WARNING "Variable ARRAYFIRE_INCLUDE_PATH is not defined. UETLI might be unable to find ArrayFire include files.")
  endif()

  # Add libraries
  if(ARRAYFIRE_LIBRARIES)
    list(APPEND UETLI_C_TARGET_LINK_LIBRARIES   ${ARRAYFIRE_LIBRARIES})
    list(APPEND UETLI_CXX_TARGET_LINK_LIBRARIES ${ARRAYFIRE_LIBRARIES})
  else()
    message(FATAL_ERROR "Variable ARRAYFIRE_LIBRARIES must point to the ArrayFire libraries.")
  endif()
  
endif()

# Enable ArrayFire support
add_definitions(-DSUPPORT_ARRAYFIRE)
