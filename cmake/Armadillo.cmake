########################################################################
# Armadillo.cmake
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the UETLI authors
#
# This file is part of the UETLI library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

########################################################################
# Armadillo
########################################################################

if (UETLI_BUILTIN_ARMADILLO)

  if (UETLI_BUILTIN_ARMADILLO MATCHES "7.800.1")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               http://downloads.sourceforge.net/project/arma/armadillo-7.800.1.tar.xz
      URL_MD5           e094351771c40a6e06e1a9c1ffdfc2b8
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (UETLI_BUILTIN_ARMADILLO MATCHES "7.800.2")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               http://downloads.sourceforge.net/project/arma/armadillo-7.800.2.tar.xz
      URL_MD5           c601f3a5ec6d50666aa3a539fa20e6ca
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (UETLI_BUILTIN_ARMADILLO MATCHES "7.800.3")
  
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               http://downloads.sourceforge.net/project/arma/armadillo-7.800.3.tar.xz
      URL_MD5           d78c2e4c72d8c1a8360e920365bdd538
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (UETLI_BUILTIN_ARMADILLO MATCHES "7.950.0")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               http://downloads.sourceforge.net/project/arma/armadillo-7.950.0.tar.xz
      URL_MD5           41700f9b7f38d5835039e014572fadd7
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (UETLI_BUILTIN_ARMADILLO MATCHES "7.950.1")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               http://downloads.sourceforge.net/project/arma/armadillo-7.950.1.tar.xz
      URL_MD5           c06eb38b12cae49cab0ce05f96147147
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (UETLI_BUILTIN_ARMADILLO MATCHES "7.950.2")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               http://downloads.sourceforge.net/project/arma/armadillo-7.950.2.tar.xz
      URL_MD5           78dbb646a437871604ce2bc308288c36
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (UETLI_BUILTIN_ARMADILLO MATCHES "7.960.0")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               http://downloads.sourceforge.net/project/arma/armadillo-7.960.0.tar.xz
      URL_MD5           70cd73c11d0ec20f309d74a8c70a9565
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (UETLI_BUILTIN_ARMADILLO MATCHES "7.960.1")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               http://downloads.sourceforge.net/project/arma/armadillo-7.690.1.tar.xz
      URL_MD5           5f6800df42e07efa65d5ad33fcdfc64d
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (UETLI_BUILTIN_ARMADILLO MATCHES "7.960.2")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               http://downloads.sourceforge.net/project/arma/armadillo-7.960.2.tar.xz
      URL_MD5           b514fa20beb381e46073b6ce064f8f09
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
  
  elseif (UETLI_BUILTIN_ARMADILLO MATCHES "8.100.0")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               http://downloads.sourceforge.net/project/arma/armadillo-8.100.0.tar.xz
      URL_MD5           5f4aa46d15d9d364874fa4bde1a7add0
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (UETLI_BUILTIN_ARMADILLO MATCHES "8.100.1")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               http://downloads.sourceforge.net/project/arma/armadillo-8.100.1.tar.xz
      URL_MD5           d9762d6f097e0451d0cfadfbda295e7c
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (UETLI_BUILTIN_ARMADILLO MATCHES "8.200.0")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               http://downloads.sourceforge.net/project/arma/armadillo-8.200.0.tar.xz
      URL_MD5           d806886d8daee94b01a6ccaa7c8ef912
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (UETLI_BUILTIN_ARMADILLO MATCHES "8.200.1")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               http://downloads.sourceforge.net/project/arma/armadillo-8.200.1.tar.xz
      URL_MD5           83f4da2b5e12d9f354c2ac10a91b6ca4
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
  
  elseif (UETLI_BUILTIN_ARMADILLO MATCHES "8.200.2")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               http://downloads.sourceforge.net/project/arma/armadillo-8.200.2.tar.xz
      URL_MD5           ba298a232a5a37071b8e685c4bd3e383
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (UETLI_BUILTIN_ARMADILLO MATCHES "8.300.0")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               http://downloads.sourceforge.net/project/arma/armadillo-8.300.0.tar.xz
      URL_MD5           591081111cbd899f19fa2c646fb643be
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (UETLI_BUILTIN_ARMADILLO MATCHES "8.300.1")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               http://downloads.sourceforge.net/project/arma/armadillo-8.300.1.tar.xz
      URL_MD5           ba50505d83e83cc9373cb3937d507a8c
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
  
  elseif (UETLI_BUILTIN_ARMADILLO MATCHES "8.300.2")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               http://downloads.sourceforge.net/project/arma/armadillo-8.300.2.tar.xz
      URL_MD5           c65d2427cb837a69cfe94012c262740a
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (UETLI_BUILTIN_ARMADILLO MATCHES "8.300.3")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               http://downloads.sourceforge.net/project/arma/armadillo-8.300.3.tar.xz
      URL_MD5           6ae86fe887c48a4c97f5e8214218f8bc
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
  
  elseif (UETLI_BUILTIN_ARMADILLO MATCHES "8.300.4")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               http://downloads.sourceforge.net/project/arma/armadillo-8.300.4.tar.xz
      URL_MD5           19d569aaf088259b54a21ee015ba75f3
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
  
  elseif (UETLI_BUILTIN_ARMADILLO MATCHES "8.400.0")
    
    include(DownloadProject)
    download_project(
      PROJ              Armadillo
      URL               http://downloads.sourceforge.net/project/arma/armadillo-8.400.0.tar.xz
      URL_MD5           0487af6131e125be091a70d600e90762
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  else()
    message(FATAL_ERROR, "Invalid Armadillo version.")
  endif()
    
  # Process Armadillo project
  if (NOT TARGET armadillo)
    add_subdirectory(${Armadillo_SOURCE_DIR} ${Armadillo_BINARY_DIR})
  endif()
  
  # Add include directory
  include_directories("${Armadillo_SOURCE_DIR}/include")
  
  # Add library
  list(APPEND UETLI_C_TARGET_LINK_LIBRARIES   armadillo)
  list(APPEND UETLI_CXX_TARGET_LINK_LIBRARIES armadillo)

elseif (UETLI_BUILTIN_ARMADILLO MATCHES "latest")
  
  include(DownloadProject)
  download_project(
    PROJ              Armadillo
    GIT_REPOSITORY    https://github.com/conradsnicta/armadillo-code.git
    TIMEOUT           180
    PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
    PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
    ${UPDATE_DISCONNECTED_IF_AVAILABLE}
    )
  
  # Process Armadillo project
  if (NOT TARGET armadillo)
    add_subdirectory(${Armadillo_SOURCE_DIR} ${Armadillo_BINARY_DIR})
  endif()
  
  # Add include directory
  include_directories("${Armadillo_SOURCE_DIR}/include")
  
  # Add library
  list(APPEND UETLI_C_TARGET_LINK_LIBRARIES   armadillo)
  list(APPEND UETLI_CXX_TARGET_LINK_LIBRARIES armadillo)
  
else()
  
  # Add include directory
  if(ARMADILLO_INCLUDE_PATH)
    include_directories(${ARMADILLO_INCLUDE_PATH})
  else()
    message(WARNING "Variable ARMADILLO_INCLUDE_PATH is not defined. UETLI might be unable to find Armadillo include files.")
  endif()

  # Add libraries
  if(ARMADILLO_LIBRARIES)
    list(APPEND UETLI_C_TARGET_LINK_LIBRARIES   ${ARMADILLO_LIBRARIES})
    list(APPEND UETLI_CXX_TARGET_LINK_LIBRARIES ${ARMADILLO_LIBRARIES})
  else()
    message(FATAL_ERROR "Variable ARMADILLO_LIBRARIES must point to the Armadillo libraries.")
  endif()
  
endif()

# Enable Armadillo support
add_definitions(-DSUPPORT_ARMADILLO)
