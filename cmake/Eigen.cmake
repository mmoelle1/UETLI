########################################################################
# Eigen.cmake
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the UETLI authors
#
# This file is part of the UETLI library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

########################################################################
# Eigen
########################################################################

if (UETLI_BUILTIN_EIGEN)

  if (UETLI_BUILTIN_EIGEN MATCHES "3.2.0")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               http://bitbucket.org/eigen/eigen/get/3.2.0.tar.bz2
      URL_MD5           894381be5be65bb7099c6fd91d61b357
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.2.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (UETLI_BUILTIN_EIGEN MATCHES "3.2.1")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               http://bitbucket.org/eigen/eigen/get/3.2.1.tar.bz2
      URL_MD5           ece1dbf64a49753218ce951624f4c487
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.2.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (UETLI_BUILTIN_EIGEN MATCHES "3.2.2")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               http://bitbucket.org/eigen/eigen/get/3.2.2.tar.bz2
      URL_MD5           fc2e814ae449d16b331f7e1f4e272bd3
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.2.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (UETLI_BUILTIN_EIGEN MATCHES "3.2.3")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               http://bitbucket.org/eigen/eigen/get/3.2.3.tar.bz2
      URL_MD5           a88bf9a8e674762619429e80f46005bf
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.2.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (UETLI_BUILTIN_EIGEN MATCHES "3.2.4")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               http://bitbucket.org/eigen/eigen/get/3.2.4.tar.bz2
      URL_MD5           4c4b5ed9a388a1e475166d575af25477
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.2.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (UETLI_BUILTIN_EIGEN MATCHES "3.2.5")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               http://bitbucket.org/eigen/eigen/get/3.2.5.tar.bz2
      URL_MD5           21a928f6e0f1c7f24b6f63ff823593f5
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.2.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (UETLI_BUILTIN_EIGEN MATCHES "3.2.6")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               http://bitbucket.org/eigen/eigen/get/3.2.6.tar.bz2
      URL_MD5           87274966745d2d3e7964fcc654d0a24b
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.2.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (UETLI_BUILTIN_EIGEN MATCHES "3.2.7")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               http://bitbucket.org/eigen/eigen/get/3.2.7.tar.bz2
      URL_MD5           cc1bacbad97558b97da6b77c9644f184
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.2.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (UETLI_BUILTIN_EIGEN MATCHES "3.2.8")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               http://bitbucket.org/eigen/eigen/get/3.2.8.tar.bz2
      URL_MD5           9e3bfaaab3db18253cfd87ea697b3ab1
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.2.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (UETLI_BUILTIN_EIGEN MATCHES "3.2.9")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               http://bitbucket.org/eigen/eigen/get/3.2.9.tar.bz2
      URL_MD5           de11bfbfe2fd2dc4b32e8f416f58ee98
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.2.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (UETLI_BUILTIN_EIGEN MATCHES "3.2.10")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               http://bitbucket.org/eigen/eigen/get/3.2.10.tar.bz2
      URL_MD5           cad3e2079d9d97e9a95f854a298e9c0e
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.2.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (UETLI_BUILTIN_EIGEN MATCHES "3.3.0")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               http://bitbucket.org/eigen/eigen/get/3.3.0.tar.bz2
      URL_MD5           fd1ecefaacc9223958b6a66f9a348424
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.3.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (UETLI_BUILTIN_EIGEN MATCHES "3.3.1")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               http://bitbucket.org/eigen/eigen/get/3.3.1.tar.bz2
      URL_MD5           edb6799ef413b0868aace20d2403864c
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.3.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (UETLI_BUILTIN_EIGEN MATCHES "3.3.2")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               http://bitbucket.org/eigen/eigen/get/3.3.2.tar.bz2
      URL_MD5           7a94c3280ae1961bc8df5e3bd304013a
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.3.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  elseif (UETLI_BUILTIN_EIGEN MATCHES "3.3.3")
  
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               http://bitbucket.org/eigen/eigen/get/3.3.3.tar.bz2
      URL_MD5           b2ddade41040d9cf73b39b4b51e8775b
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.3.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )
    
  elseif (UETLI_BUILTIN_EIGEN MATCHES "3.3.4")
    
    include(DownloadProject)
    download_project(
      PROJ              Eigen
      URL               http://bitbucket.org/eigen/eigen/get/3.3.4.tar.bz2
      URL_MD5           a7aab9f758249b86c93221ad417fbe18
      TIMEOUT           180
      PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
      PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.3.patch
      ${UPDATE_DISCONNECTED_IF_AVAILABLE}
      )

  else()
    message(FATAL_ERROR, "Invalid Eigen version.")
  endif()
  
  # Add include directory
  include_directories("${Eigen_SOURCE_DIR}")

elseif (UETLI_BUILTIN_EIGEN MATCHES "latest")
  
  include(DownloadProject)
  download_project(
    PROJ              Eigen
    GIT_REPOSITORY    https://github.com/eigenteam/eigen-git-mirror.git
    TIMEOUT           180
    PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
    PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.3.patch
    ${UPDATE_DISCONNECTED_IF_AVAILABLE}
    )
    
  # Add include directory
  include_directories("${Eigen_SOURCE_DIR}")
  
else()

  # Add include directory
  if(EIGEN_INCLUDE_PATH)
    include_directories(${EIGEN_INCLUDE_PATH})
  else()
    message(WARNING "Variable EIGEN_INCLUDE_PATH is not defined. UETLI might be unable to find Eigen include files.")
  endif()
  
  # Add libraries
  if(EIGEN_LIBRARIES)
    list(APPEND UETLI_C_TARGET_LINK_LIBRARIES   ${EIGEN_LIBRARIES})
    list(APPEND UETLI_CXX_TARGET_LINK_LIBRARIES ${EIGEN_LIBRARIES})
  else()
    message(FATAL_ERROR "Variable EIGEN_LIBRARIES must point to the Eigen libraries.")
  endif()
  
endif()

# Enable Eigen support
add_definitions(-DSUPPORT_EIGEN)
