########################################################################
# uBLAS.cmake
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the UETLI authors
#
# This file is part of the UETLI library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

# Find BOOST shipping with uBLAS
find_package(Boost COMPONENTS QUIET REQUIRED)

# Enable uBLAS support
add_definitions(-DSUPPORT_UBLAS)

# Add include directory
include_directories(${Boost_INCLUDE_DIRS})
set(LIBS ${LIBS} ${Boost_LIBRARIES})
