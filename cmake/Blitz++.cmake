########################################################################
# Blitz++.txt
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the UETLI authors
#
# This file is part of the UETLI library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

########################################################################
# Blitz++
########################################################################

if (UETLI_BUILTIN_BLITZ)
  
  include(DownloadProject)
  download_project(
    PROJ              Blitz
    URL               http://downloads.sourceforge.net/project/blitz/blitz/Blitz%2B%2B%200.10/blitz-0.10.tar.gz
    URL_MD5           66268b92bda923735f2e3afc87dcb58a
    TIMEOUT           180
    PREFIX            ${CMAKE_BINARY_DIR}/external/Blitz++
    ${UPDATE_DISCONNECTED_IF_AVAILABLE}
    )
  
  # Add Blitz++ definitions
  add_definitions(-DBZ_HAVE_CONFIG_H -DBZ_HAVE_IEEE_MATH -DBZ_HAVE_SYSTEM_V_MATH -DBZ_HAVE_COMPLEX_FCNS)
  
  # Add include directory
  include_directories("${Blitz_SOURCE_DIR}")

else()

  # Add include directory
  if(BLITZ++_INCLUDE_PATH)
    include_directories(${BLITZ_INCLUDE_PATH})
  else()
    message(WARNING "Variable BLITZ++_INCLUDE_PATH is not defined. UETLI might be unable to find Blitz++ include files.")
  endif()
  
  # Add libraries
  if(BLITZ++_LIBRARIES)
    list(APPEND UETLI_C_TARGET_LINK_LIBRARIES   ${BLITZ_LIBRARIES})
    list(APPEND UETLI_CXX_TARGET_LINK_LIBRARIES ${BLITZ_LIBRARIES})
  else()
    message(FATAL_ERROR "Variable BLITZ++_LIBRARIES must point to the Blitz++ libraries.")
  endif()
  
endif()

# Enable Blitz++ support
add_definitions(-DSUPPORT_BLITZ)
