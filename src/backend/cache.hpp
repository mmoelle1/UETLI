/** @file blaze.hpp
 *
 *  @brief Implementation details for Cache library
 *
 *  This file implements a generic expression template engine which
 *  wraps all calls to the underlying low-level expression template
 *  library into a unfied API. It also provides a user-transparent
 *  mechanism to store smart pointers (std::shared_ptr) to temporary
 *  expression returned from the low-level expression template
 *  library, which in some cases would lead to dangling pointers.
 *
 *  @copyright This file is part of the UETLI library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef BACKEND_CACHE_HPP
#define BACKEND_CACHE_HPP

#include <type_traits>

#include "uetliCache.hpp"

#ifdef DOXYGEN
namespace uetli {
#endif

/** @brief
 *  If T is of type EnumETL::CACHE, provides the member constant
 * value equal to true. Otherwise value is false.
 */
template <typename T>
struct is_type_of<T, EnumETL::CACHE,
                  typename std::enable_if<std::is_base_of<
                      ::uetli::cache::uetliExprBase,
                      typename uetli::remove_all<T>::type>::value>::type>
    : public std::true_type {};

/** @brief
 *  Result type of the expression (CACHE type)
 */
template <typename Expr>
struct result_type<
    Expr, typename uetli::enable_if_type_of<Expr, EnumETL::CACHE>::type> {
  using type = typename uetli::remove_all<Expr>::type::result_type;
};

/** @brief
 *  Scalar value type of the expression (CACHE type)
 */
template <typename Expr>
struct value_type<
    Expr, typename uetli::enable_if_type_of<Expr, EnumETL::CACHE>::type> {
  using type =
      typename uetli::value_type<typename uetli::result_type<Expr>::type>::type;
};

namespace internal {

/** @brief
 *  Indicator for specialized CACHE implementation of
 *  uetli::internal::make_constant_impl<T,Expr>(const T value, Expr&& expr)
 * function
 */
template <typename Expr>
struct has_make_constant_impl<
    Expr, typename uetli::enable_if_type_of<Expr, uetli::EnumETL::CACHE>::type>
    : public std::true_type {};

/** @brief
 *  Create CACHE constant
 */
template <typename TC, typename T, typename Expr>
static UETLI_INLINE auto make_constant_impl(const T value, Expr &&expr)
#if !defined(DOXYGEN)
    -> typename uetli::enable_if_type_of<
        Expr, EnumETL::CACHE, ::uetli::cache::uetliExpr<TC::tag, T>>::type
#endif
{
  return ::uetli::cache::uetliExpr<TC::tag, T>(T(value));
}

/** @brief
 *  Indicator for specialized CACHE implementation of
 *  uetli::internal::make_temp_impl<Tag,Expr>(Expr&& expr) function
 */
template <std::size_t Tag, typename Expr>
struct has_make_temp_impl<
    Tag, Expr, typename uetli::enable_if_type_of<Expr, EnumETL::CACHE>::type>
    : public std::true_type {};

/** @brief
 *  CACHE type creation from expressions
 */
template <std::size_t Tag, typename Expr>
static UETLI_INLINE auto make_temp_impl(Expr &&expr)
#if !defined(DOXYGEN)
    -> typename uetli::enable_if_type_of<
        Expr, EnumETL::CACHE, typename uetli::result_type<Expr>::type>::type
#endif
{
  using Temp = typename uetli::result_type<Expr>::type;
  return Temp(std::forward<Expr>(expr));
}

/** @brief
 *  Indicator for specialized CACHE implementation of
 *  uetli::internal::tag_impl<Tag,Expr>(Expr&& expr) function
 */
template <std::size_t Tag, typename Expr>
struct has_tag_impl<
    Tag, Expr, typename uetli::enable_if_type_of<Expr, EnumETL::CACHE>::type>
    : public std::true_type {};

/** @brief
 *  Tags terminal with a unique (in a single expression) tag.
 *
 *  By tagging terminals user guarantees that the terminals with same
 *  tags actually refer to the same data.
 */
template <std::size_t Tag, typename Expr>
static UETLI_INLINE auto tag_impl(Expr &&expr)
#if !defined(DOXYGEN)
    -> ::uetli::cache::uetliExprView<Tag, decltype(expr.get())>
#endif
{
  return ::uetli::cache::uetliExprView<Tag, decltype(expr.get())>(expr.get());
}

/** @brief
 *  Selector for specialized Eigen::Matrix implementation of
 *  uetli::elem_mul<A,B>(A&& a, B&& b) function
 */
template <typename A, typename B>
struct get_elem_mul_impl<
    A, B, typename uetli::enable_if_any_type_of<A, B, EnumETL::CACHE>::type>
    : public std::integral_constant<EnumETL, EnumETL::CACHE> {};

/** @brief
 *  Element-wise multiplication of CACHE types
 */
template <typename A, typename B> struct elem_mul_impl<A, B, EnumETL::CACHE> {
  static UETLI_INLINE auto constexpr eval(A &&a, B &&b)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> ::uetli::cache::uetliExprBinaryOp_elem_mul<
          utils::hash<std::size_t>('m', 'u', 'l',
                                   uetli::remove_all<A>::type::tag,
                                   uetli::remove_all<A>::type::tag),
          A, B>
#endif
  {
    return ::uetli::cache::uetliExprBinaryOp_elem_mul<
        utils::hash<std::size_t>('m', 'u', 'l', uetli::remove_all<A>::type::tag,
                                 uetli::remove_all<B>::type::tag),
        A, B>(std::forward<A>(a), std::forward<B>(b));
  }
};

/** @brief
 *  Selector for specialized CACHE implementation of
 *  uetli::elem_div<A,B>(A&& a, B&& b) function
 */
template <typename A, typename B>
struct get_elem_div_impl<
    A, B, typename uetli::enable_if_any_type_of<A, B, EnumETL::CACHE>::type>
    : public std::integral_constant<EnumETL, EnumETL::CACHE> {};

/** @brief
 *  Element-wise division of CACHE types
 */
template <typename A, typename B> struct elem_div_impl<A, B, EnumETL::CACHE> {
  static UETLI_INLINE auto constexpr eval(A &&a, B &&b)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> ::uetli::cache::uetliExprBinaryOp_elem_div<
          utils::hash<std::size_t>('d', 'i', 'v',
                                   uetli::remove_all<A>::type::tag,
                                   uetli::remove_all<A>::type::tag),
          A, B>
#endif
  {
    return ::uetli::cache::uetliExprBinaryOp_elem_div<
        utils::hash<std::size_t>('d', 'i', 'v', uetli::remove_all<A>::type::tag,
                                 uetli::remove_all<B>::type::tag),
        A, B>(std::forward<A>(a), std::forward<B>(b));
  }
};

/** @brief
 *  Selector for specialized CACHE implementation of
 *  uetli::elem_pow<A,B>(A&& a, B&& b) function
 */
template <typename A, typename B>
struct get_elem_pow_impl<
    A, B, typename uetli::enable_if_type_of<A, EnumETL::CACHE>::type>
    : public std::integral_constant<EnumETL, EnumETL::CACHE> {};

/** @brief
 *  Element-wise pow-function of CACHE types
 */
template <typename A, typename B> struct elem_pow_impl<A, B, EnumETL::CACHE> {
  static UETLI_INLINE auto constexpr eval(A &&a, B &&b)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> ::uetli::cache::uetliExprBinaryOp_elem_pow<
          utils::hash<std::size_t>('p', 'o', 'w',
                                   uetli::remove_all<A>::type::tag,
                                   uetli::remove_all<B>::type::tag),
          A, B>
#endif
  {
    return ::uetli::cache::uetliExprBinaryOp_elem_pow<
        utils::hash<std::size_t>('p', 'o', 'w', uetli::remove_all<A>::type::tag,
                                 uetli::remove_all<B>::type::tag),
        A, B>(std::forward<A>(a), std::forward<B>(b));
  }
};

/** @brief
 *  Unitary operator cache object
 */
#define UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME, ...)           \
  /** @brief                                                                   \
   *  Selector for specialized CACHE implementation of                         \
   *  uetli::elem_##OPNAME##<A>(A&& a) function                                \
   */                                                                          \
  template <typename A>                                                        \
  struct get_elem_##OPNAME##_impl<                                             \
      A, typename uetli::enable_if_type_of<A, EnumETL::CACHE>::type>           \
      : public std::integral_constant<EnumETL, EnumETL::CACHE> {};             \
                                                                               \
  /** @brief                                                                   \
   *  Element-wise OPNAME function for CACHE types                             \
   */                                                                          \
  template <typename A> struct elem_##OPNAME##_impl<A, EnumETL::CACHE> {       \
    static UETLI_INLINE auto eval(A &&a)                                       \
        -> decltype(::uetli::cache::uetliExprUnaryOp_elem_##OPNAME<            \
                    utils::hash<std::size_t>(__VA_ARGS__,                      \
                                             uetli::remove_all<A>::type::tag), \
                    A>(std::forward<A>(a))) {                                  \
      return ::uetli::cache::uetliExprUnaryOp_elem_##OPNAME<                   \
          utils::hash<std::size_t>(__VA_ARGS__,                                \
                                   uetli::remove_all<A>::type::tag),           \
          A>(std::forward<A>(a));                                              \
    }                                                                          \
  };

UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(abs, 'a', 'b', 's')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos, 'a', 'c', 'o', 's')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh, 'a', 'c', 'o', 's', 'h')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin, 'a', 's', 'i', 'n')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh, 'a', 's', 'i', 'n', 'h')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan, 'a', 't', 'a', 'n')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh, 'a', 't', 'a', 'n', 'h')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil, 'c', 'e', 'i', 'l')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(conj, 'c', 'o', 'n', 'j')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos, 'c', 'o', 's')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh, 'c', 'o', 's', 'h')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf, 'e', 'r', 'f')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc, 'e', 'r', 'f', 'c')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp, 'e', 'x', 'p')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10, 'e', 'x', 'p', '1', '0')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2, 'e', 'x', 'p', '2')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(fabs, 'f', 'a', 'b', 's')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor, 'f', 'l', 'o', 'o', 'r')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(imag, 'i', 'm', 'a', 'g')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log, 'l', 'o', 'g')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10, 'l', 'o', 'g', '1', '0')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2, 'l', 'o', 'g', '2')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(real, 'r', 'e', 'a', 'l')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round, 'r', 'o', 'u', 'n', 'd')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt, 'r', 's', 'q', 'r', 't')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign, 's', 'i', 'g', 'n')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin, 's', 'i', 'n')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh, 's', 'i', 'n', 'h')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt, 's', 'q', 'r', 't')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan, 't', 'a', 'n')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh, 't', 'a', 'n', 'h')
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc, 't', 'r', 'u', 'n', 'c')

#undef UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

} // namespace internal

#ifdef DOXYGEN
} // namespace uetli
#endif

#endif // BACKEND_CACHE_HPP
