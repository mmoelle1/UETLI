/** @file itpp.hpp
 *
 *  @brief Implementation details for IT++ library
 *
 *  This file implements a generic expression template engine which
 *  wraps all calls to the underlying low-level expression template
 *  library into a unfied API. It also provides a user-transparent
 *  mechanism to store smart pointers (std::shared_ptr) to temporary
 *  expression returned from the low-level expression template
 *  library, which in some cases would lead to dangling pointers.
 *
 *  @copyright This file is part of the UETLI library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 *
 *  @note
 *  IT++ does not require a specialization for make_temp since the
 *  result of any vector/matrix expression is automatically stored as
 *  vector/matrix. Consider the example code:
 *  \code{.cpp}
 *  itpp::Vec<double> v1(10),v2(10);
 *  auto w = v1+v2;
 *  \endcode
 *  The type of \c w is \c itpp::Vec<double>.
 */
#pragma once
#ifndef BACKEND_ITPP_HPP
#define BACKEND_ITPP_HPP

#ifdef SUPPORT_ITPP

#include <type_traits>

#ifdef DOXYGEN
namespace uetli {
#endif

/** @brief
 *  If T is of type EnumETL::ITPP_VECTOR, provides the member constant
 *  value equal to true. Otherwise value is false.
 */
template <typename T>
struct is_type_of<
    T, EnumETL::ITPP_VECTOR,
    typename std::enable_if<std::is_base_of<
        itpp::Vec<typename uetli::remove_all<T>::type::value_type>,
        typename uetli::remove_all<T>::type>::value>::type>
    : public std::true_type {};

/** @brief
 *  If T is of type EnumETL::ITPP_MATRIX, provides the member constant
 *  value equal to true. Otherwise value is false.
 */
template <typename T>
struct is_type_of<
    T, EnumETL::ITPP_MATRIX,
    typename std::enable_if<std::is_base_of<
        itpp::Mat<typename uetli::remove_all<T>::type::value_type>,
        typename uetli::remove_all<T>::type>::value>::type>
    : public std::true_type {};

/** @brief
 *  If T is of type EnumETL::ITPP, provides the member constant
 *  value equal to true. Otherwise value is false.
 */
template <typename T>
struct is_type_of<
    T, EnumETL::ITPP,
    typename std::enable_if<
        std::is_base_of<
            itpp::Vec<typename uetli::remove_all<T>::type::value_type>,
            typename uetli::remove_all<T>::type>::value ||
        std::is_base_of<
            itpp::Mat<typename uetli::remove_all<T>::type::value_type>,
            typename uetli::remove_all<T>::type>::value>::type>
    : public std::true_type {};

/** @brief
 *  Result type of the expression (IT++ type)
 */
template <typename Expr>
struct result_type<
    Expr, typename uetli::enable_if_type_of<Expr, EnumETL::ITPP>::type> {
  using type = typename uetli::remove_all<Expr>::type;
};

/** @brief
 *  Scalar value type of the expression (IT++ type)
 */
template <typename Expr>
struct value_type<
    Expr, typename uetli::enable_if_type_of<Expr, EnumETL::ITPP>::type> {
  using type = typename uetli::remove_all<Expr>::type::value_type;
};

namespace internal {

/** @brief
 *  Selector for specialized IT++ implementation of
 *  uetli::elem_mul<A,B>(A&& a, B&& b) function
 */
template <typename A, typename B>
struct get_elem_mul_impl<
    A, B,
    typename std::enable_if<std::is_same<
        decltype(itpp::elem_mult(std::declval<A>(), std::declval<B>())),
        decltype(itpp::elem_mult(std::declval<A>(),
                                 std::declval<B>()))>::value>::type>
    : public std::integral_constant<EnumETL, EnumETL::ITPP> {};

/** @brief
 *  Element-wise multiplication of IT++ types
 */
template <typename A, typename B> struct elem_mul_impl<A, B, EnumETL::ITPP> {
  static UETLI_INLINE auto constexpr eval(A &&a, B &&b)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(itpp::elem_mult(std::forward<A>(a), std::forward<B>(b)))
#endif
  {
    return itpp::elem_mult(std::forward<A>(a), std::forward<B>(b));
  }
};

/** @brief
 *  Selector for specialized IT++ implementation of
 *  uetli::elem_div<A,B>(A&& a, B&& b) function
 */
template <typename A, typename B>
struct get_elem_div_impl<
    A, B,
    typename std::enable_if<std::is_same<
        decltype(itpp::elem_div(std::declval<A>(), std::declval<B>())),
        decltype(itpp::elem_div(std::declval<A>(),
                                std::declval<B>()))>::value>::type>
    : public std::integral_constant<EnumETL, EnumETL::ITPP> {};

/** @brief
 *  Element-wise division of IT++ types
 */
template <typename A, typename B> struct elem_div_impl<A, B, EnumETL::ITPP> {
  static UETLI_INLINE auto constexpr eval(A &&a, B &&b)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(itpp::elem_div(std::forward<A>(a), std::forward<B>(b)))
#endif
  {
    return itpp::elem_div(std::forward<A>(a), std::forward<B>(b));
  }
};

/// Helper macro for generating element-wise unary operations
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
#define UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                \
  /** @brief                                                                   \
   *  Selector for specialized IT++ vector implementation of                   \
   *  uetli::elem_##OPNAME##<A>(A&& a) function                                \
   */                                                                          \
  template <typename A>                                                        \
  struct get_elem_##OPNAME##_impl<                                             \
      A, typename uetli::enable_if_type_of<A, EnumETL::ITPP_VECTOR>::type>     \
      : public std::integral_constant<EnumETL, EnumETL::ITPP_VECTOR> {};       \
                                                                               \
  /** @brief                                                                   \
   *  Element-wise OPNAME function for IT++ vector types                       \
   */                                                                          \
  template <typename A> struct elem_##OPNAME##_impl<A, EnumETL::ITPP_VECTOR> { \
    static UETLI_INLINE auto eval(A &&a) ->                                    \
        typename uetli::remove_all<A>::type {                                  \
      typename uetli::remove_all<A>::type temp(a.length());                    \
      _Pragma("omp parallel for shared(temp)") for (auto i = 0;                \
                                                    i < a.length(); i++)       \
          temp[i] = std::OPNAME(a(i));                                         \
      return temp;                                                             \
    }                                                                          \
  };                                                                           \
                                                                               \
  /** @brief                                                                   \
   *  Selector for specialized IT++ matrix implementation of                   \
   *  uetli::elem_##OPNAME##<A>(A&& a) function                                \
   */                                                                          \
  template <typename A>                                                        \
  struct get_elem_##OPNAME##_impl<                                             \
      A, typename uetli::enable_if_type_of<A, EnumETL::ITPP_MATRIX>::type>     \
      : public std::integral_constant<EnumETL, EnumETL::ITPP_MATRIX> {};       \
                                                                               \
  /** @brief                                                                   \
   *  Element-wise OPNAME function for IT++ matrix types                       \
   */                                                                          \
  template <typename A> struct elem_##OPNAME##_impl<A, EnumETL::ITPP_MATRIX> { \
    static UETLI_INLINE auto eval(A &&a) ->                                    \
        typename uetli::remove_all<A>::type {                                  \
      typename uetli::remove_all<A>::type temp(a.rows(), a.cols());            \
      _Pragma("omp parallel for shared(temp) collapse(2)") for (               \
          auto i = 0; i < a.rows(); i++) for (auto j = 0; j < a.cols(); j++)   \
          temp[i, j] = std::OPNAME(a(i, j));                                   \
      return temp;                                                             \
    }                                                                          \
  };
#else
#define UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                \
  /** @brief                                                                   \
   *  Selector for specialized IT++ vector implementation of                   \
   *  uetli::elem_##OPNAME##<A>(A&& a) function                                \
   */                                                                          \
  template <typename A>                                                        \
  struct get_elem_##OPNAME##_impl<                                             \
      A, typename uetli::enable_if_type_of<A, EnumETL::ITPP_VECTOR>::type>     \
      : public std::integral_constant<EnumETL, EnumETL::ITPP_VECTOR> {};       \
                                                                               \
  /** @brief                                                                   \
   *  Element-wise OPNAME function for IT++ vector types                       \
   */                                                                          \
  template <typename A> struct elem_##OPNAME##_impl<A, EnumETL::ITPP_VECTOR> { \
    static UETLI_INLINE auto eval(A &&a) {                                     \
      typename uetli::remove_all<A>::type temp(a.length());                    \
      _Pragma("omp parallel for shared(temp)") for (auto i = 0;                \
                                                    i < a.length(); i++)       \
          temp[i] = std::OPNAME(a(i));                                         \
      return temp;                                                             \
    }                                                                          \
  };                                                                           \
                                                                               \
  /** @brief                                                                   \
   *  Selector for specialized IT++ matrix implementation of                   \
   *  uetli::elem_##OPNAME##<A>(A&& a) function                                \
   */                                                                          \
  template <typename A>                                                        \
  struct get_elem_##OPNAME##_impl<                                             \
      A, typename uetli::enable_if_type_of<A, EnumETL::ITPP_MATRIX>::type>     \
      : public std::integral_constant<EnumETL, EnumETL::ITPP_MATRIX> {};       \
                                                                               \
  /** @brief                                                                   \
   *  Element-wise OPNAME function for IT++ matrix types                       \
   */                                                                          \
  template <typename A> struct elem_##OPNAME##_impl<A, EnumETL::ITPP_MATRIX> { \
    static UETLI_INLINE auto eval(A &&a) {                                     \
      typename uetli::remove_all<A>::type temp(a.rows(), a.cols());            \
      _Pragma("omp parallel for shared(temp) collapse(2)") for (               \
          auto i = 0; i < a.rows(); i++) for (auto j = 0; j < a.cols(); j++)   \
          temp[i, j] = std::OPNAME(a(i, j));                                   \
      return temp;                                                             \
    }                                                                          \
  };
#endif

UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(abs)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil)
// UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(conj)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp)
// UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(fabs)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(imag)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(real)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round)
// UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt)
// UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc)

#undef UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

/** @brief
 *  Selector for specialized IT++ vector implementation of
 *  uetli::elem_exp10<A>(A&& a) function
 */
template <typename A>
struct get_elem_exp10_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::ITPP_VECTOR>::type>
    : public std::integral_constant<EnumETL, EnumETL::ITPP_VECTOR> {};

/** @brief
 *  Element-wise exp10(x) function for x of IT++ vector types
 */
template <typename A> struct elem_exp10_impl<A, EnumETL::ITPP_VECTOR> {
  static UETLI_INLINE auto eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> typename uetli::remove_all<A>::type
#endif
  {
    typename uetli::remove_all<A>::type temp(a.length());
#pragma omp parallel for shared(temp)
    for (auto i = 0; i < a.length(); i++)
      temp[i] = std::exp(
          a(i) * 2.302585092994045684017991454684364207601101488628772976033);
    return temp;
  }
};

/** @brief
 *  Selector for specialized IT++ matrix implementation of
 *  uetli::elem_exp10<A>(A&& a) function
 */
template <typename A>
struct get_elem_exp10_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::ITPP_MATRIX>::type>
    : public std::integral_constant<EnumETL, EnumETL::ITPP_MATRIX> {};

/** @brief
 *  Element-wise exp10(x) function for x of IT++ matrix types
 */
template <typename A> struct elem_exp10_impl<A, EnumETL::ITPP_MATRIX> {
  static UETLI_INLINE auto eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> typename uetli::remove_all<A>::type
#endif
  {
    typename uetli::remove_all<A>::type temp(a.rows(), a.cols());
#pragma omp parallel for shared(temp) collapse(2)
    for (auto i = 0; i < a.rows(); i++)
      for (auto j = 0; j < a.cols(); j++)
        temp[i, j] = std::exp(
            a(i) * 2.302585092994045684017991454684364207601101488628772976033);
    return temp;
  }
};

/** @brief
 *  Selector for specialized IT++ vector implementation of
 *  uetli::elem_sign<A>(A&& a) function
 */
template <typename A>
struct get_elem_sign_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::ITPP_VECTOR>::type>
    : public std::integral_constant<EnumETL, EnumETL::ITPP_VECTOR> {};

/** @brief
 *  Element-wise sign(x) function for x of IT++ vector types
 */
template <typename A> struct elem_sign_impl<A, EnumETL::ITPP_VECTOR> {
  static UETLI_INLINE auto eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> typename uetli::remove_all<A>::type
#endif
  {
    typename uetli::remove_all<A>::type temp(a.length());
#pragma omp parallel for shared(temp)
    for (auto i = 0; i < a.length(); i++)
      temp[i] = (a(i) > 0.0 ? 1.0 : a(i) < 0.0 ? -1.0 : 0.0);
    return temp;
  }
};

/** @brief
 *  Selector for specialized IT++ matrix implementation of
 *  uetli::elem_sign<A>(A&& a) function
 */
template <typename A>
struct get_elem_sign_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::ITPP_MATRIX>::type>
    : public std::integral_constant<EnumETL, EnumETL::ITPP_MATRIX> {};

/** @brief
 *  Element-wise sign(x) function for x of IT++ matrix types
 */
template <typename A> struct elem_sign_impl<A, EnumETL::ITPP_MATRIX> {
  static UETLI_INLINE auto eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> typename uetli::remove_all<A>::type
#endif
  {
    typename uetli::remove_all<A>::type temp(a.rows(), a.cols());
#pragma omp parallel for shared(temp) collapse(2)
    for (auto i = 0; i < a.rows(); i++)
      for (auto j = 0; j < a.cols(); j++)
        temp[i, j] = (a(i, j) > 0.0 ? 1.0 : a(i, j) < 0.0 ? -1.0 : 0.0);
    return temp;
  }
};

/** @brief
 *  Selector for specialized IT++ vector implementation of
 *  uetli::elem_conj<A>(A&& a) function
 */
template <typename A>
struct get_elem_conj_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::ITPP_VECTOR>::type>
    : public std::integral_constant<EnumETL, EnumETL::ITPP_VECTOR> {};

/** @brief
 *  Element-wise complex conjugate of IT++ vector types
 */
template <typename A> struct elem_conj_impl<A, EnumETL::ITPP_VECTOR> {
  static UETLI_INLINE auto eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> typename uetli::remove_all<A>::type
#endif
  {
    typename uetli::remove_all<A>::type temp(a.length());
#pragma omp parallel for shared(temp)
    for (auto i = 0; i < a.length(); i++)
      temp[i] = std::to_type<typename uetli::remove_all<A>::type::value_type>(
          std::conj(a(i)));
    return temp;
  }
};

/** @brief
 *  Selector for specialized IT++ matrix implementation of
 *  uetli::elem_conj<A>(A&& a) function
 */
template <typename A>
struct get_elem_conj_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::ITPP_MATRIX>::type>
    : public std::integral_constant<EnumETL, EnumETL::ITPP_MATRIX> {};

/** @brief
 *  Element-wise complex conjugate of IT++ matrix types
 */
template <typename A> struct elem_conj_impl<A, EnumETL::ITPP_MATRIX> {
  static UETLI_INLINE auto eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> typename uetli::remove_all<A>::type
#endif
  {
    typename uetli::remove_all<A>::type temp(a.rows(), a.cols());
#pragma omp parallel for shared(temp) collapse(2)
    for (auto i = 0; i < a.rows(); i++)
      for (auto j = 0; j < a.cols(); j++)
        temp[i, j] =
            std::to_type<typename uetli::remove_all<A>::type::value_type>(
                std::conj(a(i, j)));
    return temp;
  }
};

/** @brief
 *  Selector for specialized IT++ vector implementation of
 *  uetli::elem_rsqrt<A>(A&& a) function
 */
template <typename A>
struct get_elem_rsqrt_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::ITPP_VECTOR>::type>
    : public std::integral_constant<EnumETL, EnumETL::ITPP_VECTOR> {};

/** @brief
 *  Element-wise rsqrt(x) function for x of IT++ vector types
 */
template <typename A> struct elem_rsqrt_impl<A, EnumETL::ITPP_VECTOR> {
  static UETLI_INLINE auto eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> typename uetli::remove_all<A>::type
#endif
  {
    typename uetli::remove_all<A>::type temp(a.length());
#pragma omp parallel for shared(temp)
    for (auto i = 0; i < a.length(); i++)
      temp[i] = std::pow(a(i), -0.5);
    return temp;
  }
};

/** @brief
 *  Selector for specialized IT++ matrix implementation of
 *  uetli::elem_rsqrt<A>(A&& a) function
 */
template <typename A>
struct get_elem_rsqrt_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::ITPP_MATRIX>::type>
    : public std::integral_constant<EnumETL, EnumETL::ITPP_MATRIX> {};

/** @brief
 *  Element-wise rsqrt(x) function for x of IT++ matrix types
 */
template <typename A> struct elem_rsqrt_impl<A, EnumETL::ITPP_MATRIX> {
  static UETLI_INLINE auto eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> typename uetli::remove_all<A>::type
#endif
  {
    typename uetli::remove_all<A>::type temp(a.rows(), a.cols());
#pragma omp parallel for shared(temp) collapse(2)
    for (auto i = 0; i < a.rows(); i++)
      for (auto j = 0; j < a.cols(); j++)
        temp[i, j] = std::pow(a(i, j), -0.5);
    return temp;
  }
};

/** @brief
 *  Selector for specialized IT++ vector implementation of
 *  uetli::elem_pow<A,B>(A&& a, B&& b) function
 */
template <typename A, typename B>
struct get_elem_pow_impl<
    A, B,
    typename uetli::enable_if_type_of_and_cond<
        A, EnumETL::ITPP_VECTOR, std::is_arithmetic<B>::value>::type>
    : public std::integral_constant<EnumETL, EnumETL::ITPP_VECTOR> {};

/** @brief
 *  Element-wise pow(x,y) function for x of IT++ vector types
 */
template <typename A, typename B>
struct elem_pow_impl<A, B, EnumETL::ITPP_VECTOR> {
  static UETLI_INLINE auto eval(A &&a, B &&b)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> typename uetli::remove_all<A>::type
#endif
  {
    typename uetli::remove_all<A>::type temp(a.length());
#pragma omp parallel for shared(temp)
    for (auto i = 0; i < a.length(); i++)
      temp[i] = std::pow(a(i), b);
    return temp;
  }
};

/** @brief
 *  Selector for specialized IT++ matrix implementation of
 *  uetli::elem_pow<A,B>(A&& a, B&& b) function
 */
template <typename A, typename B>
struct get_elem_pow_impl<
    A, B,
    typename uetli::enable_if_type_of_and_cond<
        A, EnumETL::ITPP_MATRIX, std::is_arithmetic<B>::value>::type>
    : public std::integral_constant<EnumETL, EnumETL::ITPP_MATRIX> {};

/** @brief
 *  Element-wise pow(x,y) function for x of IT++ matrix types
 */
template <typename A, typename B>
struct elem_pow_impl<A, B, EnumETL::ITPP_MATRIX> {
  static UETLI_INLINE auto eval(A &&a, B &&b)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> typename uetli::remove_all<A>::type
#endif
  {
    typename uetli::remove_all<A>::type temp(a.rows(), a.cols());
#pragma omp parallel for shared(temp) collapse(2)
    for (auto i = 0; i < a.rows(); i++)
      for (auto j = 0; j < a.cols(); j++)
        temp[i, j] = std::pow(a(i, j), b);
    return temp;
  }
};

#ifdef DOXYGEN
} // namespace internal
#endif

} // namespace internal

#endif // SUPPORT_ITPP
#endif // BACKEND_ITPP_HPP
