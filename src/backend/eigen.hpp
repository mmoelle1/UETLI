/** @file eigen.hpp
 *
 *  @brief Implementation details for Eigen library
 *
 *  This file implements a generic expression template engine which
 *  wraps all calls to the underlying low-level expression template
 *  library into a unfied API. It also provides a user-transparent
 *  mechanism to store smart pointers (std::shared_ptr) to temporary
 *  expression returned from the low-level expression template
 *  library, which in some cases would lead to dangling pointers.
 *
 *  @copyright This file is part of the UETLI library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef BACKEND_EIGEN_HPP
#define BACKEND_EIGEN_HPP

#ifdef SUPPORT_EIGEN

#include <type_traits>

#ifdef DOXYGEN
namespace uetli {
#endif

/** @brief
 *  If T is of type EnumETL::EIGEN, provides the member constant
 *  value equal to true. Otherwise value is false.
 */
template <typename T>
struct is_type_of<T, EnumETL::EIGEN,
                  typename std::enable_if<std::is_base_of<
                      Eigen::EigenBase<typename uetli::remove_all<T>::type>,
                      typename uetli::remove_all<T>::type>::value>::type>
    : public std::true_type {};

/// Detect Eigen base-type wrapped in G+Smo
template <typename T>
struct is_type_of<
    T, EnumETL::EIGEN,
    typename std::enable_if<std::is_base_of<
        Eigen::EigenBase<typename uetli::remove_all<T>::type::Base>,
        typename uetli::remove_all<T>::type::Base>::value>::type>
    : public std::true_type {};

/** @brief
 *  If T is of type EnumETL::EIGEN_MATRIX, provides the member constant
 *  value equal to true. Otherwise value is false.
 */
template <typename T>
struct is_type_of<T, EnumETL::EIGEN_MATRIX,
                  typename std::enable_if<std::is_base_of<
                      Eigen::MatrixBase<typename uetli::remove_all<T>::type>,
                      typename uetli::remove_all<T>::type>::value>::type>
    : public std::true_type {};

/// Detect Eigen matrix-type wrapped in G+Smo
template <typename T>
struct is_type_of<
    T, EnumETL::EIGEN_MATRIX,
    typename std::enable_if<std::is_base_of<
        Eigen::MatrixBase<typename uetli::remove_all<T>::type::Base>,
        typename uetli::remove_all<T>::type::Base>::value>::type>
    : public std::true_type {};

/** @brief
 *  If T is of type EnumETL::EIGEN_ARRAY, provides the member constant
 *  value equal to true. Otherwise value is false.
 */
template <typename T>
struct is_type_of<T, EnumETL::EIGEN_ARRAY,
                  typename std::enable_if<std::is_base_of<
                      Eigen::ArrayBase<typename uetli::remove_all<T>::type>,
                      typename uetli::remove_all<T>::type>::value>::type>
    : public std::true_type {};

/// Detect Eigen array-type wrapped in G+Smo
template <typename T>
struct is_type_of<
    T, EnumETL::EIGEN_ARRAY,
    typename std::enable_if<std::is_base_of<
        Eigen::ArrayBase<typename uetli::remove_all<T>::type::Base>,
        typename uetli::remove_all<T>::type::Base>::value>::type>
    : public std::true_type {};

/** @brief
 *  Result type of the expression (Eigen type)
 */
template <typename Expr>
struct result_type<
    Expr, typename uetli::enable_if_type_of<Expr, EnumETL::EIGEN>::type> {
  using type = typename uetli::remove_all<Expr>::type::PlainObject;
};

/** @brief
 *  Scalar value type of the expression (Eigen type)
 */
template <typename Expr>
struct value_type<
    Expr, typename uetli::enable_if_type_of<Expr, EnumETL::EIGEN>::type> {
  using type = typename uetli::remove_all<Expr>::type::Scalar;
};

namespace internal {

/** @brief
 *  Indicator for specialized Eigen implementation of
 *  uetli::internal::make_temp_impl<Tag,Expr>(Expr&& expr) function
 */
template <std::size_t Tag, typename Expr>
struct has_make_temp_impl<
    Tag, Expr, typename uetli::enable_if_type_of<Expr, EnumETL::EIGEN>::type>
    : public std::true_type {};

/** @brief
 *  Eigen type creation from expressions
 */
template <std::size_t Tag, typename Expr>
static UETLI_INLINE auto make_temp_impl(Expr &&expr)
#if !defined(DOXYGEN)
    -> typename uetli::enable_if_type_of<
        Expr, EnumETL::EIGEN, typename uetli::result_type<Expr>::type>::type
#endif
{
  using Temp = typename uetli::result_type<Expr>::type;
  return Temp(std::forward<Expr>(expr));
}

/** @brief
 *  Selector for specialized Eigen::Matrix implementation of
 *  uetli::elem_mul<A,B>(A&& a, B&& b) function
 */
template <typename A, typename B>
struct get_elem_mul_impl<
    A, B,
    typename uetli::enable_if_any_type_of<A, B, EnumETL::EIGEN_MATRIX>::type>
    : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX> {};

/** @brief
 *  Element-wise multiplication of Eigen::Matrix types
 */
template <typename A, typename B>
struct elem_mul_impl<A, B, EnumETL::EIGEN_MATRIX> {
  static UETLI_INLINE auto constexpr eval(A &&a, B &&b)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype((a.array() * b.array()).matrix())
#endif
  {
    return (a.array() * b.array()).matrix();
  }
};

/** @brief
 *  Selector for specialized Eigen::Matrix implementation of
 *  uetli::elem_div<A,B>(A&& a, B&& b) function
 */
template <typename A, typename B>
struct get_elem_div_impl<
    A, B,
    typename uetli::enable_if_any_type_of<A, B, EnumETL::EIGEN_MATRIX>::type>
    : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX> {};

/** @brief
 *  Element-wise division of Eigen::Matrix types
 */
template <typename A, typename B>
struct elem_div_impl<A, B, EnumETL::EIGEN_MATRIX> {
  static UETLI_INLINE auto constexpr eval(A &&a, B &&b)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype((a.array() / b.array()).matrix())
#endif
  {
    return (a.array() / b.array()).matrix();
  }
};

/** @brief
 *  Selector for specialized Eigen::Matrix implementation of
 *  uetli::elem_pow<A,B>(A&& a, B&& b) function
 */
template <typename A, typename B>
struct get_elem_pow_impl<
    A, B, typename uetli::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>
    : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX> {};

/** @brief
 *  Element-wise pow-function of Eigen::Matrix types
 */
template <typename A, typename B>
struct elem_pow_impl<A, B, EnumETL::EIGEN_MATRIX> {
  static UETLI_INLINE auto constexpr eval(A &&a, B &&b)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(pow(a.array(), b).matrix())
#endif
  {
    return pow(a.array(), b).matrix();
  }
};

/** @brief
 *  Selector for specialized Eigen::Array implementation of
 *  uetli::elem_fabs<A>(A&& a) function
 */
template <typename A>
struct get_elem_fabs_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::EIGEN_ARRAY>::type>
    : public std::integral_constant<EnumETL, EnumETL::EIGEN_ARRAY> {};

/** @brief
 *  Element-wise absolute value of Eigen::Array type
 *
 *  @note
 *  Eigen does not distinguish between abs and fabs.
 */
template <typename A> struct elem_fabs_impl<A, EnumETL::EIGEN_ARRAY> {
  static UETLI_INLINE auto constexpr eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(abs(std::forward<A>(a)))
#endif
  {
    return abs(std::forward<A>(a));
  }
};

/** @brief
 *  Selector for specialized Eigen::Matrix implementation of
 *  uetli::elem_fabs<A>(A&& a) function
 */
template <typename A>
struct get_elem_fabs_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>
    : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX> {};

/** @brief
 *  Element-wise absolute value of Eigen::Matrix type
 *
 *  @note
 *  Eigen does not distinguish between abs and fabs.
 */
template <typename A> struct elem_fabs_impl<A, EnumETL::EIGEN_MATRIX> {
  static UETLI_INLINE auto constexpr eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(abs(a.array()).matrix())
#endif
  {
    return abs(a.array()).matrix();
  }
};

/** @brief
 *  Selector for specialized Eigen::Array implementation of
 *  uetli::elem_exp10<A>(A&& a) function
 */
template <typename A>
struct get_elem_exp10_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::EIGEN_ARRAY>::type>
    : public std::integral_constant<EnumETL, EnumETL::EIGEN_ARRAY> {};

/** @brief
 *  Element-wise exp10(x) function for x of Eigen::Array type
 */
template <typename A> struct elem_exp10_impl<A, EnumETL::EIGEN_ARRAY> {
  static UETLI_INLINE auto constexpr eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(
          exp(std::forward<A>(a) *
              2.302585092994045684017991454684364207601101488628772976033))
#endif
  {
    return exp(std::forward<A>(a) *
               2.302585092994045684017991454684364207601101488628772976033);
  }
};

/** @brief
 *  Selector for specialized Eigen::Matrix implementation of
 *  uetli::elem_exp10<A>(A&& a) function
 */
template <typename A>
struct get_elem_exp10_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>
    : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX> {};

/** @brief
 *  Element-wise exp10(x) function for x of Eigen::Matrix type
 */
template <typename A> struct elem_exp10_impl<A, EnumETL::EIGEN_MATRIX> {
  static UETLI_INLINE auto constexpr eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(
          exp(a.array() *
              2.302585092994045684017991454684364207601101488628772976033)
              .matrix())
#endif
  {
    return exp(a.array() *
               2.302585092994045684017991454684364207601101488628772976033)
        .matrix();
  }
};

/** @brief
 *  Selector for specialized Eigen::Array implementation of
 *  uetli::elem_exp2<A>(A&& a) function
 */
template <typename A>
struct get_elem_exp2_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::EIGEN_ARRAY>::type>
    : public std::integral_constant<EnumETL, EnumETL::EIGEN_ARRAY> {};

/** @brief
 *  Element-wise exp2(x) function for x of Eigen::Array type
 */
template <typename A> struct elem_exp2_impl<A, EnumETL::EIGEN_ARRAY> {
  static UETLI_INLINE auto constexpr eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(
          exp(std::forward<A>(a) *
              0.693147180559945309417232121458176568075500134360255254120))
#endif
  {
    return exp(std::forward<A>(a) *
               0.693147180559945309417232121458176568075500134360255254120);
  }
};

/** @brief
 *  Selector for specialized Eigen::Matrix implementation of
 *  uetli::elem_exp2<A>(A&& a) function
 */
template <typename A>
struct get_elem_exp2_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>
    : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX> {};

/** @brief
 *  Element-wise exp2(x) function for x of Eigen::Matrix type
 */
template <typename A> struct elem_exp2_impl<A, EnumETL::EIGEN_MATRIX> {
  static UETLI_INLINE auto constexpr eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(
          exp(a.array() *
              0.693147180559945309417232121458176568075500134360255254120)
              .matrix())
#endif
  {
    return exp(a.array() *
               0.693147180559945309417232121458176568075500134360255254120)
        .matrix();
  }
};

/** @brief
 *  Selector for specialized Eigen::Array implementation of
 *  uetli::elem_rsqrt<A>(A&& a) function
 */
template <typename A>
struct get_elem_rsqrt_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::EIGEN_ARRAY>::type>
    : public std::integral_constant<EnumETL, EnumETL::EIGEN_ARRAY> {};

/** @brief
 *  Element-wise inverse square root of Eigen::Array type
 */
template <typename A> struct elem_rsqrt_impl<A, EnumETL::EIGEN_ARRAY> {
  static UETLI_INLINE auto constexpr eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(pow(std::forward<A>(a), -0.5))
#endif
  {
    return pow(std::forward<A>(a), -0.5);
  }
};

/** @brief
 *  Selector for specialized Eigen::Matrix implementation of
 *  uetli::elem_rsqrt<A>(A&& a) function
 */
template <typename A>
struct get_elem_rsqrt_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>
    : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX> {};

/** @brief
 *  Element-wise inverse square root of Eigen::Matrix type
 */
template <typename A> struct elem_rsqrt_impl<A, EnumETL::EIGEN_MATRIX> {
  static UETLI_INLINE auto constexpr eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(pow(a.array(), -0.5).matrix())
#endif
  {
    return pow(a.array(), -0.5).matrix();
  }
};

/** @brief
 *  Selector for specialized Eigen::Array implementation of
 *  uetli::elem_log2<A>(A&& a) function
 */
template <typename A>
struct get_elem_log2_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::EIGEN_ARRAY>::type>
    : public std::integral_constant<EnumETL, EnumETL::EIGEN_ARRAY> {};

/** @brief
 *  Element-wise log2(x) function for x of Eigen::Array type
 */
template <typename A> struct elem_log2_impl<A, EnumETL::EIGEN_ARRAY> {
  static UETLI_INLINE auto constexpr eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(log(std::forward<A>(a)) *
                  1.442695040888963407359924681001892137426645954152985934135)
#endif
  {
    return log(std::forward<A>(a)) *
           1.442695040888963407359924681001892137426645954152985934135;
  }
};

/** @brief
 *  Selector for specialized Eigen::Matrix implementation of
 *  uetli::elem_log2<A>(A&& a) function
 */
template <typename A>
struct get_elem_log2_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>
    : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX> {};

/** @brief
 *  Element-wise log2(x) function for x of Eigen::Matrix type
 */
template <typename A> struct elem_log2_impl<A, EnumETL::EIGEN_MATRIX> {
  static UETLI_INLINE auto constexpr eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(log(a.array()).matrix() *
                  1.442695040888963407359924681001892137426645954152985934135)
#endif
  {
    return log(a.array()).matrix() *
           1.442695040888963407359924681001892137426645954152985934135;
  }
};

/// Helper macro for generating element-wise unary operations
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
#define UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                \
  /** @brief                                                                   \
   *  Selector for specialized Eigen::Matrix implementation of                 \
   *  uetli::elem_##OPNAME##<A>(A&& a) function                                \
   */                                                                          \
  template <typename A>                                                        \
  struct get_elem_##OPNAME##_impl<                                             \
      A, typename uetli::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>    \
      : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX> {};      \
                                                                               \
  /** @brief                                                                   \
   *  Element-wise OPNAME function for Eigen::Matrix types                     \
   */                                                                          \
  template <typename A>                                                        \
  struct elem_##OPNAME##_impl<A, EnumETL::EIGEN_MATRIX> {                      \
    static UETLI_INLINE auto constexpr eval(A &&a)                             \
        -> decltype(a.array().OPNAME().matrix()) {                             \
      return a.array().OPNAME().matrix();                                      \
    }                                                                          \
  };
#else
#define UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                \
  /** @brief                                                                   \
   *  Selector for specialized Eigen::Matrix implementation of                 \
   *  uetli::elem_##OPNAME##<A>(A&& a) function                                \
   */                                                                          \
  template <typename A>                                                        \
  struct get_elem_##OPNAME##_impl<                                             \
      A, typename uetli::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>    \
      : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX> {};      \
                                                                               \
  /** @brief                                                                   \
   *  Element-wise OPNAME function for Eigen::Matrix types                     \
   */                                                                          \
  template <typename A>                                                        \
  struct elem_##OPNAME##_impl<A, EnumETL::EIGEN_MATRIX> {                      \
    static UETLI_INLINE auto constexpr eval(A &&a) {                           \
      return a.array().OPNAME().matrix();                                      \
    }                                                                          \
  };
#endif

UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(abs)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil)
// UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(conj)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp)
// UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10)
// UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor)
// UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(imag)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10)
// UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2)
// UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(real)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round)
// UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc)

#undef UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

/** @brief
 *  Selector for specialized Eigen::Array implementation of
 *  uetli::elem_conj<A>(A&& a) function
 */
template <typename A>
struct get_elem_conj_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::EIGEN_ARRAY>::type>
    : public std::integral_constant<EnumETL, EnumETL::EIGEN_ARRAY> {};

/** @brief
 *  Element-wise complex conjugate function for Eigen::Array types
 */
template <typename A> struct elem_conj_impl<A, EnumETL::EIGEN_ARRAY> {
  static UETLI_INLINE auto
#if defined(SUPPORT_CXX14)
      constexpr
#endif
      eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
          -> decltype(std::forward<A>(a))
#endif
  {
    throw std::logic_error("No implementation of conj function available");
    return std::forward<A>(a);
  }
};

/** @brief
 *  Selector for specialized Eigen::Matrix implementation of
 *  uetli::elem_conj<A>(A&& a) function
 */
template <typename A>
struct get_elem_conj_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>
    : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX> {};

/** @brief
 *  Element-wise complex conjugate function for Eigen::Matrix types
 */
template <typename A> struct elem_conj_impl<A, EnumETL::EIGEN_MATRIX> {
  static UETLI_INLINE auto
#if defined(SUPPORT_CXX14)
      constexpr
#endif
      eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
          -> decltype(std::forward<A>(a))
#endif
  {
    throw std::logic_error("No implementation of conj function available");
    return std::forward<A>(a);
  }
};

/** @brief
 *  Selector for specialized Eigen::Array implementation of
 *  uetli::elem_imag<A>(A&& a) function
 */
template <typename A>
struct get_elem_imag_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::EIGEN_ARRAY>::type>
    : public std::integral_constant<EnumETL, EnumETL::EIGEN_ARRAY> {};

/** @brief
 *  Element-wise imag function for Eigen::Array types
 */
template <typename A> struct elem_imag_impl<A, EnumETL::EIGEN_ARRAY> {
  static UETLI_INLINE auto
#if defined(SUPPORT_CXX14)
      constexpr
#endif
      eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
          -> decltype(std::forward<A>(a))
#endif
  {
    throw std::logic_error("No implementation of imag function available");
    return std::forward<A>(a);
  }
};

/** @brief
 *  Selector for specialized Eigen::Matrix implementation of
 *  uetli::elem_imag<A>(A&& a) function
 */
template <typename A>
struct get_elem_imag_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>
    : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX> {};

/** @brief
 *  Element-wise imag function for Eigen::Matrix types
 */
template <typename A> struct elem_imag_impl<A, EnumETL::EIGEN_MATRIX> {
  static UETLI_INLINE auto
#if defined(SUPPORT_CXX14)
      constexpr
#endif
      eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
          -> decltype(std::forward<A>(a))
#endif
  {
    throw std::logic_error("No implementation of imag function available");
    return std::forward<A>(a);
  }
};

/** @brief
 *  Selector for specialized Eigen::Array implementation of
 *  uetli::elem_real<A>(A&& a) function
 */
template <typename A>
struct get_elem_real_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::EIGEN_ARRAY>::type>
    : public std::integral_constant<EnumETL, EnumETL::EIGEN_ARRAY> {};

/** @brief
 *  Element-wise real function for Eigen::Array types
 */
template <typename A> struct elem_real_impl<A, EnumETL::EIGEN_ARRAY> {
  static UETLI_INLINE auto
#if defined(SUPPORT_CXX14)
      constexpr
#endif
      eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
          -> decltype(std::forward<A>(a))
#endif
  {
    throw std::logic_error("No implementation of real function available");
    return std::forward<A>(a);
  }
};

/** @brief
 *  Selector for specialized Eigen::Matrix implementation of
 *  uetli::elem_real<A>(A&& a) function
 */
template <typename A>
struct get_elem_real_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>
    : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX> {};

/** @brief
 *  Element-wise real function for Eigen::Matrix types
 */
template <typename A> struct elem_real_impl<A, EnumETL::EIGEN_MATRIX> {
  static UETLI_INLINE auto
#if defined(SUPPORT_CXX14)
      constexpr
#endif
      eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
          -> decltype(std::forward<A>(a))
#endif
  {
    throw std::logic_error("No implementation of real function available");
    return std::forward<A>(a);
  }
};

} // namespace internal

#ifdef DOXYGEN
} // namespace uetli
#endif

#endif // SUPPORT_EIGEN
#endif // BACKEND_EIGEN_HPP
