/** @file vexcl.hpp
 *
 *  @brief Implementation details for VexCL library
 *
 *  This file implements a generic expression template engine which
 *  wraps all calls to the underlying low-level expression template
 *  library into a unfied API. It also provides a user-transparent
 *  mechanism to store smart pointers (std::shared_ptr) to temporary
 *  expression returned from the low-level expression template
 *  library, which in some cases would lead to dangling pointers.
 *
 *  @copyright This file is part of the UETLI library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef BACKEND_VEXCL_HPP
#define BACKEND_VEXCL_HPP

#ifdef SUPPORT_VEXCL

#include <type_traits>

#ifdef DOXYGEN
namespace uetli {
#endif

/** @brief
 *  If T is of type EnumETL::VEXCL, provides the member constant
 *  value equal to true. Otherwise value is false.
 */
template <typename T>
struct is_type_of<
    T, EnumETL::VEXCL,
    typename std::enable_if<(vex::is_vector_expression<T>::value ||
                             vex::is_multivector_expression<T>::value) &&
                            !std::is_arithmetic<T>::value>::type>
    : public std::true_type {};

/** @brief
 *  Result type of the expression (VexCL type)
 */
template <typename Expr>
struct result_type<
    Expr, typename uetli::enable_if_type_of<Expr, EnumETL::VEXCL>::type> {
  using type = typename boost::proto::result_of::as_expr<
      typename uetli::remove_all<Expr>::type>::type;
};

/** @brief
 *  Scalar value type of the expression (VexCL type)
 */
template <typename Expr>
struct value_type<
    Expr, typename uetli::enable_if_type_of<Expr, EnumETL::VEXCL>::type> {
  using type = typename uetli::remove_all<Expr>::type::value_type;
};

namespace internal {

/** @brief
 *  Indicator for specialized VexCL implementation of
 *  uetli::internal::make_constant_impl<T,Expr>(const T value, Expr&& expr)
 * function
 */
template <typename Expr>
struct has_make_constant_impl<
    Expr, typename uetli::enable_if_type_of<Expr, uetli::EnumETL::VEXCL>::type>
    : public std::true_type {};

/** @brief
 *  Create VexCL constant
 */
template <typename TC, typename T, typename Expr>
static UETLI_INLINE auto make_constant_impl(const T value, Expr &&expr)
#if !defined(DOXYGEN)
    -> typename uetli::enable_if_type_of<
        Expr, EnumETL::VEXCL,
        decltype(boost::proto::as_expr<vex::vector_domain>(
            vex::user_constant<TC>()))>::type
#endif
{
  return boost::proto::as_expr<vex::vector_domain>(vex::user_constant<TC>());
}

/** @brief
 *  Indicator for specialized VexCL implementation of
 *  uetli::internal::make_temp_impl<Tag,Expr>(Expr&& expr) function
 *
 *  @note
 *  Identification of VexCL types that can be converted into temporary
 *  is non-trivial. This implementation uses the same detection
 *  mechanism as used internally by VexCL.
 */
template <std::size_t Tag, typename Expr>
struct has_make_temp_impl<
    Tag, Expr,
    typename std::enable_if<
        std::is_same<
            typename std::enable_if<
                boost::proto::matches<
                    typename boost::proto::result_of::as_expr<
                        typename uetli::remove_all<Expr>::type>::type,
                    vex::vector_expr_grammar>::value,
                vex::temporary<
                    typename vex::detail::return_type<
                        typename uetli::remove_all<Expr>::type>::type,
                    Tag,
                    typename boost::proto::result_of::as_child<
                        const typename uetli::remove_all<Expr>::type,
                        vex::vector_domain>::type> const>::type,
            typename std::enable_if<
                boost::proto::matches<
                    typename boost::proto::result_of::as_expr<
                        typename uetli::remove_all<Expr>::type>::type,
                    vex::vector_expr_grammar>::value,
                vex::temporary<
                    typename vex::detail::return_type<
                        typename uetli::remove_all<Expr>::type>::type,
                    Tag,
                    typename boost::proto::result_of::as_child<
                        const typename uetli::remove_all<Expr>::type,
                        vex::vector_domain>::type> const>::type>::value &&
        !std::is_arithmetic<typename uetli::remove_all<Expr>::type>::value>::
        type> : public std::true_type {};

/** @brief
 *  VexCL type creation from expressions
 *
 *  @note
 *  VexCL has a dedicated vex::make_temp<Tag,Expr>(Expr expr)
 *  implementation, which returns a dedicated temporal
 *  type. Therefore, this trait  does not make use of the
 *  uetli::result_type<Expr> trait to create a temporal.
 */
template <std::size_t Tag, typename Expr>
static UETLI_INLINE auto make_temp_impl(Expr &&expr)
#if !defined(DOXYGEN)
    -> typename uetli::enable_if_type_of<
        Expr, EnumETL::VEXCL,
        decltype(vex::make_temp<Tag>(std::forward<Expr>(expr)))>::type
#endif
{
  return vex::make_temp<Tag>(std::forward<Expr>(expr));
}

/** @brief
 *  Indicator for specialized VexCL implementation of
 *  uetli::internal::tag_impl<Tag,Expr>(Expr&& expr) function
 *
 *  @note
 *  Identification of VexCL types that can be tagged is
 *  non-trivial. This implementation uses the same detection mechanism
 *  as used internally by VexCL.
 */
template <std::size_t Tag, typename Expr>
struct has_tag_impl<
    Tag, Expr,
    typename std::enable_if<
        std::is_same<
            vex::tagged_terminal<
                Tag, decltype(boost::proto::as_child<vex::vector_domain>(
                         std::declval<Expr>()))>,
            vex::tagged_terminal<
                Tag, decltype(boost::proto::as_child<vex::vector_domain>(
                         std::declval<Expr>()))>>::value &&
        !std::is_arithmetic<Expr>::value>::type> : public std::true_type {};

/** @brief
 *  Tags terminal with a unique (in a single expression) tag.
 *
 *  By tagging terminals user guarantees that the terminals with same
 *  tags actually refer to the same data. VexCL is able to use this
 *  information in order to reduce number of kernel parameters and
 *  unnecessary global memory I/O operations.
 */
template <std::size_t Tag, typename Expr>
static UETLI_INLINE auto tag_impl(Expr &&expr)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
    -> decltype(vex::tag<Tag>(std::forward<Expr>(expr)))
#endif
{
  return vex::tag<Tag>(std::forward<Expr>(expr));
}

/** @brief
 *  Selector for specialized VexCL implementation of
 *  uetli::elem_conj<A>(A&& a) function
 */
template <typename A>
struct get_elem_conj_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::VEXCL>::type>
    : public std::integral_constant<EnumETL, EnumETL::VEXCL> {};

/** @brief
 *  Element-wise complex conjugate value of VexCL types
 */
template <typename A> struct elem_conj_impl<A, EnumETL::VEXCL> {
  static UETLI_INLINE auto
#if defined(SUPPORT_CXX14)
      constexpr
#endif
      eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
          -> decltype(std::forward<A>(a))
#endif
  {
    throw std::logic_error("No implementation of conj function available");
    return std::forward<A>(a);
  }
};

/** @brief
 *  Selector for specialized VexCL implementation of
 *  uetli::elem_imag<A>(A&& a) function
 */
template <typename A>
struct get_elem_imag_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::VEXCL>::type>
    : public std::integral_constant<EnumETL, EnumETL::VEXCL> {};

/** @brief
 *  Element-wise imaginare part of complex value of VexCL types
 */
template <typename A> struct elem_imag_impl<A, EnumETL::VEXCL> {
  static UETLI_INLINE auto
#if defined(SUPPORT_CXX14)
      constexpr
#endif
      eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
          -> decltype(std::forward<A>(a))
#endif
  {
    throw std::logic_error("No implementation of imag function available");
    return std::forward<A>(a);
  }
};

/** @brief
 *  Selector for specialized VexCL implementation of
 *  uetli::elem_real<A>(A&& a) function
 */
template <typename A>
struct get_elem_real_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::VEXCL>::type>
    : public std::integral_constant<EnumETL, EnumETL::VEXCL> {};

/** @brief
 *  Element-wise real part of complex value of VexCL types
 */
template <typename A> struct elem_real_impl<A, EnumETL::VEXCL> {
  static UETLI_INLINE auto
#if defined(SUPPORT_CXX14)
      constexpr
#endif
      eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
          -> decltype(std::forward<A>(a))
#endif
  {
    throw std::logic_error("No implementation of real function available");
    return std::forward<A>(a);
  }
};

} // namespace internal

#ifdef DOXYGEN
} // namespace uetli
#endif

#endif // SUPPORT_VEXCL
#endif // BACKEND_VEXCL_HPP
