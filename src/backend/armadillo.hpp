/** @file armadillo.hpp
 *
 *  @brief Implementation details for Armadillo library
 *
 *  This file implements a generic expression template engine which
 *  wraps all calls to the underlying low-level expression template
 *  library into a unfied API. It also provides a user-transparent
 *  mechanism to store smart pointers (std::shared_ptr) to temporary
 *  expression returned from the low-level expression template
 *  library, which in some cases would lead to dangling pointers.
 *
 *  @copyright This file is part of the UETLI library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef BACKEND_ARMADILLO_HPP
#define BACKEND_ARMADILLO_HPP

#ifdef SUPPORT_ARMADILLO

#include <type_traits>

#include <armadillo>

#ifdef DOXYGEN
namespace uetli {
#endif

/** @brief
 *  If T is of type EnumETL::ARMADILLO, provides the member constant
 *  value equal to true. Otherwise value is false.
 */
template <typename T>
struct is_type_of<
    T, EnumETL::ARMADILLO,
    typename std::enable_if<
        arma::is_arma_type<typename uetli::remove_all<T>::type>::value ||
        arma::is_arma_cube_type<typename uetli::remove_all<T>::type>::value ||
        arma::is_arma_sparse_type<typename uetli::remove_all<T>::type>::value>::
        type> : public std::true_type {};

/** @brief
 *  Result type of the expression (dense column vector of Armadillo type)
 */
template <typename Expr>
struct result_type<Expr, typename uetli::enable_if_type_of_and_cond<
                             Expr, EnumETL::ARMADILLO,
                             uetli::remove_all<Expr>::type::is_col>::type> {
  using type =
      typename arma::Col<typename uetli::remove_all<Expr>::type::elem_type>;
};

/** @brief
 *  Result type of the expression (dense row vector of Armadillo type)
 */
template <typename Expr>
struct result_type<Expr, typename uetli::enable_if_type_of_and_cond<
                             Expr, EnumETL::ARMADILLO,
                             uetli::remove_all<Expr>::type::is_row>::type> {
  using type =
      typename arma::Row<typename uetli::remove_all<Expr>::type::elem_type>;
};

/** @brief
 *  Result type of the expression (dense matrix of Armadillo type)
 */
template <typename Expr>
struct result_type<
    Expr,
    typename uetli::enable_if_type_of_and_cond<
        Expr, EnumETL::ARMADILLO,
        arma::is_arma_type<typename uetli::remove_all<Expr>::type>::value &&
            !(uetli::remove_all<Expr>::type::is_col ||
              uetli::remove_all<Expr>::type::is_row)>::type> {
  using type =
      typename arma::Mat<typename uetli::remove_all<Expr>::type::elem_type>;
};

/** @brief
 *  Result type of the expression (sparse matrix of Armadillo type)
 */
template <typename Expr>
struct result_type<
    Expr, typename uetli::enable_if_type_of_and_cond<
              Expr, EnumETL::ARMADILLO,
              arma::is_arma_sparse_type<
                  typename uetli::remove_all<Expr>::type>::value>::type> {
  using type =
      typename arma::SpMat<typename uetli::remove_all<Expr>::type::elem_type>;
};

/** @brief
 *  Result type of the expression (2D matrix of Armadillo type)
 */
template <typename Expr>
struct result_type<Expr, typename uetli::enable_if_type_of_and_cond<
                             Expr, EnumETL::ARMADILLO,
                             arma::is_arma_cube_type<typename uetli::remove_all<
                                 Expr>::type>::value>::type> {
  using type =
      typename arma::Cube<typename uetli::remove_all<Expr>::type::elem_type>;
};

/** @brief
 *  Scalar value type of the expression (Armadillo type)
 */
template <typename Expr>
struct value_type<
    Expr, typename uetli::enable_if_type_of<Expr, EnumETL::ARMADILLO>::type> {
  using type = typename uetli::remove_all<Expr>::type::elem_type;
};

namespace internal {

/** @brief
 *  Indicator for specialized Armadillo implementation of
 *  uetli::internal::make_temp_impl<Tag,Expr>(Expr&& expr) function
 */
template <std::size_t Tag, typename Expr>
struct has_make_temp_impl<
    Tag, Expr,
    typename uetli::enable_if_type_of<Expr, EnumETL::ARMADILLO>::type>
    : public std::true_type {};

/** @brief
 *  Armadillo terminal type creation from expressions
 */
template <std::size_t Tag, typename Expr>
static UETLI_INLINE auto make_temp_impl(Expr &&expr)
#if !defined(DOXYGEN)
    -> typename uetli::enable_if_type_of<
        Expr, EnumETL::ARMADILLO, typename uetli::result_type<Expr>::type>::type
#endif
{
  using Temp = typename uetli::result_type<Expr>::type;
  return Temp(std::forward<Expr>(expr));
}

/** @brief
 *  Selector for specialized Armadillo implementation of
 *  uetli::elem_mul<A,B>(A&& a, B&& b) function
 */
template <typename A, typename B>
struct get_elem_mul_impl<
    A, B, typename uetli::enable_if_any_type_of<A, B, EnumETL::ARMADILLO>::type>
    : public std::integral_constant<EnumETL, EnumETL::ARMADILLO> {};

/** @brief
 *  Element-wise multiplication of Armadillo types
 */
template <typename A, typename B>
struct elem_mul_impl<A, B, EnumETL::ARMADILLO> {
  static UETLI_INLINE auto constexpr eval(A &&a, B &&b)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(std::forward<A>(a) % std::forward<B>(b))
#endif
  {
    return std::forward<A>(a) % std::forward<B>(b);
  }
};

/** @brief
 *  Selector for specialized Armadillo implementation of
 *  uetli::elem_fabs<A>(A&& a) function
 */
template <typename A>
struct get_elem_fabs_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::ARMADILLO>::type>
    : public std::integral_constant<EnumETL, EnumETL::ARMADILLO> {};

/** @brief
 *  Element-wise absolute value of Armadillo types
 *
 *  @note
 *  Armadillo does not distinguish between abs and fabs.
 */
template <typename A> struct elem_fabs_impl<A, EnumETL::ARMADILLO> {
  static UETLI_INLINE auto constexpr eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(abs(std::forward<A>(a)))
#endif
  {
    return abs(std::forward<A>(a));
  }
};

/** @brief
 *  Selector for specialized Armadillo implementation of
 *  uetli::elem_rsqrt<A>(A&& a) function
 */
template <typename A>
struct get_elem_rsqrt_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::ARMADILLO>::type>
    : public std::integral_constant<EnumETL, EnumETL::ARMADILLO> {};

/** @brief
 *  Element-wise absolute value of Armadillo types
 *
 *  @note
 *  Armadillo does not provide rsqrt.
 */
template <typename A> struct elem_rsqrt_impl<A, EnumETL::ARMADILLO> {
  static UETLI_INLINE auto constexpr eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(pow(std::forward<A>(a), -0.5))
#endif
  {
    return pow(std::forward<A>(a), -0.5);
  }
};

} // namespace internal

#ifdef DOXYGEN
} // namespace uetli
#endif

#endif // SUPPORT_ARMADILLO
#endif // BACKEND_ARMADILLO_HPP
