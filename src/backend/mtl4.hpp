/** @file mtl4.hpp
 *
 *  @brief Implementation details for MTL4/CMTL4 library
 *
 *  This file implements a generic expression template engine which
 *  wraps all calls to the underlying low-level expression template
 *  library into a unfied API. It also provides a user-transparent
 *  mechanism to store smart pointers (std::shared_ptr) to temporary
 *  expression returned from the low-level expression template
 *  library, which in some cases would lead to dangling pointers.
 *
 *  @copyright This file is part of the UETLI library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef BACKEND_MTL4_HPP
#define BACKEND_MTL4_HPP

#if defined(SUPPORT_CMTL4) || defined(SUPPORT_MTL4)

#include <type_traits>

#ifdef DOXYGEN
namespace uetli {
#endif

/** @brief
 *  If T is of type EnumETL::MTL4_VECTOR, provides the member constant
 *  value equal to true. Otherwise value is false.
 */
template <typename T>
struct is_type_of<T, EnumETL::MTL4_VECTOR,
                  typename std::enable_if<mtl::traits::is_vector<
                      typename uetli::remove_all<T>::type>::value>::type>
    : public std::true_type {};

/** @brief
 *  If T is of type EnumETL::MTL4_MATRIX, provides the member constant
 *  value equal to true. Otherwise value is false.
 */
template <typename T>
struct is_type_of<T, EnumETL::MTL4_MATRIX,
                  typename std::enable_if<mtl::traits::is_matrix<
                      typename uetli::remove_all<T>::type>::value>::type>
    : public std::true_type {};

/** @brief
 *  If T is of type EnumETL::MTL4, provides the member constant
 *  value equal to true. Otherwise value is false.
 */
template <typename T>
struct is_type_of<
    T, EnumETL::MTL4,
    typename std::enable_if<
        mtl::traits::is_vector<typename uetli::remove_all<T>::type>::value ||
        mtl::traits::is_matrix<typename uetli::remove_all<T>::type>::value>::
        type> : public std::true_type {};

/** @brief
 *  Result type of the expression (MTL4 vector type)
 */
template <typename Expr>
struct result_type<
    Expr, typename uetli::enable_if_type_of<Expr, EnumETL::MTL4_VECTOR>::type> {
  using type = mtl::vector<typename uetli::value_type<Expr>::type>;
};

/** @brief
 *  Result type of the expression (MTL4 dense matrix type)
 */
template <typename Expr>
struct result_type<Expr, typename uetli::enable_if_type_of_and_cond<
                             Expr, EnumETL::MTL4_MATRIX,
                             !mtl::traits::is_sparse<typename uetli::remove_all<
                                 Expr>::type>::value>::type> {
  using type = mtl::matrix<typename uetli::value_type<Expr>::type>;
};

/** @brief
 *  Result type of the expression (MTL4 sparse matrix type)
 */
template <typename Expr>
struct result_type<Expr, typename uetli::enable_if_type_of_and_cond<
                             Expr, EnumETL::MTL4_MATRIX,
                             mtl::traits::is_sparse<typename uetli::remove_all<
                                 Expr>::type>::value>::type> {
  using type = mtl::compressed2D<typename uetli::value_type<Expr>::type>;
};

/** @brief
 *  Scalar value type of the expression (MTL4 type)
 */
template <typename Expr>
struct value_type<
    Expr, typename uetli::enable_if_type_of<Expr, EnumETL::MTL4>::type> {
  using type = typename uetli::remove_all<Expr>::type::value_type;
};

namespace internal {

/** @brief
 *  Indicator for specialized MTL4 implementation of
 *  uetli::internal::make_temp_impl<Tag,Expr>(Expr&& expr) function
 */
template <std::size_t Tag, typename Expr>
struct has_make_temp_impl<
    Tag, Expr, typename uetli::enable_if_type_of<Expr, EnumETL::MTL4>::type>
    : public std::true_type {};

/** @brief
 *  MTL4 type creation from expressions
 */
template <std::size_t Tag, typename Expr>
static UETLI_INLINE auto make_temp_impl(Expr &&expr)
#if !defined(DOXYGEN)
    -> typename uetli::enable_if_type_of<
        Expr, EnumETL::MTL4, typename uetli::result_type<Expr>::type>::type
#endif
{
  using Temp = typename uetli::result_type<Expr>::type;
  return Temp(std::forward<Expr>(expr));
}

/** @brief
 *  Selector for specialized MTL4 vector implementation of
 *  uetli::elem_mul<A,B>(A&& a, B&& b) function
 */
template <typename A, typename B>
struct get_elem_mul_impl<
    A, B,
    typename uetli::enable_if_all_type_of<A, B, EnumETL::MTL4_VECTOR>::type>
    : public std::integral_constant<EnumETL, EnumETL::MTL4_VECTOR> {};

/** @brief
 *  Element-wise multiplication of MTL4 vector types
 */
template <typename A, typename B>
struct elem_mul_impl<A, B, EnumETL::MTL4_VECTOR> {
  static UETLI_INLINE auto constexpr eval(A &&a, B &&b)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(mtl::vec::ele_prod(std::forward<A>(a), std::forward<B>(b)))
#endif
  {
    return mtl::vec::ele_prod(std::forward<A>(a), std::forward<B>(b));
  }
};

/** @brief
 *  Selector for specialized MTL4 matrix implementation of
 *  uetli::elem_mul<A,B>(A&& a, B&& b) function
 */
template <typename A, typename B>
struct get_elem_mul_impl<
    A, B,
    typename uetli::enable_if_all_type_of<A, B, EnumETL::MTL4_MATRIX>::type>
    : public std::integral_constant<EnumETL, EnumETL::MTL4_MATRIX> {};

/** @brief
 *  Element-wise multiplication of MTL4 matrix types
 */
template <typename A, typename B>
struct elem_mul_impl<A, B, EnumETL::MTL4_MATRIX> {
  static UETLI_INLINE auto constexpr eval(A &&a, B &&b)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(mtl::mat::ele_prod(std::forward<A>(a), std::forward<B>(b)))
#endif
  {
    return mtl::mat::ele_prod(std::forward<A>(a), std::forward<B>(b));
  }
};

/** @brief
 *  Selector for specialized MTL4 vector implementation of
 *  uetli::elem_div<A,B>(A&& a, B&& b) function
 */
template <typename A, typename B>
struct get_elem_div_impl<
    A, B,
    typename uetli::enable_if_all_type_of<A, B, EnumETL::MTL4_VECTOR>::type>
    : public std::integral_constant<EnumETL, EnumETL::MTL4_VECTOR> {};

/** @brief
 *  Element-wise division of MTL4 vector types
 */
template <typename A, typename B>
struct elem_div_impl<A, B, EnumETL::MTL4_VECTOR> {
  static UETLI_INLINE auto constexpr eval(A &&a, B &&b)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(mtl::vec::ele_quot(std::forward<A>(a), std::forward<B>(b)))
#endif
  {
    return mtl::vec::ele_quot(std::forward<A>(a), std::forward<B>(b));
  }
};

/// Helper macro for generating element-wise unary operations
#define UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                \
                                                                               \
  /** @brief                                                                   \
   *  Selector for specialized MTL4 matrix implementation of                   \
   *  uetli::elem_##OPNAME##<A>(A&& a) function                                \
   */                                                                          \
  template <typename A>                                                        \
  struct get_elem_##OPNAME##_impl<                                             \
      A, typename uetli::enable_if_type_of<A, EnumETL::MTL4_MATRIX>::type>     \
      : public std::integral_constant<EnumETL, EnumETL::MTL4_MATRIX> {};       \
                                                                               \
  /** @brief                                                                   \
   *  Element-wise OPNAME function for MTL4 matrix types                       \
   */                                                                          \
  template <typename A> struct elem_##OPNAME##_impl<A, EnumETL::MTL4_MATRIX> { \
    static UETLI_INLINE auto eval(A &&a) -> typename std::enable_if<           \
        mtl::traits::is_vector<A>::value,                                      \
        typename mtl::matrix<                                                  \
            typename uetli::remove_all<A>::type::value_type>>::type {          \
      using namespace std;                                                     \
      using Temp = typename mtl::matrix<                                       \
          typename uetli::remove_all<A>::type::value_type>;                    \
      auto temp = Temp(std::forward<A>(a));                                    \
      _Pragma("omp parallel for shared(temp) collapse(2)") for (               \
          auto i = 0; i < mtl::num_cols(temp);                                 \
          i++) for (auto j = 0; j < mtl::num_rows(temp); j++) temp[i, j] =     \
          OPNAME(temp[i, j]);                                                  \
      return temp;                                                             \
    }                                                                          \
  };

UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(abs)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(conj)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(fabs)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(imag)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(real)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc)

#undef UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

/** @brief
 *  Selector for specialized MTL4 vector implementation of
 *  uetli::elem_fabs<A>(A&& a) function
 */
template <typename A>
struct get_elem_fabs_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::MTL4>::type>
    : public std::integral_constant<EnumETL, EnumETL::MTL4> {};

/** @brief
 *  Element-wise fabs function for MTL4 types
 */
template <typename A> struct elem_fabs_impl<A, EnumETL::MTL4> {
  static UETLI_INLINE auto eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(abs(std::forward<A>(a)))
#endif
  {
    return abs(std::forward<A>(a));
  }
};

/** @brief
 *  Selector for specialized MTL4 implementation of
 *  uetli::elem_sign<A>(A&& a) function
 */
template <typename A>
struct get_elem_sign_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::MTL4>::type>
    : public std::integral_constant<EnumETL, EnumETL::MTL4> {};

/** @brief
 *  Element-wise sign function for MTL4 types
 */
template <typename A> struct elem_sign_impl<A, EnumETL::MTL4> {
  static UETLI_INLINE auto eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(signum(std::forward<A>(a)))
#endif
  {
    return signum(std::forward<A>(a));
  }
};

} // namespace internal

#ifdef DOXYGEN
} // namespace uetli
#endif

#endif // SUPPORT CMTL4 || SUPPORT_MTL4
#endif // BACKEND_MTL4_HPP
