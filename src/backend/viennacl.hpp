/** @file viennacl.hpp
 *
 *  @brief Implementation details for ViennaCL library
 *
 *  This file implements a generic expression template engine which
 *  wraps all calls to the underlying low-level expression template
 *  library into a unfied API. It also provides a user-transparent
 *  mechanism to store smart pointers (std::shared_ptr) to temporary
 *  expression returned from the low-level expression template
 *  library, which in some cases would lead to dangling pointers.
 *
 *  @copyright This file is part of the UETLI library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef BACKEND_VIENNACL_HPP
#define BACKEND_VIENNACL_HPP

#ifdef SUPPORT_VIENNACL

#include <type_traits>

#ifdef DOXYGEN
namespace uetli {
#endif

/** @brief
 *  If T is of type EnumETL::VIENNACL, provides the member constant
 *  value equal to true. Otherwise value is false.
 */
template <typename T>
struct is_type_of<
    T, EnumETL::VIENNACL,
    typename std::enable_if<
        std::is_same<typename viennacl::result_of::cpu_value_type<
                         typename uetli::remove_all<T>::type>::type,
                     typename viennacl::result_of::cpu_value_type<
                         typename uetli::remove_all<T>::type>::type>::value &&
        !std::is_arithmetic<T>::value>::type> : public std::true_type {};

/** @brief
 *  Result type of the expression (ViennaCL scalar type)
 */
template <typename Expr>
struct result_type<Expr, typename uetli::enable_if_type_of_and_cond<
                             Expr, EnumETL::VIENNACL,
                             viennacl::is_any_scalar<typename uetli::remove_all<
                                 Expr>::type>::value>::type> {
  using type =
      typename viennacl::scalar<typename uetli::value_type<Expr>::type>;
};

/** @brief
 *  Result type of the expression (ViennaCL vector or vector expression type)
 */
template <typename Expr>
struct result_type<
    Expr,
    typename uetli::enable_if_type_of_and_cond<
        Expr, EnumETL::VIENNACL,
        viennacl::is_any_vector<
            typename uetli::remove_all<Expr>::type>::value ||
            viennacl::is_any_vector<decltype(
                viennacl::vector<typename uetli::value_type<Expr>::type>(
                    std::declval<typename uetli::remove_all<Expr>::type>()))>::
                value>::type> {
  using type =
      typename viennacl::vector<typename uetli::value_type<Expr>::type>;
};

/** @brief
 *  Result type of the expression (ViennaCL dense matrix or matrix expression
 * type)
 */
template <typename Expr>
struct result_type<
    Expr,
    typename uetli::enable_if_type_of_and_cond<
        Expr, EnumETL::VIENNACL,
        viennacl::is_any_dense_matrix<
            typename uetli::remove_all<Expr>::type>::value ||
            viennacl::is_any_dense_matrix<decltype(
                viennacl::matrix<typename uetli::value_type<Expr>::type>(
                    std::declval<typename uetli::remove_all<Expr>::type>()))>::
                value>::type> {
  using type =
      typename viennacl::matrix<typename uetli::value_type<Expr>::type>;
};

/** @brief
 *  Result type of the expression (ViennaCL compressed matrix type)
 */
template <typename Expr>
struct result_type<
    Expr, typename uetli::enable_if_type_of_and_cond<
              Expr, EnumETL::VIENNACL,
              viennacl::is_compressed_matrix<
                  typename uetli::remove_all<Expr>::type>::value>::type> {
  using type = typename viennacl::compressed_matrix<
      typename uetli::value_type<Expr>::type>;
};

/** @brief
 *  Result type of the expression (ViennaCL coordinate matrix type)
 */
template <typename Expr>
struct result_type<
    Expr, typename uetli::enable_if_type_of_and_cond<
              Expr, EnumETL::VIENNACL,
              viennacl::is_coordinate_matrix<
                  typename uetli::remove_all<Expr>::type>::value>::type> {
  using type = typename viennacl::coordinate_matrix<
      typename uetli::value_type<Expr>::type>;
};

/** @brief
 * Result type of the expression (ViennaCL ELL matrix type)
 */
template <typename Expr>
struct result_type<Expr, typename uetli::enable_if_type_of_and_cond<
                             Expr, EnumETL::VIENNACL,
                             viennacl::is_ell_matrix<typename uetli::remove_all<
                                 Expr>::type>::value>::type> {
  using type =
      typename viennacl::ell_matrix<typename uetli::value_type<Expr>::type>;
};

/** @brief
 *  Result type of the expression (ViennaCL hybrid matrix type)
 */
template <typename Expr>
struct result_type<Expr, typename uetli::enable_if_type_of_and_cond<
                             Expr, EnumETL::VIENNACL,
                             viennacl::is_hyb_matrix<typename uetli::remove_all<
                                 Expr>::type>::value>::type> {
  using type =
      typename viennacl::hyb_matrix<typename uetli::value_type<Expr>::type>;
};

/** @brief
 *  Result type of the expression (ViennaCL sliced ELL matrix type)
 */
template <typename Expr>
struct result_type<
    Expr, typename uetli::enable_if_type_of_and_cond<
              Expr, EnumETL::VIENNACL,
              viennacl::is_sliced_ell_matrix<
                  typename uetli::remove_all<Expr>::type>::value>::type> {
  using type = typename viennacl::sliced_ell_matrix<
      typename uetli::value_type<Expr>::type>;
};

/** @brief
 *  Scalar value type of the expression (ViennaCL type)
 */
template <typename Expr>
struct value_type<
    Expr, typename uetli::enable_if_type_of<Expr, EnumETL::VIENNACL>::type> {
  using type = typename viennacl::result_of::cpu_value_type<
      typename uetli::remove_all<Expr>::type>::type;
};

namespace internal {

/** @brief
 *  Indicator for specialized ViennaCL implementation of
 *  uetli::internal::make_temp_impl<Tag,Expr>(Expr&& expr) function
 */
template <std::size_t Tag, typename Expr>
struct has_make_temp_impl<
    Tag, Expr, typename uetli::enable_if_type_of<Expr, EnumETL::VIENNACL>::type>
    : public std::true_type {};

/** @brief
 *  ViennaCL vector creation from expressions
 */
template <std::size_t Tag, typename Expr>
static UETLI_INLINE auto make_temp_impl(Expr &&expr)
#if !defined(DOXYGEN)
    -> typename uetli::enable_if_type_of<
        Expr, EnumETL::VIENNACL, typename uetli::result_type<Expr>::type>::type
#endif
{
  using Temp = typename uetli::result_type<Expr>::type;
  return Temp(std::forward<Expr>(expr));
}

/// Helper macro for generating element-wise binary operations
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
#define UETLI_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(OPNAME,               \
                                                         VIENNACL_OPNAME)      \
  /** @brief                                                                   \
   *  Selector for specialized ViennaCL implementation of                      \
   *  uetli::elem_##OPNAME##<A,B>(A&& a, B&& b) function                       \
   */                                                                          \
  template <typename A, typename B>                                            \
  struct get_elem_##OPNAME##_impl<                                             \
      A, B,                                                                    \
      typename std::enable_if<std::is_same<                                    \
          decltype(viennacl::linalg::element_##VIENNACL_OPNAME(                \
              std::declval<A>(), std::declval<B>())),                          \
          decltype(viennacl::linalg::element_##VIENNACL_OPNAME(                \
              std::declval<A>(), std::declval<B>()))>::value>::type>           \
      : public std::integral_constant<EnumETL, EnumETL::VIENNACL> {};          \
                                                                               \
  /** @brief                                                                   \
   *  Element-wise OPNAME function for ViennaCL types                          \
   */                                                                          \
  template <typename A, typename B>                                            \
  struct elem_##OPNAME##_impl<A, B, EnumETL::VIENNACL> {                       \
    static UETLI_INLINE auto constexpr eval(A &&a, B &&b)                      \
        -> decltype(viennacl::linalg::element_##VIENNACL_OPNAME(               \
            std::forward<A>(a), std::forward<B>(b))) {                         \
      return viennacl::linalg::element_##VIENNACL_OPNAME(std::forward<A>(a),   \
                                                         std::forward<B>(b));  \
    }                                                                          \
  };
#else
#define UETLI_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(OPNAME,               \
                                                         VIENNACL_OPNAME)      \
  /** @brief                                                                   \
   *  Selector for specialized ViennaCL implementation of                      \
   *  uetli::elem_##OPNAME##<A,B>(A&& a, B&& b) function                       \
   */                                                                          \
  template <typename A, typename B>                                            \
  struct get_elem_##OPNAME##_impl<                                             \
      A, B,                                                                    \
      typename std::enable_if<std::is_same<                                    \
          decltype(viennacl::linalg::element_##VIENNACL_OPNAME(                \
              std::declval<A>(), std::declval<B>())),                          \
          decltype(viennacl::linalg::element_##VIENNACL_OPNAME(                \
              std::declval<A>(), std::declval<B>()))>::value>::type>           \
      : public std::integral_constant<EnumETL, EnumETL::VIENNACL> {};          \
                                                                               \
  /** @brief                                                                   \
   *  Element-wise OPNAME function for ViennaCL types                          \
   */                                                                          \
  template <typename A, typename B>                                            \
  struct elem_##OPNAME##_impl<A, B, EnumETL::VIENNACL> {                       \
    static UETLI_INLINE auto constexpr eval(A &&a, B &&b) {                    \
      return viennacl::linalg::element_##VIENNACL_OPNAME(std::forward<A>(a),   \
                                                         std::forward<B>(b));  \
    }                                                                          \
  };
#endif

UETLI_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(mul, prod)
UETLI_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(div, div)
UETLI_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(pow, pow)

#undef UETLI_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS

/// Helper macro for generating element-wise unary operations
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
#define UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                \
  /** @brief                                                                   \
   *  Selector for specialized ViennaCL implementation of                      \
   *  uetli::elem_##OPNAME##<A>(A&& a) function                                \
   */                                                                          \
  template <typename A>                                                        \
  struct get_elem_##OPNAME##_impl<                                             \
      A, typename std::enable_if<std::is_same<                                 \
             decltype(viennacl::linalg::element_##OPNAME(std::declval<A>())),  \
             decltype(viennacl::linalg::element_##OPNAME(                      \
                 std::declval<A>()))>::value>::type>                           \
      : public std::integral_constant<EnumETL, EnumETL::VIENNACL> {};          \
                                                                               \
  /** @brief                                                                   \
   *  Element-wise OPNAME function for ViennaCL types                          \
   */                                                                          \
  template <typename A> struct elem_##OPNAME##_impl<A, EnumETL::VIENNACL> {    \
    static UETLI_INLINE auto constexpr eval(A &&a)                             \
        -> decltype(viennacl::linalg::element_##OPNAME(std::forward<A>(a))) {  \
      return viennacl::linalg::element_##OPNAME(std::forward<A>(a));           \
    }                                                                          \
  };
#else
#define UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                \
  /** @brief                                                                   \
   *  Selector for specialized ViennaCL implementation of                      \
   *  uetli::elem_##OPNAME##<A>(A&& a) function                                \
   */                                                                          \
  template <typename A>                                                        \
  struct get_elem_##OPNAME##_impl<                                             \
      A, typename std::enable_if<std::is_same<                                 \
             decltype(viennacl::linalg::element_##OPNAME(std::declval<A>())),  \
             decltype(viennacl::linalg::element_##OPNAME(                      \
                 std::declval<A>()))>::value>::type>                           \
      : public std::integral_constant<EnumETL, EnumETL::VIENNACL> {};          \
                                                                               \
  /** @brief                                                                   \
   *  Element-wise OPNAME function for ViennaCL types                          \
   */                                                                          \
  template <typename A> struct elem_##OPNAME##_impl<A, EnumETL::VIENNACL> {    \
    static UETLI_INLINE auto constexpr eval(A &&a) {                           \
      return viennacl::linalg::element_##OPNAME(std::forward<A>(a));           \
    }                                                                          \
  };
#endif

UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(fabs)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc)

#undef UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

/** @brief
 *  Selector for specialized ViennaCL implementation of
 *  uetli::elem_abs<A>(A&& a) function
 */
template <typename A>
struct get_elem_abs_impl<
    A, typename std::enable_if<std::is_same<
           decltype(viennacl::linalg::element_fabs(std::declval<A>())),
           decltype(viennacl::linalg::element_fabs(
               std::declval<A>()))>::value>::type>
    : public std::integral_constant<EnumETL, EnumETL::VIENNACL> {};

/** @brief
 *  Element-wise abs(x) function for x being of ViennaCL types
 */
template <typename A> struct elem_abs_impl<A, EnumETL::VIENNACL> {
  static UETLI_INLINE auto constexpr eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(viennacl::linalg::element_fabs(std::forward<A>(a)))
#endif
  {
    return viennacl::linalg::element_fabs(std::forward<A>(a));
  }
};

/** @brief
 *  Selector for specialized ViennaCL implementation of
 *  uetli::elem_real<A>(A&& a) function
 */
template <typename A>
struct get_elem_real_impl<
    A, typename std::enable_if<std::is_same<
           decltype(viennacl::linalg::element_fabs(std::declval<A>())),
           decltype(viennacl::linalg::element_fabs(
               std::declval<A>()))>::value>::type>
    : public std::integral_constant<EnumETL, EnumETL::VIENNACL> {};

/** @brief
 *  Element-wise real(x) function for x being of ViennaCL types
 */
template <typename A> struct elem_real_impl<A, EnumETL::VIENNACL> {
  static UETLI_INLINE auto
#if defined(SUPPORT_CXX14)
      constexpr
#endif
      eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
          -> decltype(std::forward<A>(a))
#endif
  {
    throw std::logic_error("No implementation of real function available");
    return std::forward<A>(a);
  }
};

/** @brief
 *  Selector for specialized ViennaCL implementation of
 *  uetli::elem_imag<A>(A&& a) function
 */
template <typename A>
struct get_elem_imag_impl<
    A, typename std::enable_if<std::is_same<
           decltype(viennacl::linalg::element_fabs(std::declval<A>())),
           decltype(viennacl::linalg::element_fabs(
               std::declval<A>()))>::value>::type>
    : public std::integral_constant<EnumETL, EnumETL::VIENNACL> {};

/** @brief
 *  Element-wise imag(x) function for x being of ViennaCL types
 */
template <typename A> struct elem_imag_impl<A, EnumETL::VIENNACL> {
  static UETLI_INLINE auto
#if defined(SUPPORT_CXX14)
      constexpr
#endif
      eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
          -> decltype(std::forward<A>(a))
#endif
  {
    throw std::logic_error("No implementation of imag function available");
    return std::forward<A>(a);
  }
};

/** @brief
 *  Selector for specialized ViennaCL implementation of
 *  uetli::elem_conj<A>(A&& a) function
 */
template <typename A>
struct get_elem_conj_impl<
    A, typename std::enable_if<std::is_same<
           decltype(viennacl::linalg::element_fabs(std::declval<A>())),
           decltype(viennacl::linalg::element_fabs(
               std::declval<A>()))>::value>::type>
    : public std::integral_constant<EnumETL, EnumETL::VIENNACL> {};

/** @brief
 *  Element-wise conj(x) function for x being of ViennaCL types
 */
template <typename A> struct elem_conj_impl<A, EnumETL::VIENNACL> {
  static UETLI_INLINE auto
#if defined(SUPPORT_CXX14)
      constexpr
#endif
      eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
          -> decltype(std::forward<A>(a))
#endif
  {
    throw std::logic_error("No implementation of conj function available");
    return std::forward<A>(a);
  }
};

} // namespace internal

#ifdef DOXYGEN
} // namespace uetli
#endif

#endif // SUPPORT_VIENNACL
#endif // BACKEND_VIENNACL_HPP
