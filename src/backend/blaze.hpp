/** @file blaze.hpp
 *
 *  @brief Implementation details for Blaze library
 *
 *  This file implements a generic expression template engine which
 *  wraps all calls to the underlying low-level expression template
 *  library into a unfied API. It also provides a user-transparent
 *  mechanism to store smart pointers (std::shared_ptr) to temporary
 *  expression returned from the low-level expression template
 *  library, which in some cases would lead to dangling pointers.
 *
 *  @copyright This file is part of the UETLI library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef BACKEND_BLAZE_HPP
#define BACKEND_BLAZE_HPP

#ifdef SUPPORT_BLAZE

#include <type_traits>

#ifdef DOXYGEN
namespace uetli {
#endif

/** @brief
 *  If T is of type EnumETL::BLAZE, provides the member constant
 *  value equal to true. Otherwise value is false.
 */
template <typename T>
struct is_type_of<
    T, EnumETL::BLAZE,
    typename std::enable_if<blaze::Or<
        blaze::IsVector<typename uetli::remove_all<T>::type>,
        blaze::IsMatrix<typename uetli::remove_all<T>::type>>::value>::type>
    : public std::true_type {};

/** @brief
 *  Result type of the expression (Blaze type)
 */
template <typename Expr>
struct result_type<
    Expr, typename uetli::enable_if_type_of<Expr, EnumETL::BLAZE>::type> {
  using type = typename uetli::remove_all<Expr>::type::ResultType;
};

/** @brief
 *  Scalar value type of the expression (Blaze type)
 */
template <typename Expr>
struct value_type<
    Expr, typename uetli::enable_if_type_of<Expr, EnumETL::BLAZE>::type> {
  using type = typename uetli::remove_all<Expr>::type::ElementType;
};

namespace internal {

/** @brief
 *  Indicator for specialized Blaze implementation of
 *  uetli::internal::make_temp_impl<Tag,Expr>(Expr&& expr) function
 */
template <std::size_t Tag, typename Expr>
struct has_make_temp_impl<
    Tag, Expr, typename uetli::enable_if_type_of<Expr, EnumETL::BLAZE>::type>
    : public std::true_type {};

/** @brief
 *  Blaze type creation from expressions
 */
template <std::size_t Tag, typename Expr>
static UETLI_INLINE auto make_temp_impl(Expr &&expr)
#if !defined(DOXYGEN)
    -> typename uetli::enable_if_type_of<
        Expr, EnumETL::BLAZE, typename uetli::result_type<Expr>::type>::type
#endif
{
  using Temp = typename uetli::result_type<Expr>::type;
  return Temp(std::forward<Expr>(expr));
}

/** @brief
 *  Selector for specialized Blaze implementation of
 *  uetli::elem_fabs<A>(A&& a) function
 */
template <typename A>
struct get_elem_fabs_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::BLAZE>::type>
    : public std::integral_constant<EnumETL, EnumETL::BLAZE> {};

/** @brief
 *  Element-wise absolute value of Blaze types
 *
 *  @note
 *  Blaze does not distinguish between abs and fabs.
 */
template <typename A> struct elem_fabs_impl<A, EnumETL::BLAZE> {
  static UETLI_INLINE auto constexpr eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(abs(std::forward<A>(a)))
#endif
  {
    return abs(std::forward<A>(a));
  }
};

/** @brief
 *  Selector for specialized Blaze implementation of
 *  uetli::elem_rsqrt<A>(A&& a) function
 */
template <typename A>
struct get_elem_rsqrt_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::BLAZE>::type>
    : public std::integral_constant<EnumETL, EnumETL::BLAZE> {};

/** @brief
 *  Element-wise reciprocal square root of Blaze types
 */
template <typename A> struct elem_rsqrt_impl<A, EnumETL::BLAZE> {
  static UETLI_INLINE auto constexpr eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(invsqrt(std::forward<A>(a)))
#endif
  {
    return invsqrt(std::forward<A>(a));
  }
};

/** @brief
 *  Selector for specialized Blaze implementation of
 *  uetli::elem_sign<A>(A&& a) function
 */
template <typename A>
struct get_elem_sign_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::BLAZE>::type>
    : public std::integral_constant<EnumETL, EnumETL::BLAZE> {};

/** @brief
 *  Element-wise sign function to given argument of Blaze types
 */
template <typename A> struct elem_sign_impl<A, EnumETL::BLAZE> {
  static UETLI_INLINE auto constexpr eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(abs(std::forward<A>(a)))
#endif
  {
    return blaze::forEach(
        a, [](typename uetli::remove_all<A>::type::ElementType x) {
          return (x > 0.0 ? 1.0 : x < 0.0 ? -1.0 : 0.0);
        });
  }
};

} // namespace internal

#ifdef DOXYGEN
} // namespace uetli
#endif

#endif // SUPPORT_BLAZE
#endif // BACKEND_BLAZE_HPP
