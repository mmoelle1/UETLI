/** @file arrayfire.hpp
 *
 *  @brief Implementation details for ArrayFire library
 *
 *  This file implements a generic expression template engine which
 *  wraps all calls to the underlying low-level expression template
 *  library into a unfied API. It also provides a user-transparent
 *  mechanism to store smart pointers (std::shared_ptr) to temporary
 *  expression returned from the low-level expression template
 *  library, which in some cases would lead to dangling pointers.
 *
 *  @copyright This file is part of the UETLI library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef BACKEND_ARRAYFIRE_HPP
#define BACKEND_ARRAYFIRE_HPP

#ifdef SUPPORT_ARRAYFIRE

#include <type_traits>

#ifdef DOXYGEN
namespace uetli {
#endif

/** @brief
 *  If T is of type EnumETL::ARRAYFIRE, provides the member constant
 *  value equal to true. Otherwise value is false.
 */
template <typename T>
struct is_type_of<
    T, EnumETL::ARRAYFIRE,
    typename std::enable_if<std::is_base_of<
        af::array, typename uetli::remove_all<T>::type>::value>::type>
    : public std::true_type {};

/** @brief
 *  Result type of the expression (ArrayFire type)
 */
template <typename Expr>
struct result_type<
    Expr, typename uetli::enable_if_type_of<Expr, EnumETL::ARRAYFIRE>::type> {
  using type = typename af::array;
};

/** @brief
 *  Scalar value type of the expression (ArrayFire type)
 */
template <typename Expr>
struct value_type<
    Expr, typename uetli::enable_if_type_of<Expr, EnumETL::ARRAYFIRE>::type> {
  using type = double;
};

namespace internal {

/** @brief
 *  Selector for specialized ArrayFire implementation of
 *  uetli::elem_conj<A>(A&& a) function
 */
template <typename A>
struct get_elem_conj_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::ARRAYFIRE>::type>
    : public std::integral_constant<EnumETL, EnumETL::ARRAYFIRE> {};

/** @brief
 *  Element-wise conj(x) function for x of ArrayFire types
 */
template <typename A> struct elem_conj_impl<A, EnumETL::ARRAYFIRE> {
  static UETLI_INLINE auto constexpr eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(af::conjg(std::forward<A>(a)))
#endif
  {
    return af::conjg(std::forward<A>(a));
  }
};

/** @brief
 *  Selector for specialized ArrayFire implementation of
 *  uetli::elem_exp2<A>(A&& a) function
 */
template <typename A>
struct get_elem_exp2_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::ARRAYFIRE>::type>
    : public std::integral_constant<EnumETL, EnumETL::ARRAYFIRE> {};

/** @brief
 *  Element-wise exp2(x) function for x of ArrayFire types
 */
template <typename A> struct elem_exp2_impl<A, EnumETL::ARRAYFIRE> {
  static UETLI_INLINE auto constexpr eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(
          exp(std::forward<A>(a) *
              0.693147180559945309417232121458176568075500134360255254120))
#endif
  {
    return exp(std::forward<A>(a) *
               0.693147180559945309417232121458176568075500134360255254120);
  }
};

/** @brief
 *  Selector for specialized ArrayFire implementation of
 *  uetli::elem_exp10<A>(A&& a) function
 */
template <typename A>
struct get_elem_exp10_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::ARRAYFIRE>::type>
    : public std::integral_constant<EnumETL, EnumETL::ARRAYFIRE> {};

/** @brief
 *  Element-wise exp10(x) function for x of ArrayFire types
 */
template <typename A> struct elem_exp10_impl<A, EnumETL::ARRAYFIRE> {
  static UETLI_INLINE auto constexpr eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(
          exp(std::forward<A>(a) *
              2.302585092994045684017991454684364207601101488628772976033))
#endif
  {
    return exp(std::forward<A>(a) *
               2.302585092994045684017991454684364207601101488628772976033);
  }
};

/** @brief
 *  Selector for specialized ArrayFire implementation of
 *  uetli::elem_fabs<A>(A&& a) function
 */
template <typename A>
struct get_elem_fabs_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::ARRAYFIRE>::type>
    : public std::integral_constant<EnumETL, EnumETL::ARRAYFIRE> {};

/** @brief
 *  Element-wise absolute value of ArrayFire types
 *
 *  @note
 *  ArrayFire does not distinguish between abs and fabs.
 */
template <typename A> struct elem_fabs_impl<A, EnumETL::ARRAYFIRE> {
  static UETLI_INLINE auto constexpr eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(abs(std::forward<A>(a)))
#endif
  {
    return abs(std::forward<A>(a));
  }
};

/** @brief
 *  Selector for specialized ArrayFire implementation of
 *  uetli::elem_rsqrt<A>(A&& a) function
 */
template <typename A>
struct get_elem_rsqrt_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::ARRAYFIRE>::type>
    : public std::integral_constant<EnumETL, EnumETL::ARRAYFIRE> {};

/** @brief
 *  Element-wise reciprocal square root of ArrayFire types
 */
template <typename A> struct elem_rsqrt_impl<A, EnumETL::ARRAYFIRE> {
  static UETLI_INLINE auto constexpr eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(pow(std::forward<A>(a),
                      (typename uetli::value_type<A>::type) - 0.5))
#endif
  {
    return pow(std::forward<A>(a), -0.5);
  }
};

/** @brief
 *  Selector for specialized ArrayFire implementation of
 *  uetli::elem_fabs<A>(A&& a) function
 */
template <typename A>
struct get_elem_sign_impl<
    A, typename uetli::enable_if_type_of<A, EnumETL::ARRAYFIRE>::type>
    : public std::integral_constant<EnumETL, EnumETL::ARRAYFIRE> {};

/** @brief
 *  Element-wise sign(x) function for x of ArrayFire types
 *
 *  @note
 *  ArrayFire's sign(x) function checks if the input is negative. That
 *  is, it returns 1 if the input data is negative and 0
 *  otherwise. This wrapper computes the mathematical sign(x).
 */
template <typename A> struct elem_sign_impl<A, EnumETL::ARRAYFIRE> {
  static UETLI_INLINE auto constexpr eval(A &&a)
#if defined(SUPPORT_CXX11) && !defined(DOXYGEN)
      -> decltype(-sign(a) + sign(-a))
#endif
  {
    return -sign(a) + sign(-a);
  }
};

} // namespace internal

#ifdef DOXYGEN
} // namespace uetli
#endif

#endif // SUPPORT_ARRAYFIRE
#endif // BACKEND_ARRAYFIRE_HPP
