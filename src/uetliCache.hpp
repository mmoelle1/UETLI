/** @file uetliCache.hpp
 *
 *  @brief Cache class
 *
 *  @copyright This file is part of the UETLI library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef UETLI_CACHE_HPP
#define UETLI_CACHE_HPP

#include <memory>

#include "uetliCacheForward.hpp"
#include "uetliCompatibility.hpp"
#include "uetliConstant.hpp"
#include "uetliUtils.hpp"

namespace uetli {

/** @namespace uetli::cache
 *
 *  @brief
 *  The \ref uetli::cache namespace, containing the caching
 *  functionality of the UETLI library
 *
 *  The \ref uetli::cache namespace contains an implementation of a
 *  caching strategy for the UETLI library. The caching strategy is
 *  implemented as expression template wrapper library.
 *
 *  @note This Source Code Form is subject to the terms of the Mozilla
 *  Public License, v. 2.0. If a copy of the MPL was not distributed
 *  with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace cache {

/** @brief
 *  A cache object storing an object of type \ref Expr including
 *  ownership of that type
 *
 *  A cache object stores an object of type \ref Expr and has
 *  ownership of that type. The type must be created externally and
 *  passed to the cache object via its move constructor.
 */
template <std::size_t Tag, typename Expr>
struct uetliExpr : public uetliExprBase {
private:
  /// @brief Self type
  using self_type = uetliExpr<Tag, Expr>;

  /// @brief Expression type
  using expr_type = typename uetli::remove_all<Expr>::type;

public:
  /// @brief Result type
  using result_type = expr_type;

  /// @brief Tag
  static constexpr std::size_t tag = Tag;

private:
  /// @brief Raw expression data
  expr_type expr;

public:
  /// @brief Default constructor
  uetliExpr() = default;

  /// @brief Constructor (move constructor)
  uetliExpr(Expr &&expr) : expr(expr) {}

  /// @brief Returns constant reference to raw expression data
  const expr_type &get() const { return expr; }

  /// @brief Returns reference to raw expression data
  expr_type &get() { return expr; }

  /// @brief Returns pointer to raw expression data
  expr_type *operator->() { return expr; }

  /// @brief Pretty prints expression
  void print(std::ostream &os) const { os << "expr<" << Tag << ">"; }
};

/// @brief Output to string
template <std::size_t Tag, typename Expr>
std::ostream &operator<<(std::ostream &os, uetliExpr<Tag, Expr> expr) {
  expr.print(os);
  return os;
}

/** @brief
 *  A view on a cache object storing a reference to an object of type
 *  \ref Expr excluding ownership of that object
 *
 *  A view on a cache object stores a reference to an object of type
 *  \ref Expr but does not have ownership of that type. The object is
 *  managed externally. The view just serves as a lughtweight hull.
 */
template <std::size_t Tag, typename Expr>
struct uetliExprView : public uetliExprBase {
private:
  /// @brief Self type
  using self_type = uetliExprView<Tag, Expr>;

  /// @brief Expression type
  using expr_type = typename uetli::remove_all<Expr>::type;

public:
  /// @brief Result type
  using result_type = expr_type;

  /// @brief Tag
  static constexpr std::size_t tag = Tag;

private:
  /// @brief Raw expression data
  const expr_type *expr;

public:
  /// @brief Default constructor
  uetliExprView() = default;

  /// @brief Constructor (constant reference)
  uetliExprView(const Expr &expr) : expr(&expr) {}

  /// @brief Returns constant reference to raw expression data
  const expr_type &get() const { return *expr; }

  /// @brief Returns pointer to raw expression data
  const expr_type *operator->() const { return expr; }

  /// @brief Pretty prints expression
  void print(std::ostream &os) const { os << "view<" << Tag << ">"; }
};

/// @brief Output to string
template <std::size_t Tag, typename Expr>
std::ostream &operator<<(std::ostream &os, uetliExprView<Tag, Expr> expr) {
  expr.print(os);
  return os;
}

/** @brief
 *  Element-wise unitary operators for a cache object
 */
#define UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                \
  template <std::size_t Tag, typename Expr>                                    \
  struct uetliExprUnaryOp_elem_##OPNAME : public uetliExprBase {               \
  private:                                                                     \
    /** @brief Self type */                                                    \
    using self_type = uetliExprUnaryOp_elem_##OPNAME<Tag, Expr>;               \
                                                                               \
    /** @brief Expression type */                                              \
    using expr_type = typename uetli::remove_all<Expr>::type;                  \
                                                                               \
  public:                                                                      \
    /** @brief Result type */                                                  \
    using result_type =                                                        \
        decltype(uetli::elem_##OPNAME(std::declval<expr_type>().get()));       \
                                                                               \
    /** @brief Tag */                                                          \
    static constexpr std::size_t tag = Tag;                                    \
                                                                               \
  private:                                                                     \
    /** @brief Raw expression data */                                          \
    expr_type expr;                                                            \
                                                                               \
    /** @brief Result expression data */                                       \
    result_type result;                                                        \
                                                                               \
  public:                                                                      \
    /** @brief Default constructor */                                          \
    uetliExprUnaryOp_elem_##OPNAME() = default;                                \
                                                                               \
    /** @brief Constructor */                                                  \
    uetliExprUnaryOp_elem_##OPNAME(Expr &&expr)                                \
        : expr(expr), result(uetli::elem_##OPNAME(expr.get())) {}              \
                                                                               \
    /** @brief Returns constant reference to evaluated expression */           \
    const result_type &get() const { return result; }                          \
                                                                               \
    /** @brief Returns pointer to evaluated expression */                      \
    result_type *operator->() { return result; }                               \
                                                                               \
    /** @brief Pretty prints expression */                                     \
    void print(std::ostream &os) const {                                       \
      os << #OPNAME << "(" << expr << ")<" << Tag << ">";                      \
    }                                                                          \
  };                                                                           \
                                                                               \
  template <std::size_t Tag, typename Expr>                                    \
  std::ostream &operator<<(std::ostream &os,                                   \
                           uetliExprUnaryOp_elem_##OPNAME<Tag, Expr> expr) {   \
    expr.print(os);                                                            \
    return os;                                                                 \
  }

UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(abs)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(conj)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(fabs)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(imag)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(real)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc)

#undef UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

/** @brief
 *  Element-wise binary operators between two cache objects
 */
#define UETLI_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(OPNAME)               \
  template <std::size_t Tag, typename Expr1, typename Expr2>                   \
  struct uetliExprBinaryOp_elem_##OPNAME : public uetliExprBase {              \
  private:                                                                     \
    /** @brief Self type */                                                    \
    using self_type = uetliExprBinaryOp_elem_##OPNAME<Tag, Expr1, Expr2>;      \
                                                                               \
    /** @brief First expression type */                                        \
    using expr1_type = typename uetli::remove_all<Expr1>::type;                \
                                                                               \
    /** @brief Second expression type */                                       \
    using expr2_type = typename uetli::remove_all<Expr2>::type;                \
                                                                               \
  public:                                                                      \
    /** @brief Result type */                                                  \
    using result_type = decltype(uetli::elem_##OPNAME(                         \
        std::declval<expr1_type>().get(), std::declval<expr2_type>().get()));  \
                                                                               \
    /** @brief Tag */                                                          \
    static constexpr std::size_t tag = Tag;                                    \
                                                                               \
  private:                                                                     \
    /** @brief First raw expression data */                                    \
    expr1_type expr1;                                                          \
                                                                               \
    /** @brief Second raw expression data */                                   \
    expr2_type expr2;                                                          \
                                                                               \
    /** @brief Result expression data */                                       \
    result_type result;                                                        \
                                                                               \
  public:                                                                      \
    /** @brief Default constructor */                                          \
    uetliExprBinaryOp_elem_##OPNAME() = default;                               \
                                                                               \
    /** @brief Constructor */                                                  \
    uetliExprBinaryOp_elem_##OPNAME(Expr1 &&expr1, Expr2 &&expr2)              \
        : expr1(expr1), expr2(expr2),                                          \
          result(uetli::elem_##OPNAME(expr1.get(), expr2.get())) {}            \
                                                                               \
    /** @brief Returns constant reference to evaluated expression */           \
    const result_type &get() const { return result; }                          \
                                                                               \
    /** @brief Returns pointer to evaluated expression */                      \
    result_type *operator->() { return result; }                               \
                                                                               \
    /** @brief Pretty prints expression */                                     \
    void print(std::ostream &os) const {                                       \
      os << #OPNAME << "(" << expr1 << "," << expr2 << ")<" << Tag << ">";     \
    }                                                                          \
  };                                                                           \
                                                                               \
  template <std::size_t Tag, typename Expr1, typename Expr2>                   \
  std::ostream &operator<<(                                                    \
      std::ostream &os,                                                        \
      uetliExprBinaryOp_elem_##OPNAME<Tag, Expr1, Expr2> expr) {               \
    expr.print(os);                                                            \
    return os;                                                                 \
  }

UETLI_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(div)
UETLI_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(mul)
UETLI_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(pow)

#undef UETLI_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS

/** @brief
 *  Binary operators between two cache objects
 */
#define UETLI_GENERATE_BINARY_OPERATION_OVERLOADS(OPNAME, OP)                  \
  template <std::size_t Tag, typename Expr1, typename Expr2>                   \
  struct uetliExprBinaryOp_##OPNAME : public uetliExprBase {                   \
  private:                                                                     \
    /** @brief Self type */                                                    \
    using self_type = uetliExprBinaryOp_##OPNAME<Tag, Expr1, Expr2>;           \
                                                                               \
    /** @brief First expression type */                                        \
    using expr1_type = typename uetli::remove_all<Expr1>::type;                \
                                                                               \
    /** @brief Second expression type */                                       \
    using expr2_type = typename uetli::remove_all<Expr2>::type;                \
                                                                               \
  public:                                                                      \
    /** @brief Result type */                                                  \
    using result_type = decltype(                                              \
        std::declval<expr1_type>().get() OP std::declval<expr2_type>().get()); \
                                                                               \
    /** @brief Tag */                                                          \
    static constexpr std::size_t tag = Tag;                                    \
                                                                               \
  private:                                                                     \
    /** @brief First raw expression data */                                    \
    expr1_type expr1;                                                          \
                                                                               \
    /** @brief Second raw expression data */                                   \
    expr2_type expr2;                                                          \
                                                                               \
    /** @brief Result expression data */                                       \
    result_type result;                                                        \
                                                                               \
  public:                                                                      \
    /** @brief Default constructor */                                          \
    uetliExprBinaryOp_##OPNAME() = default;                                    \
                                                                               \
    /** @brief Constructor */                                                  \
    uetliExprBinaryOp_##OPNAME(Expr1 &&expr1, Expr2 &&expr2)                   \
        : expr1(expr1), expr2(expr2), result(expr1.get() OP expr2.get()) {}    \
                                                                               \
    /** @brief Pretty prints expression */                                     \
    void print(std::ostream &os) const {                                       \
      os << "(" << expr1 << #OP << expr2 << ")<" << Tag << ">";                \
    }                                                                          \
                                                                               \
    /** @brief Returns evaluated expression */                                 \
    const result_type &get() const { return result; }                          \
  };                                                                           \
                                                                               \
  template <std::size_t Tag, typename Expr1, typename Expr2>                   \
  std::ostream &operator<<(                                                    \
      std::ostream &os, uetliExprBinaryOp_##OPNAME<Tag, Expr1, Expr2> expr) {  \
    expr.print(os);                                                            \
    return os;                                                                 \
  }

UETLI_GENERATE_BINARY_OPERATION_OVERLOADS(add, +)
UETLI_GENERATE_BINARY_OPERATION_OVERLOADS(sub, -)
UETLI_GENERATE_BINARY_OPERATION_OVERLOADS(mul, *)

#undef UETLI_GENERATE_BINARY_OPERATION_OVERLOADS

/** @brief
 *  Binary operator/ for cache object
 */
template <std::size_t Tag, typename Expr1, typename Expr2>
struct uetliExprBinaryOp_div : public uetliExprBase {
private:
  /// @brief Self type
  using self_type = uetliExprBinaryOp_div<Tag, Expr1, Expr2>;

  /// @brief First expression type
  using expr1_type = typename uetli::remove_all<Expr1>::type;

  /// @brief Second expression type
  using expr2_type = typename uetli::remove_all<Expr2>::type;

public:
  /// @brief Result type
  using result_type = decltype(std::declval<expr1_type>().get() /
                               std::declval<expr2_type>().get());

  /// @brief Tag
  static constexpr std::size_t tag = Tag;

private:
  /// @brief First raw expression data
  expr1_type expr1;

  /// @brief Second raw expression data
  expr2_type expr2;

  /// @brief Result expression data
  result_type result;

public:
  /// @brief Default constructor
  uetliExprBinaryOp_div() = default;

  /// @brief Constructor
  uetliExprBinaryOp_div(Expr1 &&expr1, Expr2 &&expr2)
      : expr1(expr1), expr2(expr2), result(expr1.get() / expr2.get()) {}

  /// @brief Pretty prints expression
  void print(std::ostream &os) const {
    os << "(" << expr1 << "/" << expr2 << ")<" << Tag << ">";
  }

  /// @brief Returns evaluated expression
  const result_type &get() const { return result; }
};

template <std::size_t Tag, typename Expr1, typename Expr2>
std::ostream &operator<<(std::ostream &os,
                         uetliExprBinaryOp_div<Tag, Expr1, Expr2> expr) {
  expr.print(os);
  return os;
}

/** @brief
 *  Binary operator+ between two cache objects
 */
template <typename Expr1, typename Expr2,
          typename = typename uetli::enable_if_all_type_of<
              Expr1, Expr2, EnumETL::CACHE>::type>
auto operator+(Expr1 &&expr1, Expr2 &&expr2) -> uetliExprBinaryOp_add<
    utils::hash<std::size_t>(uetli::remove_all<Expr1>::type::tag, '+',
                             uetli::remove_all<Expr2>::type::tag),
    Expr1, Expr2> {
  return uetliExprBinaryOp_add<utils::hash<std::size_t>(
                                   uetli::remove_all<Expr1>::type::tag, '+',
                                   uetli::remove_all<Expr2>::type::tag),
                               Expr1, Expr2>(std::forward<Expr1>(expr1),
                                             std::forward<Expr2>(expr2));
}

/** @brief
 *  Binary operator+ between a an object of arithmetic type and a cache object
 */
template <typename Expr1, typename Expr2,
          typename = typename uetli::enable_if_type_of_and_cond<
              Expr2, EnumETL::CACHE, std::is_arithmetic<Expr1>::value>::type>
auto operator+(Expr1 &&expr1, Expr2 &&expr2) -> uetliExprBinaryOp_add<
    utils::hash<std::size_t>(725795011, '+',
                             uetli::remove_all<Expr2>::type::tag),
    uetliExpr<725795011, Expr1>, Expr2> {
  return uetliExprBinaryOp_add<utils::hash<std::size_t>(
                                   725795011, '+',
                                   uetli::remove_all<Expr2>::type::tag),
                               uetliExpr<725795011, Expr1>, Expr2>(
      uetliExpr<725795011, Expr1>(Expr1(expr1)), std::forward<Expr2>(expr2));
}

/** @brief
 *  Binary operator+ between a cache object and an object of arithmetic type
 */
template <typename Expr1, typename Expr2,
          typename = typename uetli::enable_if_type_of_and_cond<
              Expr1, EnumETL::CACHE, std::is_arithmetic<Expr2>::value>::type>
auto operator+(Expr1 &&expr1, Expr2 &&expr2) -> uetliExprBinaryOp_add<
    utils::hash<std::size_t>(uetli::remove_all<Expr1>::type::tag, '+',
                             169422329),
    Expr1, uetliExpr<169422329, Expr2>> {
  return uetliExprBinaryOp_add<utils::hash<std::size_t>(
                                   uetli::remove_all<Expr1>::type::tag, '+',
                                   169422329),
                               Expr1, uetliExpr<169422329, Expr2>>(
      std::forward<Expr1>(expr1), uetliExpr<169422329, Expr2>(Expr2(expr2)));
}

/** @brief
 *  Binary operator- between two cache objects
 */
template <typename Expr1, typename Expr2,
          typename = typename uetli::enable_if_all_type_of<
              Expr1, Expr2, EnumETL::CACHE>::type>
auto operator-(Expr1 &&expr1, Expr2 &&expr2) -> uetliExprBinaryOp_sub<
    utils::hash<std::size_t>(uetli::remove_all<Expr1>::type::tag, '-',
                             uetli::remove_all<Expr2>::type::tag),
    Expr1, Expr2> {
  return uetliExprBinaryOp_sub<utils::hash<std::size_t>(
                                   uetli::remove_all<Expr1>::type::tag, '-',
                                   uetli::remove_all<Expr2>::type::tag),
                               Expr1, Expr2>(std::forward<Expr1>(expr1),
                                             std::forward<Expr2>(expr2));
}

/** @brief
 *  Binary operator- between a an object of arithmetic type and a cache object
 */
template <typename Expr1, typename Expr2,
          typename = typename uetli::enable_if_type_of_and_cond<
              Expr2, EnumETL::CACHE, std::is_arithmetic<Expr1>::value>::type>
auto operator-(Expr1 &&expr1, Expr2 &&expr2) -> uetliExprBinaryOp_sub<
    utils::hash<std::size_t>(574849605, '-',
                             uetli::remove_all<Expr2>::type::tag),
    uetliExpr<574849605, Expr1>, Expr2> {
  return uetliExprBinaryOp_sub<utils::hash<std::size_t>(
                                   574849605, '-',
                                   uetli::remove_all<Expr2>::type::tag),
                               uetliExpr<574849605, Expr1>, Expr2>(
      uetliExpr<574849605, Expr1>(Expr1(expr1)), std::forward<Expr2>(expr2));
}

/** @brief
 *  Binary operator- between a cache object and an object of arithmetic type
 */
template <typename Expr1, typename Expr2,
          typename = typename uetli::enable_if_type_of_and_cond<
              Expr1, EnumETL::CACHE, std::is_arithmetic<Expr2>::value>::type>
auto operator-(Expr1 &&expr1, Expr2 &&expr2) -> uetliExprBinaryOp_sub<
    utils::hash<std::size_t>(uetli::remove_all<Expr1>::type::tag, '-',
                             681648267),
    Expr1, uetliExpr<681648267, Expr2>> {
  return uetliExprBinaryOp_sub<utils::hash<std::size_t>(
                                   uetli::remove_all<Expr1>::type::tag, '-',
                                   681648267),
                               Expr1, uetliExpr<681648267, Expr2>>(
      std::forward<Expr1>(expr1), uetliExpr<681648267, Expr2>(Expr2(expr2)));
}

/** @brief
 *  Binary operator* between two cache objects
 */
template <typename Expr1, typename Expr2,
          typename = typename uetli::enable_if_all_type_of<
              Expr1, Expr2, EnumETL::CACHE>::type>
auto operator*(Expr1 &&expr1, Expr2 &&expr2) -> uetliExprBinaryOp_mul<
    utils::hash<std::size_t>(uetli::remove_all<Expr1>::type::tag, '*',
                             uetli::remove_all<Expr2>::type::tag),
    Expr1, Expr2> {
  return uetliExprBinaryOp_mul<utils::hash<std::size_t>(
                                   uetli::remove_all<Expr1>::type::tag, '*',
                                   uetli::remove_all<Expr2>::type::tag),
                               Expr1, Expr2>(std::forward<Expr1>(expr1),
                                             std::forward<Expr2>(expr2));
}

/** @brief
 *  Binary operator* between a an object of arithmetic type and a cache object
 */
template <typename Expr1, typename Expr2,
          typename = typename uetli::enable_if_type_of_and_cond<
              Expr2, EnumETL::CACHE, std::is_arithmetic<Expr1>::value>::type>
auto operator*(Expr1 &&expr1, Expr2 &&expr2) -> uetliExprBinaryOp_mul<
    utils::hash<std::size_t>(839176472, '*',
                             uetli::remove_all<Expr2>::type::tag),
    uetliExpr<839176472, Expr1>, Expr2> {
  return uetliExprBinaryOp_mul<utils::hash<std::size_t>(
                                   839176472, '*',
                                   uetli::remove_all<Expr2>::type::tag),
                               uetliExpr<839176472, Expr1>, Expr2>(
      uetliExpr<839176472, Expr1>(Expr1(expr1)), std::forward<Expr2>(expr2));
}

/** @brief
 *  Binary operator* between a cache object and an object of arithmetic type
 */
template <typename Expr1, typename Expr2,
          typename = typename uetli::enable_if_type_of_and_cond<
              Expr1, EnumETL::CACHE, std::is_arithmetic<Expr2>::value>::type>
auto operator*(Expr1 &&expr1, Expr2 &&expr2) -> uetliExprBinaryOp_mul<
    utils::hash<std::size_t>(uetli::remove_all<Expr1>::type::tag, '*',
                             689934539),
    Expr1, uetliExpr<689934539, Expr2>> {
  return uetliExprBinaryOp_mul<utils::hash<std::size_t>(
                                   uetli::remove_all<Expr1>::type::tag, '*',
                                   689934539),
                               Expr1, uetliExpr<689934539, Expr2>>(
      std::forward<Expr1>(expr1), uetliExpr<689934539, Expr2>(Expr2(expr2)));
}

/** @brief
 *  Binary operator/ between two cache objects
 */
template <typename Expr1, typename Expr2,
          typename = typename uetli::enable_if_all_type_of<
              Expr1, Expr2, EnumETL::CACHE>::type>
auto operator/(Expr1 &&expr1, Expr2 &&expr2) -> uetliExprBinaryOp_mul<
    utils::hash<std::size_t>(uetli::remove_all<Expr1>::type::tag, '/',
                             uetli::remove_all<Expr2>::type::tag),
    Expr1, Expr2> {
  return uetliExprBinaryOp_mul<utils::hash<std::size_t>(
                                   uetli::remove_all<Expr1>::type::tag, '/',
                                   uetli::remove_all<Expr2>::type::tag),
                               Expr1, Expr2>(std::forward<Expr1>(expr1),
                                             std::forward<Expr2>(expr2));
}

/** @brief
 *  Binary operator/ between a an object of arithmetic type and a cache object
 */
template <typename Expr1, typename Expr2,
          typename = typename uetli::enable_if_type_of_and_cond<
              Expr2, EnumETL::CACHE, std::is_arithmetic<Expr1>::value>::type>
auto operator/(Expr1 &&expr1, Expr2 &&expr2) -> uetliExprBinaryOp_mul<
    utils::hash<std::size_t>(351934064, '/',
                             uetli::remove_all<Expr2>::type::tag),
    uetliExpr<351934064, Expr1>, Expr2> {
  return uetliExprBinaryOp_mul<utils::hash<std::size_t>(
                                   351934064, '/',
                                   uetli::remove_all<Expr2>::type::tag),
                               uetliExpr<351934064, Expr1>, Expr2>(
      uetliExpr<351934064, Expr1>(Expr1(expr1)), std::forward<Expr2>(expr2));
}

/** @brief
 *  Binary operator/ between a cache object and an object of arithmetic type
 */
template <typename Expr1, typename Expr2,
          typename = typename uetli::enable_if_type_of_and_cond<
              Expr1, EnumETL::CACHE, std::is_arithmetic<Expr2>::value>::type>
auto operator/(Expr1 &&expr1, Expr2 &&expr2) -> uetliExprBinaryOp_mul<
    utils::hash<std::size_t>(uetli::remove_all<Expr1>::type::tag, '/',
                             253548959),
    Expr1, uetliExpr<253548959, Expr2>> {
  return uetliExprBinaryOp_mul<utils::hash<std::size_t>(
                                   uetli::remove_all<Expr1>::type::tag, '/',
                                   253548959),
                               Expr1, uetliExpr<253548959, Expr2>>(
      std::forward<Expr1>(expr1), uetliExpr<253548959, Expr2>(Expr2(expr2)));
}

} // namespace cache

} // namespace uetli

#endif // UETLI_CACHE_HPP
