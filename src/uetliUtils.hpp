/** @file uetliUtils.hpp
 *
 *  @brief Utility class of the Fluid Dynamics Building Blocks
 *
 *  @copyright This file is part of the UETLI library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef UETLI_UTILS_HPP
#define UETLI_UTILS_HPP

namespace uetli {

/** @namespace uetli::utils
 *
 *  @brief
 *  The \ref uetli::utils namespace, containing extra utilities of the
 *  UETLI library
 *
 *  The \ref uetli::utils namespace contains some extra utilities that
 *  are used by the UETLI library. Like the functionality in the \ref
 *  uetli namespace, the extra utilities have a stable API that does
 *  not change over future releases of the UETLI library.
 *
 *  @note This Source Code Form is subject to the terms of the Mozilla
 *  Public License, v. 2.0. If a copy of the MPL was not distributed
 *  with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace utils {

/** @brief
 *  Compile-time djb2 hashing by Dan Bernstein
 *
 * @{
 */
template <typename hash_t, typename T> hash_t constexpr djb2(hash_t hash, T t) {
  return static_cast<hash_t>(((hash << 5) + hash) + static_cast<hash_t>(t));
}

template <typename hash_t, typename T, typename... Ts>
hash_t constexpr djb2(hash_t hash, T t, Ts... ts) {
  return djb2(djb2(hash, t), ts...);
}

template <typename hash_t, typename... Ts> hash_t constexpr hash(Ts... ts) {
  return djb2<hash_t>(5381, ts...);
}
/** @} */

} // namespace utils

} // namespace uetli

#endif // UETLI_UTILS_HPP
