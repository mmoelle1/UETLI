/** @file uetliCompatibility.hpp
 *
 *  @brief Compatibility layer for Fluid Dynamics Building Blocks
 *
 *  This file implements a compatibility layer between the
 *  non-standardized expression template libraries and the naming
 *  convention using in UETLI. It also implements extra functionality
 *  not available in (some of) the expression template libraries.
 *
 *  @copyright This file is part of the UETLI library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef UETLI_COMPATIBILITY_HPP
#define UETLI_COMPATIBILITY_HPP

#include <cmath>
#include <complex>
#include <iomanip>
#include <type_traits>
#include <utility>

#include "uetliCacheForward.hpp"
#include "uetliConfig.hpp"
#include "uetliUtils.hpp"

namespace std {
/**
 * @brief
 * Checks if a type is a complex type
 *
 * @tparam T The type that is checked for being of complex type
 */
template <typename T> struct is_complex : std::false_type {};
template <typename T> struct is_complex<std::complex<T>> : std::true_type {};

/**
 * @brief
 * Converts argument to given type
 *
 * @tparam D The destination type
 * @tparam S The source type
 */
template <typename D, typename S> D to_type(S s) { return std::forward<D>(s); }
template <typename D, typename S> D to_type(std::complex<S> s) {
  return (D)s.real();
}
} // namespace std

namespace uetli {

/** @enum EnumETL
 *
 *  @brief
 *  The EnumETL enumeration type, listing all supported vector
 *  expression template libraries explicitly supported by the library.
 */
enum class EnumETL {
  /// @brief Generic implementation
  GENERIC,
  CACHE,

#ifdef SUPPORT_ARMADILLO
  /// @brief Armadillo library
  /// http://arma.sourceforge.net
  ARMADILLO,
#endif

#ifdef SUPPORT_ARRAYFIRE
  /// @brief ArrayFire library
  /// http://arrayfire.com
  ARRAYFIRE,
#endif

#ifdef SUPPORT_BLAZE
  /// @brief Blaze library
  /// https://bitbucket.org/blaze-lib/blaze
  BLAZE,
#endif

#ifdef SUPPORT_EIGEN
  /// @{
  /// @brief Eigen library
  /// http://eigen.tuxfamily.org
  EIGEN,
  EIGEN_ARRAY,
  EIGEN_MATRIX,
/// @}
#endif

#ifdef SUPPORT_ITPP
  /// @{
  /// @brief IT++ library
  /// http://itpp.sourceforge.net
  ITPP,
  ITPP_VECTOR,
  ITPP_MATRIX,
/// @}
#endif

#if defined(SUPPORT_MTL4) || defined(SUPPORT_CMTL4)
  /// @{
  /// @brief MTL4 library
  /// http://www.simunova.com/docs/mtl4/html/index.html
  MTL4,
  MTL4_VECTOR,
  MTL4_MATRIX,
/// @}
#endif

#ifdef SUPPORT_UBLAS
  /// @brief uBLAS library
  /// http://www.boost.org/doc/libs/1_60_0/libs/numeric/ublas/doc/
  UBLAS,
#endif

#ifdef SUPPORT_VEXCL
  /// @brief VexCL library
  /// https://github.com/ddemidov/vexcl
  VEXCL,
#endif

#ifdef SUPPORT_VIENNACL
  /// @brief ViennaCL library
  /// http://viennacl.sourceforge.net
  VIENNACL
#endif
};

/** @brief
 * Print EnumETL to string
 */
std::ostream &operator<<(std::ostream &os, EnumETL etl) {
  switch (etl) {
  case EnumETL::GENERIC:
    return os << "GENERIC";

  case EnumETL::CACHE:
    return os << "CACHE";

#ifdef SUPPORT_ARMADILLO
  case EnumETL::ARMADILLO:
    return os << "ARMADILLO";
#endif

#ifdef SUPPORT_ARRAYFIRE
  case EnumETL::ARRAYFIRE:
    return os << "ARRAYFIRE";
#endif

#ifdef SUPPORT_BLAZE
  case EnumETL::BLAZE:
    return os << "BLAZE";
#endif

#ifdef SUPPORT_EIGEN
  case EnumETL::EIGEN:
    return os << "EIGEN";
  case EnumETL::EIGEN_ARRAY:
    return os << "EIGEN_ARRAY";
  case EnumETL::EIGEN_MATRIX:
    return os << "EIGEN_MATRIX";
#endif

#ifdef SUPPORT_ITPP
  case EnumETL::ITPP:
    return os << "ITPP";
  case EnumETL::ITPP_VECTOR:
    return os << "ITPP_VECTOR";
  case EnumETL::ITPP_MATRIX:
    return os << "ITPP_MATRIX";
#endif

#if defined(SUPPORT_MTL4) || defined(SUPPORT_CMTL4)
  case EnumETL::MTL4:
    return os << "MTL4";
  case EnumETL::MTL4_VECTOR:
    return os << "MTL4_VECTOR";
  case EnumETL::MTL4_MATRIX:
    return os << "MTL4_MATRIX";
#endif

#ifdef SUPPORT_UBLAS
  case EnumETL::UBLAS:
    return os << "UBLAS";
#endif

#ifdef SUPPORT_VEXCL
  case EnumETL::VEXCL:
    return os << "VEXCL";
#endif

#ifdef SUPPORT_VIENNACL
  case EnumETL::VIENNACL:
    return os << "VIENNACL";
#endif
  }
  return os << static_cast<std::uint16_t>(etl);
}

/** @brief
 *  Extracts the unqualified base type by removing any const, volatile and
 *  reference
 *
 *  @tparam The type from which to extract the unqualified base type
 */
template <typename T>
using remove_all =
    std::remove_cv<typename std::remove_pointer<typename std::remove_reference<
        typename std::remove_all_extents<T>::type>::type>::type>;

/** @brief
 *  If T is type of the expression template library Expr, provides the
 *  member constant value equal to true. Otherwise value is false.
 *
 *  @tparam T    The type to check against the given expression type
 *  @tparam Expr The expression type
 */
template <typename T, EnumETL Expr, typename = void>
struct is_type_of : std::false_type {};

/** @brief
 *  If T is type of the expression template library Expr, then
 *  uetli::enable_if_type_of has a public member typedef type, equal to
 *  T; otherwise is no member typedef.
 *
 *  @tparam T    The type to check against the given expression type
 *  @tparam Expr The expression type
 */
template <typename T, EnumETL Expr, class C = void>
using enable_if_type_of = std::enable_if<is_type_of<T, Expr>::value, C>;

/** @brief
 *  If T is type of the expression template library Expr and condition
 *  cond is true, then uetli::enable_if_type_of has a public member
 *  typedef type, equal to T; otherwise is no member typedef.
 *
 *  @tparam T    The type to check against the given expression type
 *  @tparam Expr The expression type
 *  @tparam Cond The condition consider additionally
 */
template <typename T, EnumETL Expr, bool Cond, class C = void>
using enable_if_type_of_and_cond =
    std::enable_if<is_type_of<T, Expr>::value && Cond, C>;

/** @brief
 *  If T is type of the expression template library Expr or condition
 *  cond is true, then uetli::enable_if_type_of has a public member
 *  typedef type, equal to T; otherwise is no member typedef.
 *
 *  @tparam T    The type to check against the given expression type
 *  @tparam Expr The expression type
 *  @tparam Cond The condition consider additionally
 */
template <typename T, EnumETL Expr, bool Cond, class C = void>
using enable_if_type_of_or_cond =
    std::enable_if<is_type_of<T, Expr>::value || Cond, C>;

/** @brief
 *  If T1 and T2 are the same types of the expression template library
 *  Expr, then uetli::enable_if_type_of has a public member typedef type,
 *  equal to T1; otherwise is no member typedef.
 *
 *  @tparam T1 The first type to check against the given expression type
 *  @tparam T2 The second type to check against the given expression type
 *  @tparam Expr The expression type
 */
template <typename T1, typename T2, EnumETL Expr, class C = void>
using enable_if_all_type_of =
    std::enable_if<is_type_of<T1, Expr>::value && is_type_of<T2, Expr>::value,
                   C>;

/** @brief
 *  If T1 and T2 are the same types of the expression template library
 *  Expr and condition cond is true, then uetli::enable_if_type_of has a
 *  public member typedef type, equal to T1; otherwise is no member
 *  typedef.
 *
 *  @tparam T1 The first type to check against the given expression type
 *  @tparam T2 The second type to check against the given expression type
 *  @tparam Expr The expression type
 *  @tparam Cond The condition consider additionally
 */
template <typename T1, typename T2, EnumETL Expr, bool Cond, class C = void>
using enable_if_all_type_of_and_cond = std::enable_if<
    (is_type_of<T1, Expr>::value && is_type_of<T2, Expr>::value) && Cond, C>;

/** @brief
 *  If T1 and T2 are the same types of the expression template library
 *  Expr or condition cond is true, then uetli::enable_if_type_of has a
 *  public member typedef type, equal to T1; otherwise is no member
 *  typedef.
 *
 *  @tparam T1 The first type to check against the given expression type
 *  @tparam T2 The second type to check against the given expression type
 *  @tparam Expr The expression type
 *  @tparam Cond The condition consider additionally
 */
template <typename T1, typename T2, EnumETL Expr, bool Cond, class C = void>
using enable_if_all_type_of_or_cond = std::enable_if<
    (is_type_of<T1, Expr>::value && is_type_of<T2, Expr>::value) || Cond, C>;

/** @brief
 *  If T1 or T2 are the same types of the expression template library
 *  Expr, then uetli::enable_if_type_of has a public member typedef type,
 *  equal to T1 or T2; otherwise is no member typedef.
 *
 *  @tparam T1 The first type to check against the given expression type
 *  @tparam T2 The second type to check against the given expression type
 *  @tparam Expr The expression type
 */
template <typename T1, typename T2, EnumETL Expr, class C = void>
using enable_if_any_type_of =
    std::enable_if<(is_type_of<T1, Expr>::value || is_type_of<T2, Expr>::value),
                   C>;

/** @brief
 *  If T1 or T2 are the same types of the expression template library
 *  Expr and condition cond is true, then uetli::enable_if_type_of has a
 *  public member typedef type, equal to T1 or T2; otherwise is no
 *  member typedef.
 *
 *  @tparam T1 The first type to check against the given expression type
 *  @tparam T2 The second type to check against the given expression type
 *  @tparam Expr The expression type
 *  @tparam Cond The condition consider additionally
 */
template <typename T1, typename T2, EnumETL Expr, bool Cond, class C = void>
using enable_if_any_type_of_and_cond = std::enable_if<
    (is_type_of<T1, Expr>::value || is_type_of<T2, Expr>::value) && Cond, C>;

/** @brief
 *  If T1 or T2 are the same types of the expression template library
 *  Expr or condition cond is true, then uetli::enable_if_type_of has a
 *  public member typedef type, equal to T1 or T2; otherwise is no
 *  member typedef.
 *
 *  @tparam T1 The first type to check against the given expression type
 *  @tparam T2 The second type to check against the given expression type
 *  @tparam Expr The expression type
 *  @tparam Cond The condition consider additionally
 */
template <typename T1, typename T2, EnumETL Expr, bool Cond, class C = void>
using enable_if_any_type_of_or_cond = std::enable_if<
    (is_type_of<T1, Expr>::value || is_type_of<T2, Expr>::value) || Cond, C>;

/** @brief
 *  Result type of the expression Expr
 *
 *  @tparam Expr The expression type
 */
template <typename Expr, typename = void> struct result_type {
  using type = void;
};

/** @brief
 *  Result type of the expression Expr (arithmetic type)
 *
 *  @tparam Expr The expression type
 */
template <typename Expr>
struct result_type<
    Expr, typename std::enable_if<std::is_arithmetic<Expr>::value>::type> {
  using type = Expr;
};

/** @brief
 *  Scalar value type of the expression Expr
 *
 *  @tparam Expr The expression type
 */
template <typename Expr, typename = void> struct value_type {
  using type = void;
};

/** @brief
 *  Scalar value type of the expression Expr (arithmetic type)
 *
 *  @tparam Expr The expression type
 */
template <typename Expr>
struct value_type<
    Expr, typename std::enable_if<std::is_arithmetic<Expr>::value>::type> {
  using type = Expr;
};

/** @namespace uetli::internal
 *
 *  @brief
 *  The uetli::internal namespace, containing all internal details of
 *  functions of the UETLI library
 */
namespace internal {

/** @brief
 *  Indicator for (specialized) implementation of
 *  uetli::internal::make_constant<T,Expr>(const T c, Expr&& expr) function
 *
 *  @tparam T    The constant type
 *  @tparam Expr The expression type
 */
template <typename Expr, typename = void>
struct has_make_constant_impl : public std::false_type {};

/** @brief
 *  Indicator for (specialized) implementation of
 *  uetli::internal::make_temp_impl<Tag,Expr>(Expr&& expr) function
 *
 *  @tparam Expr The expression type
 */
template <std::size_t Tag, typename Expr, typename = void>
struct has_make_temp_impl : public std::false_type {};

/** @brief
 *  Indicator for (specialized) implementation of
 *  uetli::internal::tag_impl<Tag,Expr>(Expr&& expr) function
 *
 *  @tparam Tag  The tag
 *  @tparam Expr The expression type
 */
template <std::size_t Tag, typename Expr, typename = void>
struct has_tag_impl : public std::false_type {};

/// Helper macro for generating selectors for specialialized
/// implementations of element-wise binary operations; generic
/// implementation follows below
#define UETLI_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(OPNAME)               \
  /** @brief                                                                   \
   *  Selector for (specialized) implementation of                             \
   *  uetli::internal::elem_##OPNAME##<A,B>(A&& a, B&& b) function             \
   *                                                                           \
   *  @tparam A The first expression type                                      \
   *  @tparam B The second expression type                                     \
   */                                                                          \
  template <typename A, typename B, typename = void>                           \
  struct get_elem_##OPNAME##_impl                                              \
      : public std::integral_constant<EnumETL, EnumETL::GENERIC> {};

UETLI_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(mul)
UETLI_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(div)
UETLI_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(pow)

#undef UETLI_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS

/** @brief
 *  Element-wise multiplication (perfect forwarding to *-operator)
 *
 *  @tparam A       The first expression type
 *  @tparam B       The second expression type
 *  @tparam EnumETL The expression type enumerator
 */
template <typename A, typename B, EnumETL = EnumETL::GENERIC>
struct elem_mul_impl {
  static UETLI_INLINE auto constexpr eval(A &&a, B &&b)
#ifdef SUPPORT_CXX11
      -> decltype(std::forward<A>(a) * std::forward<B>(b))
#endif
  {
    return std::forward<A>(a) * std::forward<B>(b);
  }
};

/** @brief
 *  Element-wise division (perfect forwarding to /-operator)
 *
 *  @tparam A       The first expression type
 *  @tparam B       The second expression type
 *  @tparam EnumETL The expression type enumerator
 */
template <typename A, typename B, EnumETL = EnumETL::GENERIC>
struct elem_div_impl {
  static UETLI_INLINE auto constexpr eval(A &&a, B &&b)
#ifdef SUPPORT_CXX11
      -> decltype(std::forward<A>(a) / std::forward<B>(b))
#endif
  {
    return std::forward<A>(a) / std::forward<B>(b);
  }
};

/** @brief
 *  Element-wise raise to power (perfect forwarding to pow-function)
 *
 *  @tparam A       The first expression type
 *  @tparam B       The second expression type
 *  @tparam EnumETL The expression type enumerator
 */
template <typename A, typename B, EnumETL = EnumETL::GENERIC>
struct elem_pow_impl {
  static UETLI_INLINE auto constexpr eval(A &&a, B &&b)
#ifdef SUPPORT_CXX11
      -> decltype(pow(std::forward<A>(a), std::forward<B>(b)))
#endif
  {
    return pow(std::forward<A>(a), std::forward<B>(b));
  }
};

/// Helper macro for generating selectors for specialized
/// implementations of element-wise unary operations and generic implementation
#ifdef SUPPORT_CXX11
#define UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                \
  /** @brief                                                                   \
   *  Selector for (specialized) implementation of                             \
   *  internal::elem_##OPNAME##<A>(A&& a) function                             \
   *                                                                           \
   *  @tparam A       The expression type                                      \
   *  @tparam EnumETL The expression type enumerator                           \
   */                                                                          \
  template <typename A, typename = void>                                       \
  struct get_elem_##OPNAME##_impl                                              \
      : public std::integral_constant<EnumETL, EnumETL::GENERIC> {};           \
                                                                               \
  /** @brief                                                                   \
   *  Element-wise OPNAME function (perfect forwarding to OPNAME)              \
   *                                                                           \
   *  @tparam A       The expression type                                      \
   *  @tparam EnumETL The expression type enumerator                           \
   */                                                                          \
  template <typename A, EnumETL = EnumETL::GENERIC>                            \
  struct elem_##OPNAME##_impl {                                                \
    static UETLI_INLINE auto constexpr eval(A &&a)                             \
        -> decltype(OPNAME(std::forward<A>(a))) {                              \
      return OPNAME(std::forward<A>(a));                                       \
    }                                                                          \
  };
#else
#define UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                \
  /** @brief                                                                   \
   *  Selector for (specialized) implementation of                             \
   *  internal::elem_##OPNAME##<A>(A&& a) function                             \
   *                                                                           \
   *  @tparam A       The expression type                                      \
   *  @tparam EnumETL The expression type enumerator                           \
   */                                                                          \
  template <typename A, typename = void>                                       \
  struct get_elem_##OPNAME##_impl                                              \
      : public std::integral_constant<EnumETL, EnumETL::GENERIC> {};           \
                                                                               \
  /** @brief                                                                   \
   *  Element-wise OPNAME function (perfect forwarding to OPNAME)              \
   *                                                                           \
   *  @tparam A       The expression type                                      \
   *  @tparam EnumETL The expression type enumerator                           \
   */                                                                          \
  template <typename A, EnumETL = EnumETL::GENERIC>                            \
  struct elem_##OPNAME##_impl {                                                \
    static UETLI_INLINE auto constexpr eval(A &&a) {                           \
      return OPNAME(std::forward<A>(a));                                       \
    }                                                                          \
  };
#endif

UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(abs)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(conj)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(fabs)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(imag)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(real)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc)

#undef UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

} // namespace internal

// Include backend implementations
#include "backend/armadillo.hpp"
#include "backend/arrayfire.hpp"
#include "backend/blaze.hpp"
#include "backend/cache.hpp"
#include "backend/eigen.hpp"
#include "backend/itpp.hpp"
#include "backend/mtl4.hpp"
#include "backend/ublas.hpp"
#include "backend/vexcl.hpp"
#include "backend/viennacl.hpp"

namespace internal {

/** @brief
 *  Forward declaration of generic implementation of
 *  uetli::internal::make_constant_impl<T,Expr>(const T c, Expr&& expr) function
 *
 *  @param  TC   The typeconstant (stores constant encoded in template
 *               parameter)
 *  @tparam T    The constant type
 *  @tparam Expr The expression type
 */
template <typename TC, typename T, typename Expr>
static UETLI_INLINE auto constexpr make_constant_impl(const T value,
                                                      Expr &&expr) ->
    typename std::enable_if<!has_make_constant_impl<Expr>::value,
                            typename uetli::value_type<Expr>::type>::type;

/** @brief
 *  Forward declaration of generic implementation of
 *  uetli::internal::make_temp_impl<Tag,Expr>(Expr&& expr) function
 *
 *  @tparam Tag  The tag
 *  @tparam Expr The expression type
 */
template <std::size_t Tag, typename Expr>
static UETLI_INLINE auto constexpr make_temp_impl(Expr &&expr) ->
    typename std::enable_if<!has_make_temp_impl<Tag, Expr>::value,
                            decltype(std::forward<Expr>(expr))>::type;

/** @brief
 *  Forward declaration of generic implementation of
 *  uetli::internal::tag_impl<Tag,Expr>(Expr&& expr) function
 *
 *  @tparam Tag  The tag
 *  @tparam Expr The expression type
 */
template <std::size_t Tag, typename Expr>
static UETLI_INLINE auto constexpr tag_impl(Expr &&expr) ->
    typename std::enable_if<!has_tag_impl<Tag, Expr>::value,
                            decltype(std::forward<Expr>(expr))>::type;

} // namespace internal

/** @brief
 *  Creates constant that complies with the value-type of the expression.
 *
 *  @note
 *  If no specialized implementation is available then this
 *  implementation casts the given value to the scalar value-type of
 *  the expression and returns it as const.
 *
 *  @param  TC   The typeconstant (stores constant encoded in template
 parameter)
 *
 *  @tparam T    The constant type
 *  @tparam Expr The expression type
 *  @{
 */
template <typename TC, typename T, typename Expr>
static UETLI_INLINE auto constexpr make_constant(const T value, Expr &&expr) ->
    typename std::enable_if<
        internal::has_make_constant_impl<Expr>::value,
        decltype(internal::make_constant_impl<TC>(value, expr))>::type {
  return internal::make_constant_impl<TC>(value, expr);
}

template <typename TC, typename T, typename Expr>
static UETLI_INLINE auto constexpr make_constant(const T value, Expr &&expr) ->
    typename std::enable_if<!internal::has_make_constant_impl<Expr>::value,
                            typename uetli::value_type<Expr>::type>::type {
  return static_cast<typename uetli::value_type<Expr>::type>(value);
}
/** @} */

/** @brief
 *  Creates temporary expression that may be reused in a vector
 *  expression. The type of the resulting temporary variable must
 *  be explicitly specified as a template parameter.
 *
 *  @note
 *  This functions requires T!=Expr. If T==Expr the
 *  make_temp<Tag,Expr>(Expr&& expr) function gets activated
 *
 *  @tparam Tag  The tag
 *  @tparam T    The temporal type
 *  @tparam Expr The expression type
 */
template <std::size_t Tag, typename Temp, typename Expr>
static UETLI_INLINE auto constexpr make_temp(Expr &&expr) ->
    typename std::enable_if<!std::is_same<Temp, Expr>::value,
                            decltype(Temp(std::forward<Expr>(expr)))>::type {
  return Temp(std::forward<Expr>(expr));
}

/** @brief
 *  Creates temporary expression that may be reused in a vector
 *  expression. The type of the resulting temporary variable is
 *  automatically deduced from the expression.
 *
 *  @note
 *  If no specialized implementation is available then this
 *  implementation performs perfect forwarding of the argument to the
 *  caller and no temporary is created at all.
 *
 *  @tparam Tag  The tag
 *  @tparam Expr The expression type
 *  @{
 */
template <std::size_t Tag, typename Expr>
static UETLI_INLINE auto constexpr make_temp(Expr &&expr) ->
    typename std::enable_if<internal::has_make_temp_impl<Tag, Expr>::value,
                            decltype(internal::make_temp_impl<Tag, Expr>(
                                std::forward<Expr>(expr)))>::type {
  return internal::make_temp_impl<Tag, Expr>(std::forward<Expr>(expr));
}

template <std::size_t Tag, typename Expr>
static UETLI_INLINE auto constexpr make_temp(Expr &&expr) ->
    typename std::enable_if<!internal::has_make_temp_impl<Tag, Expr>::value,
                            decltype(std::forward<Expr>(expr))>::type {
  return std::forward<Expr>(expr);
}
/** @} */

/** @brief
 *  Tags terminal with a unique (in a single expression) tag.
 *
 *  @note
 *  If no specialized implementation is available then this
 *  implementation performs perfect forarding of the argument to the
 *  caller and no tag is created at all.
 *
 *  @tparam Tag  The tag
 *  @tparam Expr The expression type
 *  @{
 */
template <std::size_t Tag, typename Expr>
static UETLI_INLINE auto constexpr tag(Expr &&expr) -> typename std::enable_if<
    internal::has_tag_impl<Tag, Expr>::value,
    decltype(internal::tag_impl<Tag, Expr>(std::forward<Expr>(expr)))>::type {
  return internal::tag_impl<Tag, Expr>(std::forward<Expr>(expr));
}

template <std::size_t Tag, typename Expr>
static UETLI_INLINE auto constexpr tag(Expr &&expr) ->
    typename std::enable_if<!internal::has_tag_impl<Tag, Expr>::value,
                            decltype(std::forward<Expr>(expr))>::type {
  return std::forward<Expr>(expr);
}
/** @} */

/// Helper macro for generating element-wise binary operations
#ifdef SUPPORT_CXX11
#define UETLI_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(OPNAME)               \
  /** @brief                                                                   \
   *  Element-wise OPNAME function                                             \
   *                                                                           \
   *  @tparam A The first expression type                                      \
   *  @tparam B The second expression type                                     \
   */                                                                          \
  template <typename A, typename B>                                            \
  auto constexpr elem_##OPNAME(A &&a, B &&b)                                   \
      ->decltype(internal::elem_##OPNAME##_impl<                               \
                 A, B, internal::get_elem_##OPNAME##_impl<A, B>::value>::      \
                     eval(std::forward<A>(a), std::forward<B>(b))) {           \
    return internal::elem_##OPNAME##_impl<                                     \
        A, B, internal::get_elem_##OPNAME##_impl<A, B>::value>::               \
        eval(std::forward<A>(a), std::forward<B>(b));                          \
  }
#else
#define UETLI_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(OPNAME)               \
  /** @brief                                                                   \
   *  Element-wise OPNAME function                                             \
   *                                                                           \
   *  @tparam A The first expression type                                      \
   *  @tparam B The second expression type                                     \
   */                                                                          \
  template <typename A, typename B>                                            \
  auto constexpr elem_##OPNAME(A &&a, B &&b) {                                 \
    return internal::elem_##OPNAME##_impl<                                     \
        A, B, internal::get_elem_##OPNAME##_impl<A, B>::value>::               \
        eval(std::forward<A>(a), std::forward<B>(b));                          \
  }
#endif

UETLI_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(mul)
UETLI_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(div)
UETLI_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(pow)

#undef UETLI_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS

/// Helper macro for generating element-wise unary operations
#ifdef SUPPORT_CXX11
#define UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                \
  /** @brief                                                                   \
   *  Element-wise OPNAME function                                             \
   *                                                                           \
   *  @tparam A The expression type                                            \
   */                                                                          \
  template <typename A>                                                        \
  auto constexpr elem_##OPNAME(A &&a)->decltype(                               \
      internal::elem_##OPNAME##_impl<                                          \
          A, internal::get_elem_##OPNAME##_impl<A>::value>::                   \
          eval(std::forward<A>(a))) {                                          \
    return internal::elem_##OPNAME##_impl<                                     \
        A, internal::get_elem_##OPNAME##_impl<A>::value>::                     \
        eval(std::forward<A>(a));                                              \
  }
#else
#define UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                \
  /** @brief                                                                   \
   *  Element-wise OPNAME function                                             \
   *                                                                           \
   *  @tparam A The expression type                                            \
   */                                                                          \
  template <typename A> auto constexpr elem_##OPNAME(A &&a) {                  \
    return internal::elem_##OPNAME##_impl<                                     \
        A, internal::get_elem_##OPNAME##_impl<A>::value>::                     \
        eval(std::forward<A>(a));                                              \
  }
#endif

UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(abs)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(conj)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(fabs)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(imag)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(real)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh)
UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc)

#undef UETLI_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

} // namespace uetli

#endif // UETLI_COMPATIBILITY_HPP
