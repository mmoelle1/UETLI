/** @file uetliConstant.hpp
 *
 *  @brief Constant abstraction layer class of the Fluid Dynamics Building
 * Blocks
 *
 *  @copyright This file is part of the UETLI library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef UETLI_CONSTANT_HPP
#define UETLI_CONSTANT_HPP

#include "uetliUtils.hpp"

namespace uetli {

/** @namespace uetli::irqus
 *
 *  @brief
 *  The \ref uetli::irqus namespace, containing a simplified version of
 *  the 'typestring' header file developed by George Makrydakis
 *  <george@irrequietus.eu>.
 *
 *  The header of the original 'typestring' header file reads as follows:
 *
 *  Copyright (C) 2015, 2016 George Makrydakis <george@irrequietus.eu>
 *
 *  The 'typestring' header is a single header C++ library for creating types
 *  to use as type parameters in template instantiations, repository available
 *  at https://github.com/irrequietus/typestring. Conceptually stemming from
 *  own implementation of the same thing (but in a more complicated manner to
 *  be revised) in 'clause': https://github.com/irrequietus/clause.
 *
 *  File subject to the terms and conditions of the Mozilla Public License v
 * 2.0.
 *  If a copy of the MPLv2 license text was not distributed with this file, you
 *  can obtain it at: http://mozilla.org/MPL/2.0/.
 */
namespace irqus {

template <int N, int M> constexpr char tygrab(char const (&c)[M]) noexcept {
  return c[N < M ? N : M - 1];
}

} // namespace irqus

#define TYPESTRING16(n, x)                                                     \
  uetli::irqus::tygrab<0x##n##0>(x), uetli::irqus::tygrab<0x##n##1>(x),        \
      uetli::irqus::tygrab<0x##n##2>(x), uetli::irqus::tygrab<0x##n##3>(x),    \
      uetli::irqus::tygrab<0x##n##4>(x), uetli::irqus::tygrab<0x##n##5>(x),    \
      uetli::irqus::tygrab<0x##n##6>(x), uetli::irqus::tygrab<0x##n##7>(x),    \
      uetli::irqus::tygrab<0x##n##8>(x), uetli::irqus::tygrab<0x##n##9>(x),    \
      uetli::irqus::tygrab<0x##n##A>(x), uetli::irqus::tygrab<0x##n##B>(x),    \
      uetli::irqus::tygrab<0x##n##C>(x), uetli::irqus::tygrab<0x##n##D>(x),    \
      uetli::irqus::tygrab<0x##n##E>(x), uetli::irqus::tygrab<0x##n##F>(x)

/** @brief
 *  Creates an expression-complient constant of value Value.
 */
#define CONSTANT(Value, Expr)                                                  \
  (uetli::make_constant<                                                       \
      uetli::uetliConstant<decltype(Expr), TYPESTRING16(, #Value)>>(Value,     \
                                                                    Expr))

/** @brief
 *  Compile-time constant that decodes the given value as template parameter
 */
template <typename Expr, char... C> struct uetliConstant final {
  // Scalar value type of the expression
  using value_type = typename uetli::value_type<Expr>::type;

  // Tag value (bdj2 hash of the constant value)
  static const std::size_t tag = utils::hash<std::size_t>(C...);

  // Return constant value as string
  static std::string get() {
    std::string value;
    int unpack[]{0, (value += C, 0)...};
    static_cast<void>(unpack);

    std::ostringstream s;
    s << "( " << std::scientific << std::setprecision(16) << std::stold(value)
      << (std::is_same<value_type, float>::value
              ? 'f'
              : std::is_same<value_type, long double>::value ? 'l' : ' ')
      << " )";
    return s.str();
  }
};

} // namespace uetli

#endif // UETLI_CONSTANT_HPP
