/** @file uetliBlockExpressions.hpp
 *
 *  @brief Block expression class
 *
 *  @copyright This file is part of the UETLI library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef UETLI_BLOCK_EXPRESSIONS_HPP
#define UETLI_BLOCK_EXPRESSIONS_HPP

#include <array>
#include <initializer_list>
#include <memory>

#include "uetliCompatibility.hpp"
#include "uetliConfig.hpp"

namespace uetli {

/// Forward declaration
template <class Vector, index_t _rows = 1> struct uetliBlockRowVector;
template <class Vector, index_t _cols = 1> struct uetliBlockColVector;
template <class Vector, index_t _rows = 1> struct uetliBlockRowVectorView;
template <class Vector, index_t _cols = 1> struct uetliBlockColVectorView;
template <class Matrix, index_t _rows = 1, index_t _cols = 1>
struct uetliBlockMatrix;

/** @brief
 *  A fixed-size block row-vector
 *
 *  This is a block row-vector container for a fixed number of
 *  scalar vectors of type Vector. It supports all arithmetic
 *  operations like addition, subtraction, element-wise
 *  multiplication and division. Moreover, it can be
 *  transposed by returning a view of a block column-vector.
 */
template <class Vector, index_t _rows> struct uetliBlockRowVector {
private: // *** Attributes ***
  /// @brief Self type
  using self_type = uetliBlockRowVector<Vector, _rows>;

  /// @brief Data type
  using data_type = std::array<Vector, _rows>;

  /// @brief Raw data of block row-vector
  data_type data;

public: // *** Constructors and accessors ***
  /// @brief Default constructor
  uetliBlockRowVector() = default;

  /// @brief Length constructor
  uetliBlockRowVector(const index_t &size) {
    for (index_t row = 0; row < _rows; row++)
      data[row] = Vector(size);
  }

  /// @brief Length constructor
  uetliBlockRowVector(const std::array<index_t, _rows> &size) {
    for (index_t row = 0; row < _rows; row++)
      data[row] = Vector(size[row]);
  }

  /// @brief Initialiser list constructor
  uetliBlockRowVector(std::initializer_list<Vector> list) : data(list) {}

  /// @brief Returns the number of rows
  const index_t rows() const { return _rows; }

  /// @brief Returns array of sizes
  std::array<index_t, _rows> size() const {
    std::array<index_t, _rows> result;
    for (index_t row = 0; row < _rows; row++)
      result[row] = data[row].size();
    return result;
  }

  /// @brief Returns the transposed
  uetliBlockColVectorView<Vector, _rows> transpose() {
    return uetliBlockColVectorView<Vector, _rows>(*this);
  }

  /// @brief Returns constant reference to raw data
  const data_type &operator()() const { return data; }

  /// @brief Returns reference to raw data
  data_type &operator()() { return data; }

  /// @brief Returns constant reference to scalar vector from row number \a row
  const Vector &operator()(const index_t &row) const { return data[row]; }

  /// @brief Returns reference to scalar vector from row number \a row
  Vector &operator()(const index_t &row) { return data[row]; }

public: // *** Arithmetic operators ***
  /// @brief Adds two block row-vectors
  self_type operator+(const self_type &other) const {
    self_type result(other);
    for (index_t row = 0; row < _rows; row++)
      result(row) = (*this)(row) + other(row);
    return result;
  }

  /// @brief Subtracts one block row-vector from another
  self_type operator-(const self_type &other) const {
    self_type result(other);
    for (index_t row = 0; row < _rows; row++)
      result(row) = (*this)(row)-other(row);
    return result;
  }

  /// @brief Multiplies two block row-vectors element-wise
  self_type operator*(const self_type &other) const {
    self_type result(other);
    for (index_t row = 0; row < _rows; row++)
      result(row) = uetli::elem_mul((*this)(row), other(row));
    return result;
  }

  /// @brief Divides one block row-vector by another element-wise
  self_type operator/(const self_type &other) const {
    self_type result(other);
    for (index_t row = 0; row < _rows; row++)
      result(row) = uetli::elem_div((*this)(row), other(row));
    return result;
  }

  /// @brief Adds two block row-vectors
  self_type &operator+=(const self_type &other) {
    for (index_t row = 0; row < _rows; row++)
      (*this)(row) += other(row);
    return *this;
  }

  /// @brief Subtracts one block row-vector from another
  self_type &operator-=(const self_type &other) {
    for (index_t row = 0; row < _rows; row++)
      (*this)(row) -= other(row);
    return *this;
  }

  /// @brief Multiplies two block row-vectors element-wise
  self_type &operator*=(const self_type &other) {
    for (index_t row = 0; row < _rows; row++)
      (*this)(row) = uetli::elem_mul((*this)(row), other(row));
    return *this;
  }

  /// @brief Divides one block row-vector by another element-wise
  self_type &operator/=(const self_type &other) {
    for (index_t row = 0; row < _rows; row++)
      (*this)(row) = uetli::elem_div((*this)(row), other(row));
    return *this;
  }
};

/// @brief Prints a short description of the block row-vector
template <class Vector, index_t _rows>
std::ostream &operator<<(std::ostream &os,
                         const uetliBlockRowVector<Vector, _rows> &obj) {
  os << "Block row-vector[" << _rows << "]: (";
  for (index_t row = 0; row < _rows; row++)
    os << obj(row).size() << (row + 1 < _rows ? "," : ")");
  return os;
}

/// @brief Prints a short description of the block row-vector
template <class Vector, index_t _rows>
std::ostream &operator<<(std::ostream &os,
                         uetliBlockRowVector<Vector, _rows> &&obj) {
  os << "Block row-vector[" << _rows << "]: (";
  for (index_t row = 0; row < _rows; row++)
    os << obj(row).size() << (row + 1 < _rows ? "," : ")");
  return os;
}

/** @brief
 *  A fixed-size block column-vector
 *
 *  This is a block column-vector container for a fixed number of
 *  scalar vectors of type Vector. It supports all arithmetic
 *  operations like addition, subtraction, element-wise
 *  multiplication and division. Moreover, it can be
 *  transposed by returning a view of a block row-vector.
 */
template <class Vector, index_t _cols> struct uetliBlockColVector {
private: // *** Attributes ***
  /// @brief Self type
  using self_type = uetliBlockColVector<Vector, _cols>;

  /// @brief Data type
  using data_type = std::array<Vector, _cols>;

  /// @brief Raw data of block column-vector
  data_type data;

public: // *** Constructors and accessors ***
  /// @brief Default constructor
  uetliBlockColVector() = default;

  /// @brief Length constructor
  uetliBlockColVector(const index_t &size) {
    for (index_t col = 0; col < _cols; col++)
      data[col] = Vector(size);
  }

  /// @brief Length constructor
  uetliBlockColVector(const std::array<index_t, _cols> &size) {
    for (index_t col = 0; col < _cols; col++)
      data[col] = Vector(size[col]);
  }

  /// @brief Initialiser list constructor
  uetliBlockColVector(std::initializer_list<Vector> list) : data(list) {}

  /// @brief Returns the number of columns
  const index_t cols() const { return _cols; }

  /// @brief Returns array of sizes
  std::array<index_t, _cols> size() const {
    std::array<index_t, _cols> result;
    for (index_t col = 0; col < _cols; col++)
      result[col] = data[col].size();
    return result;
  }

  /// @brief Returns the transposed
  uetliBlockRowVectorView<Vector, _cols> transpose() {
    return uetliBlockRowVectorView<Vector, _cols>(*this);
  }

  /// @brief Returns constant reference to raw data
  const data_type &operator()() const { return data; }

  /// @brief Returns reference to raw data
  data_type &operator()() { return data; }

  /// @brief Returns constant reference to scalar vector from column number \a
  /// col
  const Vector &operator()(const index_t &col) const { return data[col]; }

  /// @brief Returns reference to scalar vector from column number \a col
  Vector &operator()(const index_t &col) { return data[col]; }

public: // *** Arithmetic operators ***
  /// @brief Adds two block column-vectors
  self_type operator+(const self_type &other) const {
    self_type result(other);
    for (index_t col = 0; col < _cols; col++)
      result(col) = (*this)(col) + other(col);
    return result;
  }

  /// @brief Subtracts one block column-vector from another
  self_type operator-(const self_type &other) const {
    self_type result(other);
    for (index_t col = 0; col < _cols; col++)
      result(col) = (*this)(col)-other(col);
    return result;
  }

  /// @brief Multiplies two block column-vectors element-wise
  self_type operator*(const self_type &other) const {
    self_type result(other);
    for (index_t col = 0; col < _cols; col++)
      result(col) = uetli::elem_mul((*this)(col), other(col));
    return result;
  }

  /// @brief Divides one block column-vector by another element-wise
  self_type operator/(const self_type &other) const {
    self_type result(other);
    for (index_t col = 0; col < _cols; col++)
      result(col) = uetli::elem_div((*this)(col), other(col));
    return result;
  }

  /// @brief Adds two block column-vectors
  self_type &operator+=(const self_type &other) {
    for (index_t col = 0; col < _cols; col++)
      (*this)(col) += other(col);
    return *this;
  }

  /// @brief Subtracts one block column-vector from another
  self_type &operator-=(const self_type &other) {
    for (index_t col = 0; col < _cols; col++)
      (*this)(col) -= other(col);
    return *this;
  }

  /// @brief Multiplies two block column-vectors element-wise
  self_type &operator*=(const self_type &other) {
    for (index_t col = 0; col < _cols; col++)
      (*this)(col) = uetli::elem_mul((*this)(col), other(col));
    return *this;
  }

  /// @brief Divides one block column-vector by another element-wise
  self_type &operator/=(const self_type &other) {
    for (index_t col = 0; col < _cols; col++)
      (*this)(col) = uetli::elem_div((*this)(col), other(col));
    return *this;
  }
};

/// @brief Prints a short description of the block column-vector
template <class Vector, index_t _cols>
std::ostream &operator<<(std::ostream &os,
                         const uetliBlockColVector<Vector, _cols> &obj) {
  os << "Block column-vector[" << _cols << "]: (";
  for (index_t col = 0; col < _cols; col++)
    os << obj(col).size() << (col + 1 < _cols ? "," : ")");
  return os;
}

/// @brief Prints a short description of the block column-vector
template <class Vector, index_t _cols>
std::ostream &operator<<(std::ostream &os,
                         uetliBlockColVector<Vector, _cols> &&obj) {
  os << "Block column-vector[" << _cols << "]: (";
  for (index_t col = 0; col < _cols; col++)
    os << obj(col).size() << (col + 1 < _cols ? "," : ")");
  return os;
}

/** @brief
 *  A view on a fixed-size block row-vector
 *
 *  This is a view on a fixed-size block row-vector. It
 *  supports exactly the same functionality but keeps
 *  only references to external data arrays.
 */
template <class Vector, index_t _rows> struct uetliBlockRowVectorView {
private: // *** Attributes ***
  /// @brief Self type
  using self_type = uetliBlockRowVectorView<Vector, _rows>;

  /// @brief Data type
  using data_type = std::array<Vector *, _rows>;

  /// @brief Raw data of block row-vector
  data_type data;

public: // *** Constructors and accessors ***
  /// @brief Default constructor
  uetliBlockRowVectorView() = default;

  /// @brief Initialiser list constructor
  uetliBlockRowVectorView(std::initializer_list<Vector> list) {
    for (index_t row = 0; row < _rows; row++)
      data[row] = list[row];
  }

  /// @brief Block row-vector constructor
  uetliBlockRowVectorView(uetliBlockRowVector<Vector, _rows> &vector) {
    for (index_t row = 0; row < _rows; row++)
      data[row] = &vector(row);
  }

  /// @brief Block column-vector constructor
  uetliBlockRowVectorView(uetliBlockColVector<Vector, _rows> &vector) {
    for (index_t row = 0; row < _rows; row++)
      data[row] = &vector(row);
  }

  /// @brief Block row-vector constructor
  uetliBlockRowVectorView(uetliBlockRowVectorView<Vector, _rows> &vector) {
    for (index_t row = 0; row < _rows; row++)
      data[row] = &vector(row);
  }

  /// @brief Block column-vector constructor
  uetliBlockRowVectorView(uetliBlockColVectorView<Vector, _rows> &vector) {
    for (index_t row = 0; row < _rows; row++)
      data[row] = &vector(row);
  }

  /// @brief Block row-vector constructor
  uetliBlockRowVectorView(uetliBlockRowVector<Vector, _rows> &&vector) {
    for (index_t row = 0; row < _rows; row++)
      data[row] = &vector(row);
  }

  /// @brief Block column-vector constructor
  uetliBlockRowVectorView(uetliBlockColVector<Vector, _rows> &&vector) {
    for (index_t row = 0; row < _rows; row++)
      data[row] = &vector(row);
  }

  /// @brief Block row-vector constructor
  uetliBlockRowVectorView(uetliBlockRowVectorView<Vector, _rows> &&vector) {
    for (index_t row = 0; row < _rows; row++)
      data[row] = &vector(row);
  }

  /// @brief Block column-vector constructor
  uetliBlockRowVectorView(uetliBlockColVectorView<Vector, _rows> &&vector) {
    for (index_t row = 0; row < _rows; row++)
      data[row] = &vector(row);
  }

  /// @brief Returns the number of rows
  const index_t rows() const { return _rows; }

  /// @brief Returns array of sizes
  std::array<index_t, _rows> size() const {
    std::array<index_t, _rows> result;
    for (index_t row = 0; row < _rows; row++)
      result[row] = data[row].size();
    return result;
  }

  /// @brief Returns the transposed
  uetliBlockColVectorView<Vector, _rows> transpose() {
    return uetliBlockColVectorView<Vector, _rows>(*this);
  }

  /// @brief Returns constant reference to raw data
  const data_type &operator()() const { return data; }

  /// @brief Returns reference to raw data
  data_type &operator()() { return data; }

  /// @brief Returns constant reference to scalar vector from row number \a row
  const Vector &operator()(const index_t &row) const { return *data[row]; }

  /// @brief Returns reference to scalar vector from row number \a row
  Vector &operator()(const index_t &row) { return *data[row]; }

public: // *** Arithmetic operators ***
  /// @brief Adds two block row-vectors
  self_type operator+(const self_type &other) const {
    self_type result(other);
    for (index_t row = 0; row < _rows; row++)
      result(row) = (*this)(row) + other(row);
    return result;
  }

  /// @brief Subtracts one block row-vector from another
  self_type operator-(const self_type &other) const {
    self_type result(other);
    for (index_t row = 0; row < _rows; row++)
      result(row) = (*this)(row)-other(row);
    return result;
  }

  /// @brief Multiplies two block row-vectors element-wise
  self_type operator*(const self_type &other) const {
    self_type result(other);
    for (index_t row = 0; row < _rows; row++)
      result(row) = uetli::elem_mul((*this)(row), other(row));
    return result;
  }

  /// @brief Divides one block row-vector by another element-wise
  self_type operator/(const self_type &other) const {
    self_type result(other);
    for (index_t row = 0; row < _rows; row++)
      result(row) = uetli::elem_div((*this)(row), other(row));
    return result;
  }

  /// @brief Adds two block row-vectors
  self_type &operator+=(const self_type &other) {
    for (index_t row = 0; row < _rows; row++)
      (*this)(row) += other(row);
    return *this;
  }

  /// @brief Subtracts one block row-vector from another
  self_type &operator-=(const self_type &other) {
    for (index_t row = 0; row < _rows; row++)
      (*this)(row) -= other(row);
    return *this;
  }

  /// @brief Multiplies two block row-vectors element-wise
  self_type &operator*=(const self_type &other) {
    for (index_t row = 0; row < _rows; row++)
      (*this)(row) = uetli::elem_mul((*this)(row), other(row));
    return *this;
  }

  /// @brief Divides one block row-vector by another element-wise
  self_type &operator/=(const self_type &other) {
    for (index_t row = 0; row < _rows; row++)
      (*this)(row) = uetli::elem_div((*this)(row), other(row));
    return *this;
  }
};

/// @brief Prints a short description of the block row-vector
template <class Vector, index_t _rows>
std::ostream &operator<<(std::ostream &os,
                         const uetliBlockRowVectorView<Vector, _rows> &obj) {
  os << "Block row-vector view[" << _rows << "]: (";
  for (index_t row = 0; row < _rows; row++)
    os << obj(row).size() << (row + 1 < _rows ? "," : ")");
  return os;
}

/// @brief Prints a short description of the block row-vector
template <class Vector, index_t _rows>
std::ostream &operator<<(std::ostream &os,
                         uetliBlockRowVectorView<Vector, _rows> &&obj) {
  os << "Block row-vector view[" << _rows << "]: (";
  for (index_t row = 0; row < _rows; row++)
    os << obj(row).size() << (row + 1 < _rows ? "," : ")");
  return os;
}

/** @brief
 *  A view on a fixed-size block column-vector
 *
 *  This is a view on a fixed-size block column-vector. It
 *  supports exactly the same functionality but keeps
 *  only references to external data arrays.
 */
template <class Vector, index_t _cols> struct uetliBlockColVectorView {
private: // *** Attributes ***
  /// @brief Self type
  using self_type = uetliBlockColVectorView<Vector, _cols>;

  /// @brief Data type
  using data_type = std::array<Vector *, _cols>;

  /// @brief Raw data of block column-vector
  data_type data;

public: // *** Constructors and accessors ***
  /// @brief Default constructor
  uetliBlockColVectorView() = default;

  /// @brief Initialiser list constructor
  uetliBlockColVectorView(std::initializer_list<Vector> list) {
    for (index_t col = 0; col < _cols; col++)
      data[col] = list[col];
  }

  /// @brief Block column-vector constructor
  uetliBlockColVectorView(uetliBlockColVector<Vector, _cols> &vector) {
    for (index_t col = 0; col < _cols; col++)
      data[col] = &vector(col);
  }

  /// @brief Block row-vector constructor
  uetliBlockColVectorView(uetliBlockRowVector<Vector, _cols> &vector) {
    for (index_t col = 0; col < _cols; col++)
      data[col] = &vector(col);
  }

  /// @brief Block column-vector constructor
  uetliBlockColVectorView(uetliBlockColVectorView<Vector, _cols> &vector) {
    for (index_t col = 0; col < _cols; col++)
      data[col] = &vector(col);
  }

  /// @brief Block row-vector constructor
  uetliBlockColVectorView(uetliBlockRowVectorView<Vector, _cols> &vector) {
    for (index_t col = 0; col < _cols; col++)
      data[col] = &vector(col);
  }

  /// @brief Block column-vector constructor
  uetliBlockColVectorView(uetliBlockColVector<Vector, _cols> &&vector) {
    for (index_t col = 0; col < _cols; col++)
      data[col] = &vector(col);
  }

  /// @brief Block row-vector constructor
  uetliBlockColVectorView(uetliBlockRowVector<Vector, _cols> &&vector) {
    for (index_t col = 0; col < _cols; col++)
      data[col] = &vector(col);
  }

  /// @brief Block column-vector constructor
  uetliBlockColVectorView(uetliBlockColVectorView<Vector, _cols> &&vector) {
    for (index_t col = 0; col < _cols; col++)
      data[col] = &vector(col);
  }

  /// @brief Block row-vector constructor
  uetliBlockColVectorView(uetliBlockRowVectorView<Vector, _cols> &&vector) {
    for (index_t col = 0; col < _cols; col++)
      data[col] = &vector(col);
  }

  /// @brief Returns the number of cols
  const index_t cols() const { return _cols; }

  /// @brief Returns array of sizes
  std::array<index_t, _cols> size() const {
    std::array<index_t, _cols> result;
    for (index_t col = 0; col < _cols; col++)
      result[col] = data[col].size();
    return result;
  }

  /// @brief Returns the transposed
  uetliBlockRowVectorView<Vector, _cols> transpose() {
    return uetliBlockRowVectorView<Vector, _cols>(*this);
  }

  /// @brief Returns constant reference to raw data
  const data_type &operator()() const { return data; }

  /// @brief Returns reference to raw data
  data_type &operator()() { return data; }

  /// @brief Returns constant reference to scalar vector from col number \a col
  const Vector &operator()(const index_t &col) const { return *data[col]; }

  /// @brief Returns reference to scalar vector from col number \a col
  Vector &operator()(const index_t &col) { return *data[col]; }

public: // *** Arithmetic operators ***
  /// @brief Adds two block column-vectors
  self_type operator+(const self_type &other) const {
    self_type result(other);
    for (index_t col = 0; col < _cols; col++)
      result(col) = (*this)(col) + other(col);
    return result;
  }

  /// @brief Subtracts one block column-vector from another
  self_type operator-(const self_type &other) const {
    self_type result(other);
    for (index_t col = 0; col < _cols; col++)
      result(col) = (*this)(col)-other(col);
    return result;
  }

  /// @brief Multiplies two block column-vectors element-wise
  self_type operator*(const self_type &other) const {
    self_type result(other);
    for (index_t col = 0; col < _cols; col++)
      result(col) = uetli::elem_mul((*this)(col), other(col));
    return result;
  }

  /// @brief Divides one block column-vector by another element-wise
  self_type operator/(const self_type &other) const {
    self_type result(other);
    for (index_t col = 0; col < _cols; col++)
      result(col) = uetli::elem_div((*this)(col), other(col));
    return result;
  }

  /// @brief Adds two block column-vectors
  self_type &operator+=(const self_type &other) {
    for (index_t col = 0; col < _cols; col++)
      (*this)(col) += other(col);
    return *this;
  }

  /// @brief Subtracts one block column-vector from another
  self_type &operator-=(const self_type &other) {
    for (index_t col = 0; col < _cols; col++)
      (*this)(col) -= other(col);
    return *this;
  }

  /// @brief Multiplies two block column-vectors element-wise
  self_type &operator*=(const self_type &other) {
    for (index_t col = 0; col < _cols; col++)
      (*this)(col) = uetli::elem_mul((*this)(col), other(col));
    return *this;
  }

  /// @brief Divides one block column-vector by another element-wise
  self_type &operator/=(const self_type &other) {
    for (index_t col = 0; col < _cols; col++)
      (*this)(col) = uetli::elem_div((*this)(col), other(col));
    return *this;
  }
};

/// @brief Prints a short description of the block column-vector
template <class Vector, index_t _cols>
std::ostream &operator<<(std::ostream &os,
                         const uetliBlockColVectorView<Vector, _cols> &obj) {
  os << "Block column-vector view[" << _cols << "]: (";
  for (index_t col = 0; col < _cols; col++)
    os << obj(col).size() << (col + 1 < _cols ? "," : ")");
  return os;
}

/// @brief Prints a short description of the block column-vector
template <class Vector, index_t _cols>
std::ostream &operator<<(std::ostream &os,
                         uetliBlockColVectorView<Vector, _cols> &&obj) {
  os << "Block column-vector view[" << _cols << "]: (";
  for (index_t col = 0; col < _cols; col++)
    os << obj(col).size() << (col + 1 < _cols ? "," : ")");
  return os;
}

/** @brief
 *  A fixed-size block matrix
 */
template <class Matrix, index_t _rows, index_t _cols> struct uetliBlockMatrix {
private: // *** Attributes ***
  /// @brief Self type
  using self_type = uetliBlockMatrix<Matrix, _rows, _cols>;

  /// @brief Data type
  using data_type = std::array<std::array<Matrix, _cols>, _rows>;

  /// @brief Raw data of block matrix
  data_type data;

public: // *** Constructors and accessors ***
  /// @brief Default constructor
  uetliBlockMatrix() = default;

  /// @brief Length constructor
  uetliBlockMatrix(const int rows, int cols) {
    for (index_t row = 0; row < _rows; row++)
      for (index_t col = 0; col < _cols; col++)
        data[row][col] = Matrix(rows, cols);
  }

  /// @brief Returns the number of rows
  const index_t rows() const { return _rows; }

  /// @brief Returns the number of columns
  const index_t cols() const { return _cols; }

  /// @brief Returns constant reference to raw data
  const data_type &operator()() const { return data; }

  /// @brief Returns reference to raw data
  data_type &operator()() { return data; }

  /// @brief Returns constant reference to scalar matrix
  /// block from row number \a row and column number \a col
  const Matrix &operator()(const index_t &row, const index_t &col) const {
    return data[row][col];
  }

  /// @brief Returns reference to scalar matrix block
  /// from row number \a row and column number \a col
  Matrix &operator()(const index_t &row, const index_t &col) {
    return data[row][col];
  }

public: // *** Arithmetic operators ***
  /// @brief Adds two block matrices
  self_type operator+(const self_type &other) const {
    self_type result(other);
    for (index_t row = 0; row < _rows; row++)
      for (index_t col = 0; col < _cols; col++)
        result(row, col) = (*this)(row, col) + other(row, col);
    return result;
  }

  /// @brief Subtracts one block matrix from another
  self_type operator-(const self_type &other) const {
    self_type result(other);
    for (index_t row = 0; row < _rows; row++)
      for (index_t col = 0; col < _cols; col++)
        result(row, col) = (*this)(row, col) - other(row, col);
    return result;
  }

  /// @brief Multiplies two block matrices element-wise
  self_type operator*(const self_type &other) const {
    self_type result(other);
    for (index_t row = 0; row < _rows; row++)
      for (index_t col = 0; col < _cols; col++)
        result(row, col) = uetli::elem_mul((*this)(row, col), other(row, col));
    return result;
  }

  /// @brief Divides one block matrix by another element-wise
  self_type operator/(const self_type &other) const {
    self_type result(other);
    for (index_t row = 0; row < _rows; row++)
      for (index_t col = 0; col < _cols; col++)
        result(row, col) = uetli::elem_div((*this)(row, col), other(row, col));
    return result;
  }

  /// @brief Adds two block matrices
  self_type &operator+=(const self_type &other) {
    for (index_t row = 0; row < _rows; row++)
      for (index_t col = 0; col < _cols; col++)
        (*this)(row, col) += other(row, col);
    return *this;
  }

  /// @brief Subtracts one block matrix from another
  self_type &operator-=(const self_type &other) {
    for (index_t row = 0; row < _rows; row++)
      for (index_t col = 0; col < _cols; col++)
        (*this)(row, col) -= other(row, col);
    return *this;
  }

  /// @brief Multiplies two block matrices element-wise
  self_type &operator*=(const self_type &other) {
    for (index_t row = 0; row < _rows; row++)
      for (index_t col = 0; col < _cols; col++)
        (*this)(row, col) = uetli::elem_mul((*this)(row, col), other(row, col));
    return *this;
  }

  /// @brief Divides one block matrix by another element-wise
  self_type &operator/=(const self_type &other) {
    for (index_t row = 0; row < _rows; row++)
      for (index_t col = 0; col < _cols; col++)
        (*this)(row, col) = uetli::elem_div((*this)(row, col), other(row, col));
    return *this;
  }
};

/// @brief Multiplies a scalar matrix with all blocks of a block column-vector
template <class Matrix, class Vector, index_t _cols>
uetliBlockColVector<Vector, _cols>
operator*(const Matrix &matrix,
          const uetliBlockColVector<Vector, _cols> &vector) {
  uetliBlockColVector<Vector, _cols> result;
  for (index_t col = 0; col < _cols; col++)
    result(col) = matrix * vector(col);
}

/// @brief Multiplies a scalar matrix with all blocks of a block column-vector
/// view
// template<class Matrix, class Vector, index_t _cols>
// uetliBlockColVector<Vector, _cols> operator* (const Matrix& matrix,
//                                                 const
//                                                 uetliBlockColVectorView<Vector,
//                                                 _cols>& vector)
// {
//     uetliBlockColVector<Vector, _cols> result;
//     for (index_t col=0; col<_cols; col++)
//         result(col) = matrix * vector(col);
// }

/// @brief Multiplies a block matrix with a block column-vector view
template <class Matrix, class Vector, index_t _rows, index_t _cols>
uetliBlockRowVector<Vector, _rows>
operator*(const uetliBlockMatrix<Matrix, _rows, _cols> &matrix,
          const uetliBlockRowVector<Vector, _cols> &vector) {
  uetliBlockRowVector<Vector, _rows> result;
  for (index_t row = 0; row < _rows; row++)
    for (index_t col = 0; col < _cols; col++)
      result(row) = matrix(row, col) * vector(col);
  return result;
}

/// @brief Multiplies a block matrix with a block column-vector view
// template<class Matrix, class Vector, index_t _rows, index_t _cols>
// uetliBlockRowVector<Vector, _rows> operator* (const uetliBlockMatrix<Matrix,
// _rows, _cols>& matrix,
//                                              const
//                                              uetliBlockRowVectorView<Vector,
//                                              _cols>& vector)
// {
//     uetliBlockRowVector<Vector, _rows> result;
//     for (index_t row=0; row<_rows; row++)
//         for (index_t col=0; col<_cols; col++)
//             result(row) = matrix(row,col) * vector(col);
//     return result;
// }

/// @brief Multiplies a block row-vector with a block column-vector
template <class Matrix, class Vector, index_t _blocks>
Matrix operator*(const uetliBlockRowVector<Vector, _blocks> &vector1,
                 const uetliBlockColVector<Vector, _blocks> &vector2) {
  Matrix result(vector2(0).rows(), vector1(0).cols());
  for (index_t block = 0; block < _blocks; block++)
    result = vector1(block) * vector2(block);
  return result;
}

/// @brief Multiplies a block row-vector view with a block column-vector
// template<class Matrix, class Vector, index_t _blocks>
// Matrix operator* (const uetliBlockRowVectorView<Vector, _blocks>& vector1,
//                   const uetliBlockColVector<Vector, _blocks>&  vector2)
// {
//     Matrix result(vector2(0).rows(), vector1(0).cols());
//     for (index_t block=0; block<_blocks; block++)
//         result = vector1(block) * vector2(block);
//     return result;
// }

/// @brief Multiplies a block row-vector with a block column-vector view
// template<class Matrix, class Vector, index_t _blocks>
// Matrix operator* (const uetliBlockRowVector<Vector, _blocks>&        vector1,
//                   const uetliBlockColVectorView<Vector, _blocks>& vector2)
// {
//     Matrix result(vector2(0).rows(), vector1(0).cols());
//     for (index_t block=0; block<_blocks; block++)
//         result = vector1(block) * vector2(block);
//     return result;
// }

/// @brief Multiplies a block row-vector view with a block column-vector view
// template<class Matrix, class Vector, index_t _blocks>
// Matrix operator* (const uetliBlockRowVectorView<Vector, _blocks>&    vector1,
//                   const uetliBlockColVectorView<Vector, _blocks>& vector2)
// {
//     Matrix result(vector2(0).rows(), vector1(0).cols());
//     for (index_t block=0; block<_blocks; block++)
//         result = vector1(block) * vector2(block);
//     return result;
// }

/// @brief Multiplies a block column-vector with a block row-vector
template <class Matrix, class Vector, index_t _rows, index_t _cols>
uetliBlockMatrix<Matrix, _rows, _cols>
operator*(const uetliBlockColVector<Vector, _cols> &vector1,
          const uetliBlockRowVector<Vector, _rows> &vector2) {
  uetliBlockMatrix<Matrix, _rows, _cols> result;
  for (index_t row = 0; row < _rows; row++)
    for (index_t col = 0; col < _cols; col++)
      result(row, col) = vector1(row) * vector2(col);
  return result;
}

/// @brief Multiplies a block column-vector view with a block row-vector
// template<class Matrix, class Vector, index_t _rows, index_t _cols>
// uetliBlockMatrix<Matrix, _rows, _cols> operator* (const
// uetliBlockColVectorView<Vector, _cols>& vector1,
//                                                  const
//                                                  uetliBlockRowVector<Vector,
//                                                  _rows>&        vector2)
// {
//     uetliBlockMatrix<Matrix, _rows, _cols> result;
//     for (index_t row=0; row<_rows; row++)
//         for (index_t col=0; col<_cols; col++)
//             result(row,col) = vector1(row) * vector2(col);
//     return result;
// }

/// @brief Multiplies a block column-vector with a block row-vector view
// template<class Matrix, class Vector, index_t _rows, index_t _cols>
// uetliBlockMatrix<Matrix, _rows, _cols> operator* (const
// uetliBlockColVector<Vector, _cols>&  vector1,
//                                                  const
//                                                  uetliBlockRowVectorView<Vector,
//                                                  _rows>& vector2)
// {
//     uetliBlockMatrix<Matrix, _rows, _cols> result;
//     for (index_t row=0; row<_rows; row++)
//         for (index_t col=0; col<_cols; col++)
//             result(row,col) = vector1(row) * vector2(col);
//     return result;
// }

/// @brief Multiplies a block column-vector view with a block row-vector view
// template<class Matrix, class Vector, index_t _rows, index_t _cols>
// uetliBlockMatrix<Matrix, _rows, _cols> operator* (const
// uetliBlockColVectorView<Vector, _cols>& vector1,
//                                                  const
//                                                  uetliBlockRowVectorView<Vector,
//                                                  _rows>&    vector2)
// {
//     uetliBlockMatrix<Matrix, _rows, _cols> result;
//     for (index_t row=0; row<_rows; row++)
//         for (index_t col=0; col<_cols; col++)
//             result(row,col) = vector1(row) * vector2(col);
//     return result;
// }

/// @brief Prints a short description of the block matrix
template <class Matrix, index_t _rows, index_t _cols>
std::ostream &operator<<(std::ostream &os,
                         const uetliBlockMatrix<Matrix, _rows, _cols> &obj) {
  os << "Block matrix[" << _rows << "x" << _cols << "]: ((";
  for (index_t row = 0; row < _rows; row++)
    for (index_t col = 0; col < _rows; col++)
      os << obj(row, col).size()
         << (col + 1 < _cols ? "," : row + 1 < _rows ? "),(" : "))");
  return os;
}

/// @brief Prints a short description of the block matrix
template <class Matrix, index_t _rows, index_t _cols>
std::ostream &operator<<(std::ostream &os,
                         uetliBlockMatrix<Matrix, _rows, _cols> &&obj) {
  os << "Block matrix[" << _rows << "x" << _cols << "]: ((";
  for (index_t row = 0; row < _rows; row++)
    for (index_t col = 0; col < _rows; col++)
      os << obj(row, col).size()
         << (col + 1 < _cols ? "," : row + 1 < _rows ? "),(" : "))");
  return os;
}

} // namespace uetli

#endif // UETLI_BLOCK_EXPRESSIONS_HPP
