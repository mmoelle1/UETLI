/** @file uetliConfig.hpp
 *
 *  @brief Configuration class of the Fluid Dynamics Building Blocks
 *
 *  @copyright This file is part of the UETLI library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef UETLI_CONFIG_HPP
#define UETLI_CONFIG_HPP

// Support for Armadillo
#ifdef SUPPORT_ARMADILLO
#include <armadillo>
#endif

// Support for ArrayFire
#ifdef SUPPORT_ARRAYFIRE
#include <arrayfire.h>
#endif

// Support for Blaze
#ifdef SUPPORT_BLAZE
#include <blaze/Math.h>
#endif

// Support for Boost uBLAS
#ifdef SUPPORT_UBLAS
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>
#endif

// Support for (C)MTL4
#if defined(SUPPORT_CMTL4) || defined(SUPPORT_MTL4)
#include <boost/numeric/mtl/mtl.hpp>
#endif

// Support for Eigen
#ifdef SUPPORT_EIGEN
#include <Eigen/Dense>
#include <Eigen/Sparse>
#endif

// Support for IT++
#ifdef SUPPORT_ITPP
#include <itpp/itbase.h>
#endif

// Support for VexCL
#ifdef SUPPORT_VEXCL
#include <vexcl/vexcl.hpp>
#if defined(CL_VERSION_2_0) || defined(VEXCL_BACKEND_CUDA)
#include <vexcl/element_index.hpp>
#include <vexcl/function.hpp>
#include <vexcl/svm_vector.hpp>
#endif
#endif

// Support for ViennaCL
#ifdef SUPPORT_VIENNACL
#include <viennacl/matrix.hpp>
#include <viennacl/vector.hpp>
#endif

/** @namespace uetli
 *
 *  @brief
 *  The \ref uetli namespace, containing all public functionality of
 *  the UETLI library
 *
 *  The \ref uetli namespace contains all functionality of the UETLI
 *  library that is exposed to the end-user. All functionality in this
 *  namespace has a stable API over future UETLI library releases.
 *
 *  @note This Source Code Form is subject to the terms of the Mozilla
 *  Public License, v. 2.0. If a copy of the MPL was not distributed
 *  with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace uetli {

/** @namespace uetli::internal
 *
 *  @brief
 *  The \ref uetli::internal namespace, containing all internal
 *  functionality of the UETLI library
 *
 *  The \ref uetli::internal namespace contains all internal
 *  functionality of the UETLI library such as implementation details
 *  and is not exposed to the end-user. The functionality is subject
 *  to change between different releases without notice. For this
 *  reason, end-users are advised to not make explicit use of
 *  functionality from the \ref uetli::internal namespace.
 *
 *  @note This Source Code Form is subject to the terms of the Mozilla
 *  Public License, v. 2.0. If a copy of the MPL was not distributed
 *  with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace internal {} // namespace internal
} // namespace uetli

/** @typedef index_t
 *
 *  @brief
 *  The index_t typedef specifying the default type for indices
 */
#ifndef index_t
#ifdef UETLI_INDEX_TYPE
#define index_t UETLI_INDEX_TYPE
#pragma message "Using UETLI_INDEX_TYPE for index_t type"
#else
#define index_t std::size_t
#pragma message "Using std::size_t for index_t type"
#endif
#else
#pragma message "Using externally defined index_t type"
#endif

/** @brief Macro for forcing inlining of code.
 *
 *  The macro defines the attribute of the function such that it is
 *  directly inlined and not just an recomendation for the compiler.
 */
#ifdef UETLI_FORCE_INLINE
#if defined(_MSC_VER) || defined(__INTEL_COMPILER)
#define UETLI_INLINE __forceinline
#elif defined(__GNUC__)
#define UETLI_INLINE inline __attribute__((always_inline))
#else
#warning                                                                       \
    "Could not determine compiler for forced inline definitions. Using inline."
#define UETLI_INLINE inline
#endif
#else
#define UETLI_INLINE inline
#endif

/** @brief Macro for avoiding inlining of code.
 *
 *  The macro defines the attribute of the function such that it is no
 *  longer considered for inlining.
 */
#ifdef UETLI_FORCE_INLINE
#define UETLI_NO_INLINE __attribute__((noinline))
#else
#define UETLI_NO_INLINE /* no avoiding of inline defined */
#endif

#endif // UETLI_CONFIG_HPP
