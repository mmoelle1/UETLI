/** @file uetli.h
 *
 *  @brief Main header to be included by clients using the UETLI library
 *
 *  @copyright This file is part of the UETLI library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author: Matthias Moller
 */
#pragma once
#ifndef UETLI_H
#define UETLI_H

/* ------------------- Configuration ---------- */
#include <uetliConfig.hpp>

/*  ------------------ Debug ------------------ */
#include <uetliDebug.hpp>

/* ------------------- Core ------------------- */
#include <uetliCache.hpp>
#include <uetliCompatibility.hpp>

#endif // UETLI_H
