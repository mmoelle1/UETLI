/** @file test_op_plus.hpp
 *
 *  @brief UnitTests++ +operator test
 *
 *  @copyright This file is part of the UETLI library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller, Andrzej Jaeschke
 */

/** @brief
    Test arithmetic operation: plus
 */
TEST_FIXTURE(UETLI_FIXTURE, op_plus) {
  TEST_INIT((1), (UETLI_FIXTURE::len), (3 * sizeof(type)),
            (UETLI_FIXTURE::len));

  try {
    TEST_START();

    for (auto i = 0; i < TEST_RUNS(); i++) {
      fill_vector_d(result_d, type_real{2.0});

      // Addition: 2.0 + 2.0 = 4.0
      uetli::tag<0>(result_d) =
          uetli::tag<0>(result_d) + uetli::tag<0>(result_d);
    }
    TEST_STOP();
    TEST_REPORT();
  } catch (const std::exception &e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  copy_vector_d2h(result_d, result_h);
  fill_vector_h(dummy_h, type_real{4.0});
  CHECK_ARRAY_EQUAL(result_h, dummy_h, UETLI_FIXTURE::len);
}
