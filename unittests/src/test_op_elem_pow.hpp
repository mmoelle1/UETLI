/** @file test_op_elem_pow.hpp
 *
 *  @brief UnitTests++ elem_pow test
 *
 *  @copyright This file is part of the UETLI library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller, Andrzej Jaeschke
 */

/** @brief
    Test arithmetic operation: power
 */
TEST_FIXTURE(UETLI_FIXTURE, op_elem_pow1) {
  TEST_INIT((1), (UETLI_FIXTURE::len), (3 * sizeof(type)),
            (UETLI_FIXTURE::len));

  // Element-wise pow-operation between expression and constant
  try {
    TEST_START();

    for (auto i = 0; i < TEST_RUNS(); i++) {
      fill_vector_d(result_d, type_real{2.0});

      // Power: 2.0 ^ 3.0 = 8.0
      uetli::tag<0>(result_d) =
          uetli::elem_pow(uetli::tag<0>(result_d), CONSTANT(3.0, result_d));
    }

    TEST_STOP();
    TEST_REPORT();
  } catch (const std::exception &e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  copy_vector_d2h(result_d, result_h);
  fill_vector_h(dummy_h, type_real{8.0});
  CHECK_ARRAY_CLOSE(result_h, dummy_h, UETLI_FIXTURE::len, type_real{1e-5});
}

TEST_FIXTURE(UETLI_FIXTURE, op_elem_pow2) {
  TEST_INIT((1), (UETLI_FIXTURE::len), (3 * sizeof(type)),
            (UETLI_FIXTURE::len));

  // Element-wise pow-operation between two expressions
  try {
    TEST_START();

    for (auto i = 0; i < TEST_RUNS(); i++) {
      fill_vector_d(result_d, type_real{2.0});

      // Power: 2.0 ^ 2.0 = 4.0
      result_d = uetli::elem_pow(result_d, result_d);
    }

    TEST_STOP();
    TEST_REPORT();
  } catch (const std::exception &e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  copy_vector_d2h(result_d, result_h);
  fill_vector_h(dummy_h, type_real{4.0});
  CHECK_ARRAY_CLOSE(result_h, dummy_h, UETLI_FIXTURE::len, type_real{1e-5});
}
