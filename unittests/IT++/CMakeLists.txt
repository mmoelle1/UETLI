########################################################################
# CMakeLists.txt
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the UETLI authors
#
# This file is part of the UETLI library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
########################################################################

include(IT++)

########################################################################
# Generate test suite
########################################################################

set(UNITTESTS_SRC "")

# Generate individual source/header files of the form
# itpp-unittest_<test>_<precision>.cxx
# itpp-unittest_<test>_<precision>.hpp
foreach(HEADER ${UETLI_UNITTESTS_HEADERS})

  # Include unittest header file in HPP file
  set(INCLUDE_UNITTESTS_HEADERS "#include \"${HEADER}\"")

  # Extract unittest name
  string(REPLACE ".hpp"  "" HEADER ${HEADER})

  foreach(PRECISION ${UETLI_UNITTESTS_PRECISIONS})

    # Include unittest header file in CXX file
    set(INCLUDE_UNITTESTS_HPP "#include \"itpp-unittest_${HEADER}_${PRECISION}.hpp\"")
    
    # Generate CXX file
    configure_file(itpp-unittest.cxx.in itpp-unittest_${HEADER}_${PRECISION}.cxx)
    list(APPEND UNITTESTS_SRC itpp-unittest_${HEADER}_${PRECISION}.cxx)

    # Generate HPP file
    configure_file(itpp-unittest.hpp.in itpp-unittest_${HEADER}_${PRECISION}.hpp)
    
    set(COUNTER 0)
    foreach(SIZE ${UETLI_UNITTESTS_SIZES})
      
      string(REGEX REPLACE ":|<|>" "T" SUITE_SPEC "${PRECISION}_${SIZE}")
      list(GET UETLI_UNITTESTS_RUNS ${COUNTER} RUNS)
      math(EXPR COUNTER "${COUNTER}+1")
      
      configure_file("${CMAKE_CURRENT_SOURCE_DIR}/itpp-unittest.suite.in"
        "${CMAKE_CURRENT_BINARY_DIR}/itpp-unittest.tmp")
      file(READ "${CMAKE_CURRENT_BINARY_DIR}/itpp-unittest.tmp"   tmp)
      file(APPEND "${CMAKE_CURRENT_BINARY_DIR}/itpp-unittest_${HEADER}_${PRECISION}.hpp" "${tmp}")
      file(REMOVE "${CMAKE_CURRENT_BINARY_DIR}/itpp-unittest.tmp")
      
    endforeach()  
  endforeach()
endforeach()

# Include generated header file into search path
include_directories(${CMAKE_CURRENT_BINARY_DIR})

########################################################################
# Create executables from source files and add tests
########################################################################

add_executables("${UNITTESTS_SRC}")
add_tests("${UNITTESTS_SRC}")

file(GLOB UNITTESTS_SRC RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} *.cpp *.cxx)
add_executables("${UNITTESTS_SRC}" "")
add_tests("${UNITTESTS_SRC}")

if(CUDA_FOUND)
  file(GLOB UNITTESTS_SRC RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} *.cu)
  add_executables("${UNITTESTS_SRC}")
  add_tests("${UNITTESTS_SRC}")
endif()
