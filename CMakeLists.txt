########################################################################
# CMakeLists.txt
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the UETLI authors
#
# This file is part of the UETLI library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# CMakeLists.txt accepts the following command line parameters
#
# UETLI_BUILD_TESTS
# UETLI_BUILD_UNITTESTS
# UETLI_BUILTIN_ARMADILLO
# UETLI_BUILTIN_ARRAYFIRE
# UETLI_BUILTIN_BLAZE
# UETLI_BUILTIN_CMTL4

# UETLI_BUILTIN_EIGEN
# UETLI_BUILTIN_ITPP
# UETLI_BUILTIN_MTL4
# UETLI_BUILTIN_UBLAS
# UETLI_BUILTIN_UNITTESTS
# UETLI_BUILTIN_VEXCL
# UETLI_BUILTIN_VIENNACL
# UETLI_CXX_STANDARD
# UETLI_FORCE_INLINE

# UETLI_WITH_CUDA
# UETLI_WITH_HWOPT
# UETLI_WITH_MIC
# UETLI_WITH_OCL
# UETLI_WITH_OMP
#
# CMakeLists.txt makes use of the following local variables
#
# UETLI_C_FLAGS                     UETLI C compilation flags
# UETLI_C_TARGET_DEPENDENCIES       UETLI dependencies
# UETLI_C_TARGET_LINK_LIBRARIES     UETLI libraries to link against
# UETLI_CXX_FLAGS                   UETLI CXX compilation flags
# UETLI_CXX_TARGET_DEPENDENCIES     UETLI dependencies
# UETLI_CXX_TARGET_LINK_LIBRARIES   UETLI libraries to link against
# UETLI_EXE_LINK_FLAGS              UETLI linking flags
########################################################################

#
# Force CMake version 3.1 or above
#
cmake_minimum_required (VERSION 3.1)

# Set RPATH on MacOSX
if (APPLE)
  set(CMAKE_MACOSX_RPATH ON)
  SET(CMAKE_SKIP_BUILD_RPATH FALSE)
  SET(CMAKE_BUILD_WITH_INSTALL_RPATH FALSE)
  SET(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
  SET(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
  LIST(FIND CMAKE_PLATFORM_IMPLICIT_LINK_DIRECTORIES "${CMAKE_INSTALL_PREFIX}/lib" isSystemDir)
  IF ("${isSystemDir}" STREQUAL "-1")
    SET(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
  ENDIF ()
endif()

#
# Use solution folders for Visual Studio
#
set_property(GLOBAL PROPERTY USE_FOLDERS ON)

if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
   # Set default build type to RelWithDebInfo
   set(CMAKE_BUILD_TYPE RelWithDebInfo CACHE STRING
   "Type of build (None Debug Release RelWithDebInfo MinSizeRel)" FORCE)
   set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug" "Release"
     "RelWithDebInfo" "MinSizeRel")
endif()

#
# Load macros
#
include("${CMAKE_SOURCE_DIR}/cmake/add_executables.cmake")
include("${CMAKE_SOURCE_DIR}/cmake/add_tests.cmake")

#
# Append path to additional modules
#
list(APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake")

#
# We do not support in-source build
#
if("${CMAKE_SOURCE_DIR}" STREQUAL "${CMAKE_BINARY_DIR}")
  message(FATAL_ERROR "In-source builds are not permitted.\nPlease make a separate folder for building, otherwise type \nmake\nthat will create a ./build folder and will compile in that folder. Cmake has created garbage files/dirs (you may manually remove them):\nCMakeCache.txt CMakeFiles")
endif()

#
# Doxygen
#
find_package(Doxygen QUIET)
if(DOXYGEN_FOUND)
  configure_file(${CMAKE_CURRENT_SOURCE_DIR}/doc/Doxyfile.in ${CMAKE_CURRENT_BINARY_DIR}/doc/Doxyfile @ONLY)
  add_custom_target(doc
    ${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/doc/Doxyfile
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    )
endif()

########################################################################
#
# Project: UETLI
#
########################################################################

#
# Project name
#
project(UETLI C CXX)

#
# Testing
#
include(CTest)
enable_testing()

#
# UETLI version
#
set(UETLI_MAJOR_VERSION 2018)
set(UETLI_MINOR_VERSION 04)
set(UETLI_REVISION_VERSION 35)
set(UETLI_VERSION "${UETLI_MAJOR_VERSION}.${UETLI_MINOR_VERSION}-${UETLI_REVISION_VERSION}")

#
# Option list
#
option(UETLI_BUILD_TESTS       "Build unittests"           OFF)
option(UETLI_BUILD_UNITTESTS   "Build unittests"           OFF)

set(UETLI_BUILTIN_ARMADILLO "8.400.0" CACHE STRING "Use built-in Armadillo")
set_property(CACHE UETLI_BUILTIN_ARMADILLO PROPERTY STRINGS "OFF" "7.800.1" "7.800.2" "7.800.3" "7.950.0" "7.950.1" "7.950.2" "7.960.0" "7.960.1" "7.960.2" "8.100.0" "8.100.1" "8.200.0" "8.200.1" "8.200.2" "8.300.0" "8.300.1" "8.300.2" "8.300.3" "8.300.4" "8.400.0" "latest")

option(UETLI_BUILTIN_ARRAYFIRE "Use built-in ArrayFire"    ON)

set(UETLI_BUILTIN_BLAZE "3.3" CACHE STRING "Use built-in Blaze")
set_property(CACHE UETLI_BUILTIN_BLAZE PROPERTY STRINGS "OFF" "3.0" "3.1" "3.2" "3.3" "latest")

option(UETLI_BUILTIN_CMTL4     "Use built-in CMTL4"        ON)



set(UETLI_BUILTIN_EIGEN "3.3.4" CACHE STRING "Use built-in Eigen")
set_property(CACHE UETLI_BUILTIN_EIGEN PROPERTY STRINGS "OFF" "3.2.0" "3.2.1" "3.2.2" "3.2.3" "3.2.4" "3.2.5" "3.2.6" "3.2.7" "3.2.8" "3.2.9" "3.2.10" "3.3.0" "3.3.1" "3.3.2" "3.3.3" "3.3.4" "latest")

option(UETLI_BUILTIN_ITPP      "Use built-in IT++"         ON)
option(UETLI_BUILTIN_MTL4      "Use built-in MTL4"         ON)
option(UETLI_BUILTIN_UBLAS     "Use built-in uBLAS"        ON)
option(UETLI_BUILTIN_UNITTESTS "Use built-in UnitTests++"  ON)

set(UETLI_BUILTIN_VEXCL "latest" CACHE STRING "Use built-in VexCL")
set_property(CACHE UETLI_BUILTIN_VEXCL PROPERTY STRINGS "OFF" "1.4.0" "1.4.1" "latest")

set(UETLI_BUILTIN_VIENNACL "latest" CACHE STRING "Use built-in ViennaCL")
set_property(CACHE UETLI_BUILTIN_VIENNACL PROPERTY STRINGS "OFF" "1.7.0" "1.7.1" "latest")

#
# C++ standard
#
set(UETLI_CXX_STANDARD "11" CACHE STRING "Build with C++11 or C++14")
set_property(CACHE UETLI_CXX_STANDARD PROPERTY STRINGS 11 14)

if(${UETLI_CXX_STANDARD} MATCHES "11")
  add_definitions(-DSUPPORT_CXX11)
elseif(${UETLI_CXX_STANDARD} MATCHES "14")
  add_definitions(-DSUPPORT_CXX14)
else()
  message(FATAL_ERROR "Unsupported C++ standard")
endif()

#
# Force code inlining
#
option(UETLI_FORCE_INLINE "Force code inlining" ON)

if (UETLI_FORCE_INLINE)
  add_definitions(-DFORCE_INLINE)
endif()

#
# Check for CUDA support
#
option(UETLI_WITH_CUDA "Build with CUDA support" OFF)

if(UETLI_WITH_CUDA)
  find_package(CUDA QUIET REQUIRED)
  add_definitions(-DSUPPORT_CUDA)
  
  # Enable C++11 support in NVCC
  if(${UETLI_CXX_STANDARD} MATCHES "11")
    set(CUDA_HOST_COMPILER ${CMAKE_CXX_COMPILER})
    set(CUDA_NVCC_FLAGS ${CUDA_NVCC_FLAGS};-std=c++11)
  else()
    message(FATAL_ERROR "CUDA does not yet support C++14")
  endif()
endif()

#
# Check for hardware optimization support
#
option(UETLI_WITH_HWOPT "Build with hardware optimization" OFF)

if(UETLI_WITH_HWOPT)
  include(OptimizeForArchitecture)
  OptimizeForArchitecture()
  list(APPEND UETLI_C_FLAGS ${UETLI_ARCHITECTURE_FLAGS})
  list(APPEND UETLI_CXX_FLAGS ${UETLI_ARCHITECTURE_FLAGS})  
endif()

#
# Check for MIC support
#
option(UETLI_WITH_MIC "Build with MIC support" OFF)

if(UETLI_WITH_MIC)
  # Requires Intel compiler suite
  if((NOT ${CMAKE_C_COMPILER_ID} MATCHES "Intel") OR
      (NOT ${CMAKE_CXX_COMPILER_ID} MATCHES "Intel"))
    message(FATAL_ERROR "Support for Intel MIC requires Intel compiler suite")
  endif()
  add_definitions(-DSUPPORT_MIC)
endif()
  
#
# Check for OpenCL support
#
option(UETLI_WITH_OCL "Build with OpenCL support" OFF)

if(UETLI_WITH_OCL)
  find_package(OpenCL QUIET REQUIRED)
  add_definitions(-DSUPPORT_OCL)
  include_directories(${OpenCL_INCLUDE_DIR})
  list(APPEND UETLI_C_TARGET_LINK_LIBRARIES "${OpenCL_LIBRARY}")
  list(APPEND UETLI_CXX_TARGET_LINK_LIBRARIES "${OpenCL_LIBRARY}")
endif()

#
# Check for OpenMP support
#
option(UETLI_WITH_OMP "Build with OpenMP support" OFF)

if(UETLI_WITH_OMP)
  find_package(OpenMP QUIET REQUIRED)
  add_definitions(-DUETLI_WITH_OMP)
  
  list(APPEND UETLI_C_FLAGS ${OpenMP_C_FLAGS})
  list(APPEND UETLI_CXX_FLAGS ${OpenMP_CXX_FLAGS})
  list(APPEND UETLI_EXE_LINKER_FLAGS ${OpenMP_EXE_LINKER_FLAGS})
endif()

#
# Apply fixes for known bugs in compilers
#
include("${CMAKE_SOURCE_DIR}/cmake/CompilerFixes.cmake")

########################################################################
#
# Include UETLI_C_FLAGS and UETLI_CXX_FLAGS
#
########################################################################
list(APPEND CMAKE_C_FLAGS   ${UETLI_C_FLAGS})
list(APPEND CMAKE_CXX_FLAGS ${UETLI_CXX_FLAGS})
string(REPLACE ";" " " CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}")
string(REPLACE ";" " " CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")

########################################################################
#
# Summary
#
########################################################################
message("")
message("UETLI version: ${UETLI_VERSION}")
message("")
message("Configuration:")
message("Build type.........................: ${CMAKE_BUILD_TYPE}")
message("Build shared libraries.............: ${BUILD_SHARED_LIBS}")
message("Build directory....................: ${CMAKE_BINARY_DIR}")
message("Source directory...................: ${CMAKE_SOURCE_DIR}")
message("Install directory..................: ${CMAKE_INSTALL_PREFIX}")

message("")
message("AR command.........................: ${CMAKE_AR}")
message("RANLIB command.....................: ${CMAKE_RANLIB}")

if(CMAKE_C_COMPILER)
  message("")
  message("C compiler.........................: ${CMAKE_C_COMPILER}")
  message("C compiler flags ..................: ${CMAKE_C_FLAGS}")
  message("C compiler flags (debug)...........: ${CMAKE_C_FLAGS_DEBUG}")
  message("C compiler flags (release).........: ${CMAKE_C_FLAGS_RELEASE}")
  message("C compiler flags (release+debug)...: ${CMAKE_C_FLAGS_RELWITHDEBINFO}")
endif()

if(CMAKE_CXX_COMPILER)
  message("")
  message("CXX compiler.......................: ${CMAKE_CXX_COMPILER}")
  message("CXX compiler flags ................: ${CMAKE_CXX_FLAGS}")
  message("CXX compiler flags (debug).........: ${CMAKE_CXX_FLAGS_DEBUG}")
  message("CXX compiler flags (release).......: ${CMAKE_CXX_FLAGS_RELEASE}")
  message("CXX compiler flags (release+debug).: ${CMAKE_CXX_FLAGS_RELWITHDEBINFO}")
endif()

message("")
message("EXE linker flags...................: ${CMAKE_EXE_LINKER_FLAGS}")
message("EXE linker flags (debug)...........: ${CMAKE_EXE_LINKER_FLAGS_DEBUG}")
message("EXE linker flags (release).........: ${CMAKE_EXE_LINKER_FLAGS_RELEASE}")
message("EXE linker flags (release+debug)...: ${CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO}")

if(DOXYGEN_FOUND)
  message("")
  message("Doxygen............................: ${DOXYGEN_EXECUTABLE}")
endif()

message("")
message("Options:")
message("UETLI_BUILD_TESTS...................: ${UETLI_BUILD_TESTS}")
message("UETLI_BUILD_UNITTESTS...............: ${UETLI_BUILD_UNITTESTS}")

message("")
message("Features:")
message("UETLI_CXX_STANDARD..................: ${UETLI_CXX_STANDARD}")
message("UETLI_FORCE_INLINE..................: ${UETLI_FORCE_INLINE}")

message("UETLI_WITH_CUDA.....................: ${UETLI_WITH_CUDA}")
message("UETLI_WITH_HWOPT....................: ${UETLI_WITH_HWOPT}")
message("UETLI_WITH_MIC......................: ${UETLI_WITH_MIC}")
message("UETLI_WITH_OCL......................: ${UETLI_WITH_OCL}")
message("UETLI_WITH_OMP......................: ${UETLI_WITH_OMP}")
message("")

########################################################################
# Add kernel directory
########################################################################
include_directories(uetli)
include_directories(uetli/Backend)
include_directories(uetli/BlockExpression)
include_directories(uetli/Cache)
include_directories(uetli/Core)

file (GLOB_RECURSE UETLI_HEADERS
  ${PROJECT_SOURCE_DIR}/uetli/*.h)
source_group("Headers" FILES ${UETLI_HEADERS})


########################################################################
# Tests
########################################################################
if(UETLI_BUILD_TESTS)
  include_directories(tests)
  add_subdirectory(tests)
endif()

########################################################################
# UnitTests
########################################################################
if(UETLI_BUILD_UNITTESTS)
  include_directories(unittests)
  add_subdirectory(unittests)
endif()
