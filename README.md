UETLI (Unified Expression Template Library Interface) is a C++ wrapper
library that provides a unified interface (API) to several expression
template libraries
-  The latest revision of the **UETLI** library is available at
   https://gitlab.com/mmoelle1/UETLI.
-  The latest documentation of the **UETLI** library is available at
   https://mmoelle1.gitlab.io/UETLI/.

# <a name="contents">Contents</a>

1. [Introduction](#introduction)
   * [Prerequisites](#prerequisites)
   * [Coding style](#coding-style)
   * [License](#license)
2. [Getting started](#getting-started)
   * [Obtaining the latest code](#obtaining-the-latest-code)
   * [Compiling and running the UnitTests](#compiling-and-running-the-unittests)
   * [Including UETLI into own codes](#including-uetli-into-own-codes)
3. [The plain program](#the-plain-program)

___

# <a name="introduction">Introduction</a>

**UETLI** is a unified expression template library interface written
in C++14 (C++11 by compiler flag) that provides a unified interface
(API) to several of the most widely used expression template
libraries. It is designed as a header-only C++ library that can be
easily integrated into existing codes by including the main header
file `uetli.h`.


## <a name="prerequisites">Prerequisites</a>

The **UETLI** library makes extensively use of C++11 features (`auto`
keyword, variadic templates, etc.) so that a C++ compiler with full
C++11 support is a prerequisite. Some ETLs, like Blaze, even require
C++14 support. Beside that, additional prerequisites depend on the
specific ETL used. The **UETLI** library currently supports the
following ETLs and compilers (tested under Linux/macOS)

| ETL                                                                   | Remark                                 | Clang | GCC 4.9 | GCC 5.x | GCC 6.x | PGI 16.10 | OracleStudio 12.5 |
|-----------------------------------------------------------------------|----------------------------------------|-------|---------|---------|---------|-----------|-------------------|
| [Armadillo](http://arma.sourceforge.net)                              | version 7.800.2 and better             | yes   | yes     | yes     | yes     | no        | yes (1)           |
| [Arrayfire](http://arrayfire.com)                                     | version 3.3.x and better               | yes   | yes     | yes     | yes     | no        | yes               |
| [Blaze](https://bitbucket.org/blaze-lib/blaze)                        | version 3.0 and better                 | yes   | yes     | yes     | yes     | no        | no                |
| [CMTL4](http://www.simunova.com/de/cmtl4)                             | not tested yet                         |  -    |  -      |  -      |  -      |  -        |  -                |
| [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page)         | version 3.2.x and better with patching | yes   | yes     | yes     | yes     | no        | no                |
| [IT++](http://itpp.sourceforge.net)                                   | version 4.3.1 with patching            | yes   | yes     | yes     | yes     | no        | no                |
| [MTL4](http://www.simunova.com/de/mtl4)                               | not fully supported yet                |  -    |  -      |  -      |  -      |  -        |  -                |
| [uBLAS](http://www.boost.org/doc/libs/1_60_0/libs/numeric/ublas/doc/) | version 1.58 and better                | yes   | yes     | yes     | yes     | no        | no                |
| [VexCL](https://github.com/ddemidov/vexcl)                            | developer version                      | yes   | yes     | yes     | yes     | no        | no                |
| [ViennaCL](http://viennacl.sourceforge.net)                           | developer version                      | yes   | yes     | yes     | yes     | no        | no                |


(1) OracleStudio 12.5 only supports double-type data and throws an
    error of float-type data is used. Moreover, C++14 support is very
    limited leading to many errors about missing trailing return type
    specification.


## <a name="coding-style">Coding style</a>

The coding style of the **UETLI** library is based on [Google's C++
style guide](https://google.github.io/styleguide/cppguide.html). The
[ClangFormat](http://clang.llvm.org/docs/ClangFormat.html) tool is
used regularly to ensure a consistent code format.

## <a name="license">License</a>

The library is licensed under the [Mozilla Public License Version 2.0](https://www.mozilla.org/MPL/2.0).

The MPL is a simple copyleft license. The MPL's "file-level" copyleft
is designed to encourage contributors to share their modifications
with the library, while still allowing them to combine the library
with code under other licenses (open or proprietary) with minimal
restrictions.

___

# <a name="getting-started">Getting started</a>

## <a name="obtaining-the-latest-code">Obtaining the latest code</a>

*   On **Linux/MacOSX**, you may checkout the latest revision using
```shell
$ git clone https://gitlab.com/mmoelle1/UETLI.git UETLI
```
Once the initial checkout is complete you can keep the code up-to-date using
```shell
$ cd UETLI
$ git pull
```

*   On **Windows**, you can use any [git
client](https://git-scm.com/download/win) to checkout the **UETLI**
library and keep the code up-to-date, using the same URL as above.

## <a name="compiling-and-running-the-unittests">Compiling and running the UnitTests</a>

The **UETLI** library comes with unit tests for all supported ETLs. The
compilation requires configuration using [CMake](https://cmake.org) at
a new, empty folder (in-source builds are disabled).

*   On **Linux/MacOSX**, you need to create an empty `build` folder,
    execute CMake and start the compilation process inside that
    folder. If your source folder is `UETLI` then the sequence of commands

```shell
$ mkdir build
$ cd build
$ cmake ../path/to/UETLI -DUETLI_BUILD_UNITTESTS=ON -DUETLI_BUILD_UNITTESTS_<ETL>=ON -DUETLI_<FEATURE>=ON
 -- Build files have been written to: /path/to/build
$ make
 ...
 [100%] Built
```
  compiles the unit tests for the enabled ETLs and features. To see
  a complete list of supported ETLs and features run
```shell
$ mkdir build
$ cd build
$ ccmake ../path/to/UETLI
```
  and set/unset the configuration in the CMake GUI.

  After successful compilation executable unit tests are created at
  the `./unittests/<ETL>/` subdirectory of the build folder.

  All compiled unit tests can be run by executing
```shell
$ make test
```

  If Doxygen is available on your system, you can compile and open
  the Doxygen HTML pages by executing
```shell
$ cd build
$ make doc
 ...
  Built target doc
$ firefox doc/html/index.html
```

*   On **Windows**, you need to run the cmake-gui tool to generate a
    project file and open it with your IDE (e.g., [Visual
    Studio](https://www.visualstudio.com)). Please consider the
    documentation of your IDE how to compile and run executables.

### Example: Compiling UnitTests for VexCL

To compile the unit tests for the VexCL library under Linux/MacOSX you
need to call CMake as follows:
```shell
$ mkdir build
$ cd build
$ cmake ../path/to/UETLI -DUETLI_BUILD_UNITTESTS=ON -DUETLI_BUILD_UNITTESTS_VEXCL=ON -DUETLI_OCL=ON
  ...
  Options:
  UETLI_BUILD_TESTS...................: OFF
  UETLI_BUILD_UNITTESTS...............: ON

  Features:
  UETLI_CXX_STANDARD..................: 14
  UETLI_WITH_CUDA.....................: OFF
  UETLI_WITH_MIC......................: OFF
  UETLI_WITH_OCL......................: ON
  UETLI_WITH_OMP......................: OFF

  UnitTests:
  UETLI_BUILD_UNITTESTS_DEVICES.......: CPU
  UETLI_BUILD_UNITTESTS_PERFMODE......: OFF
  UETLI_BUILD_UNITTESTS_PRECISIONS....: ALL
  UETLI_BUILD_UNITTESTS_SIZES.........: 1000
  UETLI_BUILD_UNITTESTS_RUNS..........: 1
  UETLI_BUILD_UNITTESTS_TESTS.........: ALL

  UETLI_BUILD_UNITTESTS_ARMADILLO.....: OFF
  UETLI_BUILD_UNITTESTS_ARRAYFIRE.....: OFF
  UETLI_BUILD_UNITTESTS_BLAZE.........: OFF
  UETLI_BUILD_UNITTESTS_CMTL4.........: OFF
  UETLI_BUILD_UNITTESTS_EIGEN.........: OFF
  UETLI_BUILD_UNITTESTS_ITPP..........: OFF
  UETLI_BUILD_UNITTESTS_MTL4..........: OFF
  UETLI_BUILD_UNITTESTS_UBLAS.........: OFF
  UETLI_BUILD_UNITTESTS_VEXCL.........: ON
  UETLI_BUILD_UNITTESTS_VIENNACL......: OFF
  ...
 -- Build files have been written to: /path/to/build
$ make vexcl-unittest
 ...
 [100%] Built
$ make test
  Running tests...
  Test project /path/to/build
      Start 1: vexcl-unittest
      1/1 Test #1: vexcl-unittest ...............   Passed    0.02 sec

  100% tests passed, 0 tests failed out of 1

  Total Test time (real) =   0.03 sec
```

### Example: Compiling selection of UnitTests

It is also possible to specify a selection of unit tests that should
be run by specifying a list/file patterns of tests to be included and
a list/file pattern of tests to be excluded.

To perform all unit tests for element-wise operations except for the
power function you need to call CMake as follows:
```shell
$ mkdir build
$ cd build
$ cmake ../path/to/UETLI -DUETLI_BUILD_UNITTESTS=ON -DUETLI_BUILD_UNITTESTS_VEXCL=ON -DUETLI_OCL=ON \
        -DUETLI_UNITTESTS_TESTS=ALL \
        -DUETLI_UNITTESTS_TESTS_INCLUDE="test_op_elem_*" \
        -DUETLI_UNITTESTS_TESTS_EXCLUDE="test_op_elem_pow"
```

## <a name="including-uetli-into-own-codes">Including UETLI into own codes</a>

The **UETLI** library is included in your program by including the
header file
```cpp
#include "uetli.h"
```
By default, **UETLI** assumes
C++14 support enabled and all accelerators and parallelization
techniques disabled. This behavior can be changed by enabled one or
more of the following flags when compiling your program:
*   C++11 support:     `-DUETLI_WITH_CXX11`
*   CUDA support:      `-DUETLI_WITH_CUDA`
*   Intel MIC support: `-DUETLI_WITH_MIC`
*   OpenCL support:    `-DUETLI_WITH_OCL`
*   OpenMP support:    `-DUETLI_WITH_OMP`


# <a name="the-plain-program">The plain program</a>

```cpp
#include <vector>
#include <vexcl/vexcl.hpp>

#include "uetli.h"

// VexCL context
vex::Context ctx( vex::Filter::CPU && vex::Filter::DoublePrecision );

// Vector length
std::size_t N = 10;

// VexCL vectors
vex::vector<double> u0(ctx, N);
vex::vector<double> u1(ctx, N);
vex::vector<double> u2(ctx, N);
vex::vector<double> u3(ctx, N);

// Initialization
u0 = 2.0;
u1 = 1.0;
u2 = 5.0;

// Compute element-wise expression sin(u0+u1)*u2
auto expr = uetli::elem_mul(uetli::elem_sin(u0+u1),u2);

// Assign expression to u3
u3 = expr;
```

___

Author: Matthias Möller
